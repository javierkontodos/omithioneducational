<?php
require 'includes/verificacion.inc.php';
require 'src/modelo/curso.class.php';

$curso = new Curso();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion1').className = "activo";
			cursoPlegable(2);
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="misCursos">
			<?php
				echo $curso->listaMisCursos($_SESSION['user']);
			?>
		</div>

		<div id="otrosCursos">
			<?php
				echo $curso->listaOtrosCursos($_SESSION['user']);
			?>
		</div>
	</section>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>