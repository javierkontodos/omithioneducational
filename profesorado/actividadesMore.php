<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/actividad.class.php';

$actividad = new Actividad();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $actividad->obtenerActividadPorId($id);
}else{
    header("location:actividadesList.php?asignatura=".$_GET['asignatura']);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Actividad | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('icono1').className = "activo";
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
	</script>
</head>
<body>
    <?php include 'includes/header.inc.php';?>
	<section>
		<div class='tarjetaCampus'>
            <h3><i class='fas fa-bookmark'></i> <?php echo $actividad->getNombre(); ?></h3>
            <h5><?php echo "Abierta desde el ".$actividad->converirFecha($actividad->getFecha_inicio()); ?></h5>
            <h5><?php echo "Hasta el  ".$actividad->converirFecha($actividad->getFecha_fin()); ?></h5>
            <p><?php echo $actividad->getContenido(); ?></p>
            <ul>
            	<?php
            		echo $actividad->archivosCampus("SELECT archivo FROM Archivo_Actividad WHERE id_actividad = ".$actividad->getId().";");
            	?>
            </ul>
        </div>

        <h2 id="tituloH2"><i class="fas fa-comments"></i> Respuestas de los alumnos</h2>

        <?php
            echo $actividad->actividadComentariosListProfesor($actividad->getId());
        ?>

	</section>
    <div id="backList">
        <a href="<?php echo 'actividadesList.php?asignatura='.$_GET['asignatura'] ?>"><i class="fas fa-arrow-alt-circle-left"></i></a>
    </div>
    <div id="alertaPantalla" onclick="cerrarAlerta()"></div>
    <div id="alertaMensaje">
        <h2 id="alertaH2"></h2>
        <h3 id="alertaH3"></h3>
        <p id="alertaP"></p>
        <button onclick="cerrarAlerta()">Cerrar</button>
    </div>
    <?php include 'includes/footer.inc.php';?>
</body>
</html>