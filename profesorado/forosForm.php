<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/foro.class.php';

$foro = new Foro();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $foro->obtenerForoPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Foro | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="foro_form" action="../src/controlador/foro.main.php?opcion=1" method="post">
				<input type="hidden" name="url" value="<?php echo 'profesorado/forosList.php?asignatura='.$_GET['asignatura'] ?>">
				<input type="hidden" name="id" value="<?php echo ($foro->getId()!= NULL)?$foro->getId():"0" ?>">
				<input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura'] ?>">
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Título del Foro</label></li>
					<li><input type="text" name="titulo" onblur="check_form_vacio(this, 0)" placeholder="Introduce un titulo" value="<?php echo ($foro->getTitulo()!= NULL)?$foro->getTitulo():"" ?>"></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Descripción del Foro</label></li>
					<li><textarea name="descripcion" onblur="check_form_vacio(this, 1)" placeholder="Introduce la descripcion que tendrá" maxlength="160"><?php echo ($foro->getDescripcion()!= NULL)?$foro->getDescripcion():"" ?></textarea></li>
				</ul>
				<div id="fileForm">
					<input id='numSelect' type='hidden'>
					<?php
						if ($foro->getId()!= NULL) {
							echo $foro->obtenerArchivos($foro->getId());
						}else{
							echo $foro->pintarArchivo(1);
							echo $foro->pintarArchivo(2);
							echo $foro->pintarArchivo(3);
						}
					?>
				</div>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.foro_form.submit()" value="<?php echo ($foro->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="<?php echo 'forosList.php?asignatura='.$_GET['asignatura'] ?>"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>