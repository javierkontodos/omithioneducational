<?php
	require_once '../src/modelo/usuario.class.php';
    require_once '../src/modelo/matricula.class.php';
    require_once '../src/modelo/asignatura.class.php';
    require_once '../src/conector/bd.class.php';

    session_start();
    if (!isset($_SESSION['user'])) {
        header("location:../login.php");
    }elseif (Usuario::usuarioAdmin($_SESSION['user'])) {
        header("location:../administration/index.php");
    }elseif (!Usuario::usuarioProfesor($_SESSION['user'])) {
        header("location:../index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion1').className = "activo";
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section id="asignaturasProfesor">
		<h1>Tus Asignaturas</h1>
		<?php
			$asignaturas = new Asignatura();
			echo $asignaturas->asignaturasProfesorado($_SESSION['user']);
		?>
	</section>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>