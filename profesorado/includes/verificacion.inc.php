<?php
    require_once '../src/modelo/usuario.class.php';
    require_once '../src/modelo/matricula.class.php';
    require_once '../src/modelo/asignatura.class.php';
    require_once '../src/conector/bd.class.php';

    session_start();
    if(isset($_GET['asignatura']) && !empty($_GET['asignatura'])){
        if (!isset($_SESSION['user'])) {
            header("location:../login.php");
        }elseif (!Asignatura::verificaProfesor($_SESSION['user'], $_GET['asignatura'])) {
            header("location:index.php");
        }elseif (Usuario::usuarioAdmin($_SESSION['user'])) {
            header("location:../administration/index.php");
        }elseif (!Usuario::usuarioProfesor($_SESSION['user'])) {
            header("location:../index.php");
        }
    }else header("location:index.php");
?>