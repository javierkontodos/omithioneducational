var bBusquedaProfesor = getXMLHTTPRequest();
function barraBusquedaProfesor(ruta) {
    var nombreBuscado = document.formBusqueda.busqueda.value;
    var asignatura = document.formBusqueda.asignatura.value;
    var myurl = '../src/llamadas/'+ruta+'.php';
    modurl = myurl+'?asignatura='+asignatura+'&nombre='+nombreBuscado;
    bBusquedaProfesor.open("GET", modurl, true); //true es que la conexion es asincrona
    bBusquedaProfesor.onreadystatechange = barraBusquedaProfesorResponse;
    bBusquedaProfesor.send(null);
}
function barraBusquedaProfesorResponse() {
    if (bBusquedaProfesor.readyState == 4) {
        if (bBusquedaProfesor.status == 200) {
            var miTexto = bBusquedaProfesor.responseText;

            if (miTexto == "") {
                document.getElementById('listadoContainer').innerHTML = "<h1>No existen coincidencias con ese nombre</h1>";
            }else{
                document.getElementById('listadoContainer').innerHTML = miTexto;
            }
        }
    }
}