<?php 
	require 'includes/verificacion.inc.php';

	$asignatura = new Asignatura();
	$asignatura->obtenerAsignaturaPorId($_GET['asignatura']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion1').className = "activo";
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<?php
			echo "<h1>".$asignatura->getNombre()."</h1>
				  <div class='adminCards'>
				  	<div onclick=\"window.location='temariosList.php?asignatura=".$asignatura->getId()."';\">
				  		<p class='iconoCard'><i class='fas fa-atlas'></i></p>
				  		<p>Temarios</p>
				  		<input type='button' onclick=\"window.location='temariosList.php?asignatura=".$asignatura->getId()."';\" value='Entrar'>
				  	</div>
				  	<div onclick=\"window.location='actividadesList.php?asignatura=".$asignatura->getId()."';\">
				  		<p class='iconoCard'><i class='fas fa-journal-whills'></i></p>
				  		<p>Actividades</p>
				  		<input type='button' onclick=\"window.location='actividadesList.php?asignatura=".$asignatura->getId()."';\" value='Entrar'>
				  	</div>
				  	<div onclick=\"window.location='forosList.php?asignatura=".$asignatura->getId()."';\">
				  		<p class='iconoCard'><i class='fas fa-comments'></i></p>
				  		<p>Foros</p>
				  		<input type='button' onclick=\"window.location='forosList.php?asignatura=".$asignatura->getId()."';\" value='Entrar'>
				  	</div>
				  </div>
				  <div class='adminCards'>
				  	<div onclick=\"window.location='tutoriasList.php?asignatura=".$asignatura->getId()."';\">
				  		<p class='iconoCard'><i class='fas fa-chalkboard-teacher'></i></p>
				  		<p>Tutorias</p>
				  		<input type='button' onclick=\"window.location='tutoriasList.php?asignatura=".$asignatura->getId()."';\" value='Entrar'>
				  	</div>
				  	<div>
				  		<p class='iconoCard'><i class='fas fa-calendar-alt'></i></p>
				  		<p>Eventos</p>
				  		<input type='button' value='Entrar'>
				  	</div>
				  	<div>
				  		<p class='iconoCard'><i class='fas fa-file-alt'></i></p>
				  		<p>Tests</p>
				  		<input type='button' value='Entrar'>
				  	</div>
				  </div>";
		?>	  
	</section>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>