<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/tutoria.class.php';

$tutoria = new Tutoria();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $tutoria->obtenerTutoriaPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Tutoria | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="tutoria_form" action="../src/controlador/tutoria.main.php?opcion=1" method="post">
				<input type="hidden" name="url" value="<?php echo 'profesorado/tutoriasList.php?asignatura='.$_GET['asignatura'] ?>">
				<input type="hidden" name="id" value="<?php echo ($tutoria->getId()!= NULL)?$tutoria->getId():"0" ?>">
				<input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura'] ?>">
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Alumno</label></li>
					<li>
						<select name="alumno">
							<?php
								$usuario = new Usuario();
								echo $usuario->selectorAlumnos(($tutoria->getId_usuario()!= NULL)?$tutoria->getId_usuario():0);
							?>
						</select>
					</li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Título del Tutoria</label></li>
					<li><input type="text" name="titulo" onblur="check_form_vacio(this, 0)" placeholder="Introduce un titulo" value="<?php echo ($tutoria->getTitulo()!= NULL)?$tutoria->getTitulo():"" ?>"></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Descripción del Tutoria</label></li>
					<li><textarea name="descripcion" onblur="check_form_vacio(this, 1)" placeholder="Introduce la descripcion que tendrá" maxlength="160"><?php echo ($tutoria->getDescripcion()!= NULL)?$tutoria->getDescripcion():"" ?></textarea></li>
				</ul>
				<div id="fileForm">
					<input id='numSelect' type='hidden'>
					<?php
						if ($tutoria->getId()!= NULL) {
							echo $tutoria->obtenerArchivos($tutoria->getId());
						}else{
							echo $tutoria->pintarArchivo(1);
							echo $tutoria->pintarArchivo(2);
							echo $tutoria->pintarArchivo(3);
						}
					?>
				</div>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.tutoria_form.submit()" value="<?php echo ($tutoria->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="<?php echo 'tutoriasList.php?asignatura='.$_GET['asignatura'] ?>"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>