<?php 	
require 'includes/verificacion.inc.php'; 
require 'src/modelo/curso.class.php';
require 'src/modelo/matricula.class.php';
$curso = new Curso();
$usuario = new Usuario();
$listaTarjetas = new TarjetaBancaria();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $curso->obtenerCursoPorId($id);
    $usuario->obtenerUsuarioPorId($usuario->obtenerIdPorEmail($_SESSION['user']));
}else{
	header("location:index.php");
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Matriculación | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<h1><?php echo $curso->getNombre() ?></h1>

		<div id='tarjetaCurso'>
			<div id='imagenCurso'>
				<?php echo "<span style='background-image: url(".$curso->getFoto()."'></span>" ?>
			</div>
			<div id='datosCurso'>
				<p><?php echo $curso->getDescripcion() ?></p>
				<div id='infoCurso'>
					<h3 style="color: #FFBA00;"><?php echo $curso->getPrecio() ?> €</h3>
					<h4><?php echo $curso->getDuracion() ?> Hrs</h4>
				</div>
			</div>
		</div>

		<p style="text-align: center;">Para realizar correctamente la matriculación en el curso deberas cumplimentar un pago a continuacion.</p>

		<div id="matriculaForm">
			<form name="nuevaMatricula" action="src/controlador/matricula.main.php?opcion=1" method="post">
				<input type="hidden" name="id_usuario" value="<?php echo $usuario->getId() ?>">
				<input type="hidden" name="id_curso" value="<?php echo $curso->getId() ?>">
				<ul>
					<li>
						<select name='tipo_pago' onchange="seleccionarPago(this)">
							<option value='0'>Selecciona un tipo pago</option>
							<option value='1'>Tarjeta</option>
							<option value='2'>Transferencia</option>
						</select>
					</li>
				</ul>

				<div id="opcionesTarjeta">
					<h2>Mis tarjetas de crédito y débito</h2>
					<hr>
					<input type="hidden" name="idTarjetaBancaria" id="idTarjetaBancaria">
					<?php echo $listaTarjetas->listarTarjetas($usuario->getId()) ?>
					<hr style="width: 40%; margin-left: 5%;">
					<ul onclick="anadirTarjeta()" style="color: #FFBA00">
						<li><i class="fas fa-plus-circle"></i></li>
						<li>Añadir una tarjeta de crédito o de débito</li>
					</ul>
				</div>

				<div id="nuevaTarjeta">
					<h2>Nueva Tarjeta</h2>
					<ul>
						<li><input type="text" name="numeroTarjeta" placeholder="Número de la tarjeta"></li>
					</ul>
					<div id="datosTarjeta">
						<ul>
							<li><label>Mes</label></li>
							<li>
								<select name='mesTarjeta'>
									<option value='1'>1</option>
									<option value='2'>2</option>
									<option value='3'>3</option>
									<option value='4'>4</option>
									<option value='5'>5</option>
									<option value='6'>6</option>
									<option value='7'>7</option>
									<option value='8'>8</option>
									<option value='9'>9</option>
									<option value='10'>10</option>
									<option value='11'>11</option>
									<option value='12'>12</option>
								</select>
							</li>
						</ul>
						<ul>
							<li><label>Año</label></li>
							<li>
								<select name='anoTarjeta'>
									<option value='19'>19</option>
									<option value='20'>20</option>
									<option value='21'>21</option>
									<option value='22'>22</option>
									<option value='22'>23</option>
									<option value='24'>24</option>
									<option value='25'>25</option>
									<option value='26'>26</option>
									<option value='27'>27</option>
									<option value='28'>28</option>
									<option value='29'>29</option>
									<option value='30'>30</option>
								</select>
							</li>
						</ul>
						<ul style="margin-right: 0;">
							<li><label>CVV</label></li>
							<li><input type="number" name="cvvTarjeta" placeholder="CVV"></li>
						</ul>
					</div>
					<ul>
						<li><label>Nombre del titular</label></li>
						<li><input type="text" name="nombreTarjeta" placeholder="Nombre del titular"></li>
					</ul>
					<ul id="botonesTarjeta">
						<li>
							<input type="button" style="background-color: #149F1C;" onclick="recordarTarjetaCheck(this)" value="Recordar">
							<input type="checkbox" id="recordarCheck" name="recordarTarjeta" style="display: none;" checked>
						</li>
						<li><input type="button" name="cancelar" value="Cancelar" onclick="anadirTarjeta()"></li>
					</ul>
				</div>

				<div id="nuevaTransferencia">
					<h2>Nueva transferencia bancaria</h2>
					<p>Puedes realizar una transferencia o ingreso bancario en la cuenta que ves a continuación, indicando además quién es el titular de la cuenta desde la que se emite el dinero, y el número de tu pedido. Los datos que te faltan para poder completar la transacción son los del destinatario del ingreso (que somos nosotros) y la entidad donde se depositará el dinero. Puedes ver esa información aquí:</p>

					<ul>
						<li><label><i class="fas fa-caret-right"></i> Banco</label></li>
						<li><p>BBVA, S.A</p></li>
						<li><label><i class="fas fa-caret-right"></i> Dirección bancaria</label></li>
						<li><p>Calle Domingo Pérez del Val, 2, 28045 Madrid</p></li>
						<li><label><i class="fas fa-caret-right"></i> IBAN BBVA</label></li>
						<li><p>ES41 0882 2641 74 0201462438</p></li>
						<li><label><i class="fas fa-caret-right"></i> Swift</label></li>
						<li><p>BBVAESMMXXX</p></li>
						<li><label><i class="fas fa-caret-right"></i> Titular</label></li>
						<li><p>Omithion Educational</p></li>
					</ul>
					<ul>
						<li><label>A continuación sube tu recibo</label></li>
						<li>
							<p id="nombreFile"></p>
							<input type="hidden" name="recibo" id="selectedRecibo">
							<input type="file" name="selectedFile" id="selectedFile" style="display: none;" onchange="reciboMatricula()">
							<input type="button" name="botonTranferencia" value="Subir Recibo" onclick="document.getElementById('selectedFile').click();">
						</li>
					</ul>
				</div>

				<div id="botonesPago">
					<input type="button" id="notificacionesBtn" style="background-color: #149F1C;" onclick="recibirNotificaciones(this)" value="Recibir Notificaciones">
					<input type="checkbox" id="notificacionesCheck" name="notificaciones" style="display: none;" checked>
					<hr>
					<input type="button" onclick="verifica_matriculacion()" value="Finalizar Pago">
				</div>
			</form>
		</div>
	</section>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>