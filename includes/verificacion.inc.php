<?php
	require_once 'src/modelo/usuario.class.php';
	require_once 'src/conector/bd.class.php';

	session_start();
    if (!isset($_SESSION["user"])) {
        header("location:login.php");
    }elseif (!Usuario::verificado($_SESSION["user"])) {
    	header("location:verificacion.php");
    }elseif (!Usuario::perfilCompletado($_SESSION["user"])) {
    	header("location:completaperfil.php");
    }elseif (Usuario::usuarioAdmin($_SESSION["user"])) {
        header("location:administration/index.php");
    }elseif (Usuario::usuarioProfesor($_SESSION['user'])) {
        header("location:profesorado/index.php");
    }
?>