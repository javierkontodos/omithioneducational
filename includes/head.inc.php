<meta charset="UTF-8">
<meta name="keywords" content="">
<link href="css/style.css" rel="stylesheet" type="text/css"/>
<link href="css/login.style.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" href="images/favicon.png">
<script defer src="https://use.fontawesome.com/releases/v5.7.0/js/all.js"></script>
<script src="js/scripts.js" type="text/javascript"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Bungee|Bungee+Hairline&display=swap" rel="stylesheet">

