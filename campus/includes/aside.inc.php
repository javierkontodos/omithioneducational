<aside>
	<div id="asideMovilPlegable" title='Minimizar menu' onclick="cerrarAsideMovil()">
		<span><i class="fas fa-angle-down fa-7x"></i></span>
	</div>
	<div id="asidePlegable" title='Minimizar menu' onclick="cerrarAside()">
		<span><i class="fas fa-angle-right fa-3x"></i></span>
	</div>
	<div id="asideGeneral">
		<img src="../images/logo_blanco.png">
		<ul>
			<li><a id="opcion1" title='Inicio' href="<?php echo "index.php?curso=".$_GET['curso'] ?>"><i class="fab fa-fort-awesome-alt fa-lg"></i></a></li>
			<li><a id="opcion2" title='Perfil' href="<?php echo "perfil.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura'] ?>"><i class="fas fa-user-circle fa-lg"></i></a></li>
			<li><a id="opcion3" title='Ajustes' href="<?php echo "ajustes.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura'] ?>"><i class="fas fa-cog fa-lg"></i></a></li>
			<li><a id="opcion4" title='Ajustes' href="<?php echo "ayuda.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura'] ?>"><i class="fas fa-question-circle fa-lg"></i></a></li>
			<li><a title='Salir' href="../index.php"><i class="fas fa-power-off fa-lg"></i></a></li>
		</ul>

		<?php
			$monedas = new Usuario();
			echo "<h2 title='Monedas ganadas'>".$monedas->obtenerMonedas($_SESSION['user'])." Monedas <i class='fas fa-donate'></i></h2>";
		?>
		
		<?php
			echo "<select onchange='cambiarAsignatura(this,".$_GET['curso'].");'>";
			$asignaturas = new Asignatura();
			echo $asignaturas->asignaturasAside($_GET['curso'], $_GET['asignatura']);
			echo "</select>";
		?>

		<div id="menuAside1">
			<ul>
				<?php echo "<li id='icono1'><i title='Actividades' class='far fa-file-alt fa-2x' onclick=window.location='actividades.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura']."';></i></li>"; ?>
				<?php echo "<li id='icono2'><i title='Temarios' class='fas fa-book-open fa-2x' onclick=window.location='temarios.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura']."';></i></li>"; ?>
				<?php echo "<li id='icono3'><i title='Foros' class='fas fa-bullhorn fa-2x' onclick=window.location='foros.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura']."';></i></li>"; ?>
				<?php echo "<li id='icono4'><i title='Notificaciones' class='fas fa-bell fa-2x' onclick=window.location='notificaciones.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura']."';></i></li>"; ?>
			</ul>
			<ul>
				<li style="width: 15%; margin-left: 35%"><i class="fas fa-circle"></i></li>
				<li style="width: 15%;" onclick="cambiarMenuAside(1)"><i class="far fa-circle"></i></li>
			</ul>
		</div>
		<div id="menuAside2">
			<ul>
				<?php echo "<li id='icono5'><i title='Tienda' class='fas fa-shopping-cart fa-2x' onclick=window.location='tienda.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura']."';></i></li>"; ?>
				<?php echo "<li id='icono6'><i title='Tutorias' class='fas fa-chalkboard-teacher fa-2x' onclick=window.location='tutorias.php?curso=".$_GET['curso']."&asignatura=".$_GET['asignatura']."';></i></li>"; ?>
				<?php echo "<li id='icono7'><i title='Eventos' class='fas fa-calendar-alt fa-2x'></i></li>"; ?>
				<?php echo "<li id='icono8'><i title='Calificaciones' class='fas fa-chart-pie fa-2x' ></i></li>"; ?>
			</ul>
			<ul>
				<li style="width: 15%; margin-left: 35%" onclick="cambiarMenuAside(2)"><i class="far fa-circle"></i></li>
				<li style="width: 15%;"><i class="fas fa-circle"></i></li>
			</ul>
		</div>
		<p>© 2019 Omithion Educational. Todos los derechos reservados.</p>
	</div>
</aside>
<div id="asidePlegado" title='Maximizar menu' onclick="cerrarAside()" style="display: none;">
	<span><i class="fas fa-angle-left fa-3x"></i></span>
</div>
<div id="asideMovilPlegado" title='Maximizar menu' onclick="cerrarAsideMovil()">
	<span><i class="fas fa-angle-up fa-7x"></i></span>
</div>