var bCambiaFoto = getXMLHTTPRequest();
function cambiaFotoUsuario() {
    var fotoAntigua = document.getElementById('selectedFoto').value;
    var myurl = '../src/llamadas/cambiarFotoUser.php';
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&fotoAntigua='+fotoAntigua;
    bCambiaFoto.open("POST", modurl, true); //true es que la conexion es asincrona
    bCambiaFoto.onreadystatechange = cambiaFotoUsuarioResponse;

    var fileInput = document.getElementById('selectedFile');
	var file = fileInput.files[0];
	var formData = new FormData();
	formData.append('foto', file);

	bCambiaFoto.send(formData);
}
function cambiaFotoUsuarioResponse() {
    if (bCambiaFoto.readyState == 4) {
        if (bCambiaFoto.status == 200) {
            var miTexto = bCambiaFoto.responseText;

            if (miTexto != "grande") {
                if (miTexto == "tipo") {
                    alertaPantalla("Archivo no reconocido.", "El archivo que intentas subir no se corresponde al de una imagen.", "Intentalo de nuevo con un archivo de tipo .jpg, .jpeg o .png.");
                }else{
                    document.getElementById('fotoUser').style.backgroundImage = "url('"+miTexto+"')";
                    document.getElementById('selectedFoto').value = miTexto;
                }
            }else {
                alertaPantalla("Archivo demasiado grande", "El archivo no puede superar los 2 Mb de tamaño.", "Intentalo de nuevo con un archivo mas pequeño.");
            }
        }
    }
}

function cambiarMenuAside(pos){
    document.getElementById('menuAside1').style.display = "none";
    document.getElementById('menuAside2').style.display = "none";
    if (pos == 1) document.getElementById('menuAside2').style.display = "inline-block";
    else document.getElementById('menuAside1').style.display = "inline-block";
}

function cerrarAside() {
    if (document.getElementById('asidePlegado').style.display == "none") {
        document.getElementById('asidePlegado').style.display = "inline-block";
        document.getElementsByTagName('aside')[0].style.display = "none";
        document.getElementsByTagName('section')[0].style.width = "88%";
    }else{
        document.getElementById('asidePlegado').style.display = "none";
        document.getElementsByTagName('aside')[0].style.display = "inline-block";
        document.getElementsByTagName('section')[0].style.width = sectionSize;
    }
}

function cambiarAsignatura(select, curso) {
    window.location="temarios.php?curso="+curso+"&asignatura="+select.value;
}

function cerrarAsideMovil() {
    if (document.getElementById('asideMovilPlegado').style.display == "none") {
        document.getElementById('asideMovilPlegado').style.display = "inline-block";
        document.getElementsByTagName('aside')[0].style.display = "none";
    }else{
        document.getElementById('asideMovilPlegado').style.display = "none";
        document.getElementsByTagName('aside')[0].style.display = "inline-block";
    }
}

var comentarioNew = false;
function muestraComentario() {
   if (comentarioNew) {
       document.getElementById('newComentarioCampus').style.display = "none";
   }else{
       document.getElementById('newComentarioCampus').style.display = "inline-block";
   }
   comentarioNew  = !comentarioNew;
}

var nuevoNew = false;
function muestraNuevo() {
   if (nuevoNew) {
       document.getElementById('newComentarioCampus').style.display = "none";
       document.getElementById('nuevoButton').style.backgroundColor = "#9FD120";
       document.getElementById('nuevoButton').innerHTML = "Nuevo Foro";
   }else{
       document.getElementById('newComentarioCampus').style.display = "inline-block";
       document.getElementById('nuevoButton').style.backgroundColor = "#C0392B";
       document.getElementById('nuevoButton').innerHTML = "Cancelar";
   }
   nuevoNew  = !nuevoNew;
}

function nuevoComentario() {
    var fomulario = document.comentario_form;
    if (fomulario.comentario.value != "" && fomulario.comentario.value.length > 10) {
        fomulario.submit();
    }else{
        document.getElementById('comentarioLabel').style.color = "red";
        fomulario.comentario.style.borderColor = "red";
    }
}

function nuevoForoTutoria() {
    ok = false;
    var fomulario = document.comentario_form;
    if (fomulario.descripcion.value != "" && fomulario.descripcion.value.length > 10) {
        ok = true;
    }else{
        document.getElementById('nuevoLabel2').style.color = "red";
        fomulario.descripcion.style.borderColor = "red";
    }
    if (fomulario.titulo.value != "" && fomulario.titulo.value.length > 5) {
        ok = true;
    }else{
        document.getElementById('nuevoLabel1').style.color = "red";
        fomulario.titulo.style.borderColor = "red";
    }

    if (ok) {
        fomulario.submit();
    }
}
