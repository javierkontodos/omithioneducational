<?php
require_once '../src/modelo/usuario.class.php';
require_once '../src/modelo/matricula.class.php';
require_once '../src/modelo/asignatura.class.php';
require_once '../src/modelo/tutoria.class.php';
require_once '../src/conector/bd.class.php';

session_start();
if(isset($_GET['curso']) && !empty($_GET['curso']) && isset($_GET['asignatura']) && !empty($_GET['asignatura']) && isset($_GET['tutoria']) && !empty($_GET['tutoria'])){
	if (!isset($_SESSION['user'])) {
   	    header("location:../login.php");
   	}else if (!Matricula::verificada($_SESSION['user'], $_GET['curso'], $_GET['asignatura'])) {
   		header("location:../index.php");
   	}elseif (Usuario::usuarioAdmin($_SESSION['user'])) {
   	    header("location:../administration/index.php");
   	}elseif (Usuario::usuarioProfesor($_SESSION['user'])) {
   	    header("location:profesorado/index.php");
   	}elseif (!Tutoria::verificado($_GET['asignatura'], $_GET['tutoria'])) {
   		header("location:../index.php");
   	}
}else header("location: ../index.php");

$tutoria = new Tutoria();
$tutoria->obtenerTutoriaPorId($_GET['tutoria']);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Tutoria | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('icono6').className = "activo";
			cambiarMenuAside(1);
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
	</script>
</head>
<body>
	<section>
		<div class='tarjetaCampus'>
            <h3><i class='fas fa-bookmark'></i> <?php echo $tutoria->getTitulo(); ?></h3>
            <h5><?php echo $tutoria->converirFecha($tutoria->getFecha()); ?></h5>
            <p><?php echo $tutoria->getDescripcion(); ?></p>
            <ul>
            	<?php
            		echo $tutoria->archivosCampus("SELECT archivo FROM Archivo_Tutoria WHERE id_tutoria = ".$tutoria->getId().";");
            	?>
            </ul>
            <button onclick="muestraComentario()">Comentar</button>
        </div>

        <div id="newComentarioCampus">
        	<form name="comentario_form" action="../src/controlador/tutoria.main.php?opcion=3" method="post">
        		<h2 id="tituloH2">Nuevo comentario</h2>
        		<ul>
        			<li><label id="comentarioLabel">Tu Comentario</label></li>
        			<li>
                        <input type="hidden" name="curso" value="<?php echo $_GET['curso']; ?>">
                        <input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura']; ?>">
                        <input type="hidden" name="tutoria" value="<?php echo $_GET['tutoria']; ?>">
        				<textarea name="comentario" placeholder="Escribe aqui tu comentario" maxlength="140"></textarea>
        			</li>
        		</ul>
                <div id="fileForm">
                    <input id="numSelect" type="hidden">
                    <ul>
                        <li id="spanImage1" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName1" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected1" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected1').click()" value="Subir Archivo"></li>
                        <li><input id="archivo1" name="archivo1" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected1" type="file" style="display: none;" onchange="subirArchivoForm(1, 'cambiarArchivoTutoria')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage2" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName2" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected2" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected2').click()" value="Subir Archivo"></li>
                        <li><input id="archivo2" name="archivo2" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected2" type="file" style="display: none;" onchange="subirArchivoForm(2, 'cambiarArchivoTutoria')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage3" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName3" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected3" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected3').click()" value="Subir Archivo"></li>
                        <li><input id="archivo3" name="archivo3" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected3" type="file" style="display: none;" onchange="subirArchivoForm(3, 'cambiarArchivoTutoria')"></li>
                    </ul>
                </div>
        		<input type="button" value="Enviar" onclick="nuevoComentario()">
        	</form>
        </div>

        <h2 id="tituloH2"><i class="fas fa-comments"></i> Comentarios</h2>

        <?php
            echo $tutoria->tutoriaComentariosListCampus($tutoria->getId(), $_SESSION['user']);
        ?>

	</section>
	<?php include 'includes/aside.inc.php';?>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
</body>
</html>