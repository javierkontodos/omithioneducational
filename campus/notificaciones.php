<?php
require 'includes/verificacion.inc.php';
$usuario = new Usuario();
$id = $usuario->obtenerIdPorEmail($_SESSION['user']);
$usuario->obtenerUsuarioPorId($id);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Perfil | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('icono4').className = "activo";
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
	</script>
</head>
<body>
	<section>
		<h1>Bandeja de Notificaciones</h1>
		<p style="text-align: center;">Aqui encontrarás las notificaciones sobre la asignatura</p>
		
	</section>
	<?php include 'includes/aside.inc.php';?>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
</body>
</html>