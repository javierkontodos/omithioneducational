<?php
require 'includes/verificacion.inc.php';

$usuario = new Usuario();
$id = $usuario->obtenerIdPorEmail($_SESSION['user']);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ayuda | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('opcion4').className = "activo";
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
		function mostrarconsulta() {
			document.getElementById('ayudaLista').style.display = "none";
			document.getElementById('formConsulta').style.display = "inline-block";
		}
		function check_form_vacio(elemento, pos){
		    if (elemento.value == "") {
		        elemento.style.borderColor = "red";
		        document.getElementsByClassName('labelForm')[pos].style.color = "red";
		        document.getElementsByClassName('spanForm')[pos].style.display = "inline-block";
		    }else{
		        elemento.style.borderColor = "black";
		        document.getElementsByClassName('labelForm')[pos].style.color = "black";
		        document.getElementsByClassName('spanForm')[pos].style.display = "none";
		    }
		}
		function compruebaConsulta(argument) {
			var ok = true;
    		var formulario = document.formConsulta;
    		if(!formulario.titulo.value){
    			check_form_vacio(formulario.titulo, 0);
    		    ok = false;
    		}
    		if(!formulario.descripcion.value){
    			check_form_vacio(formulario.descripcion, 1);
    		    ok = false;
    		}
    		if (ok) {
    		    formulario.submit();
    		}
		}
	</script>
	<style type="text/css">
		.vidios{
			width: 915px;
			height: 515px;
		}
		@media screen and (max-width:700px){
			.vidios{
				width: 350px;
				height: 198px;
			}
		}
	</style>
</head>
<body>
	<section id="ayudaView">
		<div id="ayudaLista">
			<h1>Bienvenido a la pagina de ayuda</h1>
			<p style="text-align: center;">Aqui encontrarás una pequeña guia de uso de la aplicación, si necesitas hacer alguna duda al soporte no dudes en rellenar el formulario de consulta.</p>
			<button onclick="mostrarconsulta()">Enviar Consulta</button>
			
			<h3><a href='javascript:ayudaPlegable(2)'><span id='iconoPlegable2'><i class='fas fa-chevron-circle-right'></i></span> Cambiar foto de perfil</a></h3>
    	    <span id='ayudaPlegable2' style="display: none;">
    	    	<p>Para cambiar la imagen de perfil deberemos pulsar sobre perfil, en la parde de arriba a la izquierda, una vez estemos en la zona de perfil, pulsamos sobre la imagen de perfíl anterior y podremos cambiar la imagen.</p>
    	    	<span style="text-align: center; width: 100%; display: inline-block;"><iframe class="vidios" src="https://www.youtube.com/embed/wwlpgFQOlbI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: inline-block;"></iframe></span>
    	    	<p>No deberán ponerse imágenes que contengan contenido pornográfio, o insultante. El administrador podrá ver ésto y cambiarlo.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(3)'><span id='iconoPlegable3'><i class='fas fa-chevron-circle-right'></i></span> Cambiar la biografía</a></h3>
    	    <span id='ayudaPlegable3' style="display: none;">
    	    	<p>Para cambiar la biografía deberemos ir a la zona de perfíl, y después el texto que queramos en la zona de la biografía (es la que está debajo de la foto), deberemos pulsar en ella y escribir.</p>
    	    	<span style="text-align: center; width: 100%; display: inline-block;"><iframe class="vidios" src="https://www.youtube.com/embed/GnohK0BYsJU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: inline-block;"></iframe></span>
    	    	<p>En la biografía no deberán ponerse palabras malsonantes o hirientes, ten en cuanta que el administrador puede ver esto y eliminarlo cuando quiera.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(4)'><span id='iconoPlegable4'><i class='fas fa-chevron-circle-right'></i></span> Cambiar redes sociales</a></h3>
    	    <span id='ayudaPlegable4' style="display: none;">
    	    	<p>Para cambiar las redes sociales deberemos pulsar sobre el botón perfíl de arriba a la izquierda. Una vez estemos allí deberemos bajar hasta lllegar a dónde pone 
    	    	"Redes sociales". Desplegaremos la opción y podremos ya editar el texto.</p>
    	    	<span style="text-align: center; width: 100%; display: inline-block;"><iframe class="vidios" src="https://www.youtube.com/embed/-hCnbmMw06Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: inline-block;"></iframe></span>
    	    	<p>El Twitter deberá ponerse sin la "@" ya que así se generará un enlace al Twitter oficial de la persona.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(5)'><span id='iconoPlegable5'><i class='fas fa-chevron-circle-right'></i></span> Cambiar de tema</a></h3>
    	    <span id='ayudaPlegable5' style="display: none;">
    	    	<p>Para cambiar el tema de la aplicación deberemos acceder a la zona de ajustes. Una vez allí aplicamos el tema que queramos seleccionando el color. </p>
    	    	<span style="text-align: center; width: 100%; display: inline-block;"><iframe class="vidios" src="https://www.youtube.com/embed/4JHepY_g5yk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: inline-block;"></iframe></span>
    	    	<p>Tras haber realizado la selección del color debemos pulsar el botón guardar.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(6)'><span id='iconoPlegable6'><i class='fas fa-chevron-circle-right'></i></span> Cambiar la contraseña</a></h3>
    	    <span id='ayudaPlegable6' style="display: none;">
    	    	<p>Para cambiar la contraseña deberemos ir a ajustes y en ajuste deberemos bajar a la zona de cambiar contraseña, una vez estemos en ella introduciremos nuestra contraseña actual y luego la nueva contraseña, que habrá que ponerla 2 veces.</p>
    	    	<span style="text-align: center; width: 100%; display: inline-block;"><iframe class="vidios" src="https://www.youtube.com/embed/3MD1jAZ7lKM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: inline-block;"></iframe></span>
    	    	<p>Os recomendamos poner una contraseña robusta, de más de 8 carácteres, y que no esté repetida en otras plataformas.</p>
    	    </span>
    	    <h3><a href='javascript:ayudaPlegable(8)'><span id='iconoPlegable8'><i class='fas fa-chevron-circle-right'></i></span> Consulta al soporte</a></h3>
    	    <span id='ayudaPlegable8' style="display: none;">
    	    	<p>Si quieres abrir un ticket con una incidencia lo que deberás hacer es ir a la parte de ayuda, y después darle al botón en el que pone "Enviar consulta".</p>
    	    	<span style="text-align: center; width: 100%; display: inline-block;"><iframe class="vidios" src="https://www.youtube.com/embed/iIlRDZJzPOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="display: inline-block;"></iframe></span>
    	    	<p>Una vez dentro rellenas los campos con la información solicitada y le das a "Enviar".</p>
    	    </span>
		</div>

		<div id="formConsulta">
			<h1>Consulta al Soporte</h1>
			<p style="text-align: center;">Completa el siguiente formulario para cualquier consulta o problema que tengas con la aplicación. El soporte te contestará via e-mail en cuanto sea posibe.</p>
			<form name="formConsulta" action="../src/controlador/consulta.main.php?opcion=1" method="post">
				<input type="hidden" name="id" value="<?php echo $id; ?>">
				<input type="hidden" name="url" value="<?php echo 'campus/ayuda.php?curso='.$_GET['curso'].'&asignatura='.$_GET['asignatura'] ?>">
				<ul>
					<li><label class="labelForm"><span class="spanForm" style="display: none;"><i class="fas fa-exclamation-circle"></i></span> Título</label></li>
					<li><input type="text" name="titulo" placeholder="Titulo de la consulta" onblur="check_form_vacio(this, 0)"></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm" style="display: none;"><i class="fas fa-exclamation-circle"></i></span> Descripción de la consulta</label></li>
					<li><textarea name="descripcion" placeholder="Describe aqui tu consulta" onblur="check_form_vacio(this, 1)" maxlength="250"></textarea></li>
				</ul>
				<input type="button" onclick="compruebaConsulta()" name="enviar" value="Enviar">
			</form>
		</div>
	</section>
	<?php include 'includes/aside.inc.php';?>
</body>
</html>