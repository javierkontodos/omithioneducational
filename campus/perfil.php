<?php
require 'includes/verificacion.inc.php';
$usuario = new Usuario();
$id = $usuario->obtenerIdPorEmail($_SESSION['user']);
$usuario->obtenerUsuarioPorId($id);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Perfil | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('opcion2').className = "activo";
			redesPlegable();
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
	</script>
</head>
<body>
	<section>
		<div id="tarjetaPerfil">
			<form name="datosPerfil" action="../src/controlador/usuario.main.php?opcion=10" method="post">
				<input type="hidden" name="id" value="<?php echo $usuario->getId() ?>">
				<input type="hidden" name="url" value="<?php echo 'campus/perfil.php?curso='.$_GET['curso'].'&asignatura='.$_GET['asignatura'] ?>">
				<div id="imagenPerfil">
					<div id="cambiaFotoPerfil">
						<p>Cambiar foto</p>
						<i class="fas fa-chevron-down"></i>
					</div>
					<span id="fotoUser" style="background-image: url(<?php echo "../".$usuario->getFoto()?>);" onclick="document.getElementById('selectedFile').click();" onmouseover="imagenHoverPerfil(this, 1)" onmouseout="imagenHoverPerfil(this, 2)"></span>
					<input type="file" id="selectedFile"  name="selectedFile" style="display: none;" onchange="cambiaFotoUsuario()" accept="image/jpeg">
					<input type="hidden" name="foto" id="selectedFoto" value="<?php echo $usuario->getFoto()?>">
					<input type="hidden" name="fotoAntigua" value="<?php echo $usuario->getFoto()?>">
				</div>
				<div id="datosPerfil">
					<h2><?php echo $usuario->getNombre()." ".$usuario->getApellidos(); ?></h2>
					<textarea name="biografia"><?php echo $usuario->getBiografia(); ?></textarea>
				</div>
				<hr>
				<h3>Datos de contacto</h3>
				<p>Estos datos son exclusivos de tu ficha y NO son visibles para otros alumnos.</p>
				<div id="personalPerfil">
					<ul>
						<li><label>Dirección</label></li>
						<li><input type="text" name="direccion" placeholder="Direccion" value="<?php echo ($usuario->getDireccion()!= NULL)?$usuario->getDireccion():"" ?>"></li>
					</ul>
					<ul>
						<li><label>Población</label></li>
						<li><input type="text" name="poblacion" placeholder="Codigo Postal" value="<?php echo ($usuario->getPoblacion()!= NULL)?$usuario->getPoblacion():"" ?>"></li>
					</ul>
					<ul>
						<li><label>Telefono movil</label></li>
						<li><input type="text" name="movil" placeholder="Telefono movil" value="<?php echo ($usuario->getMovil()!= NULL)?$usuario->getMovil():"" ?>"></li>
					</ul>
					<ul>
						<li><label>Otro telefono</label></li>
						<li><input type="text" name="telefono" placeholder="Otro telefono" value="<?php echo ($usuario->getTelefono()!= NULL)?$usuario->getTelefono():"" ?>"></li>
					</ul>
					<input type="submit" name="enviar" value="Guardar">
				</div>
			</form>
		</div>

		<h2 id="redesPlegablePerfil"><a href='javascript:redesPlegable()'><span><i class='fas fa-chevron-circle-down'></i></span> Redes sociales</a></h2>
		<div id="redesPerfil">
			<form name="redesPerfil" action="../src/controlador/usuario.main.php?opcion=11" method="post">
				<input type="hidden" name="id" value="<?php echo $usuario->getId() ?>">
				<input type="hidden" name="url" value="<?php echo 'campus/perfil.php?curso='.$_GET['curso'].'&asignatura='.$_GET['asignatura'] ?>">
				<ul>
					<li><label>Nick de Twitter</label></li>
					<li><input type="text" name="twitter" placeholder="Twitter" value="<?php echo ($usuario->getTwitter()!= NULL)?$usuario->getTwitter():"" ?>"></li>
				</ul>
				<ul>
					<li><label>Enlace de LinkedIn</label></li>
					<li><input type="url" name="linkedin" placeholder="LinkedIn" value="<?php echo ($usuario->getLinkedin()!= NULL)?$usuario->getLinkedin():"" ?>"></li>
				</ul>
				<input type="submit" name="enviar" value="Guardar">
			</form>
		</div>
	</section>
	<?php include 'includes/aside.inc.php';?>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
</body>
</html>