<?php
require 'includes/verificacion.inc.php';
require_once '../src/modelo/temario.class.php';
$usuario = new Usuario();
$id = $usuario->obtenerIdPorEmail($_SESSION['user']);
$usuario->obtenerUsuarioPorId($id);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Temarios | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('icono2').className = "activo";
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
	</script>
</head>
<body>
	<section>
		<h1>Lista de Temarios</h1>
		<?php
			$temarios = new Temario();
			echo $temarios->temarioListCampus($_GET['asignatura'], $_GET['curso']);
		?>
	</section>
	<?php include 'includes/aside.inc.php';?>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
</body>
</html>