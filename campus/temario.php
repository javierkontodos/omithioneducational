<?php
require_once '../src/modelo/usuario.class.php';
require_once '../src/modelo/matricula.class.php';
require_once '../src/modelo/asignatura.class.php';
require_once '../src/modelo/temario.class.php';
require_once '../src/conector/bd.class.php';

session_start();
if(isset($_GET['curso']) && !empty($_GET['curso']) && isset($_GET['asignatura']) && !empty($_GET['asignatura']) && isset($_GET['temario']) && !empty($_GET['temario'])){
	if (!isset($_SESSION['user'])) {
   	    header("location:../login.php");
   	}else if (!Matricula::verificada($_SESSION['user'], $_GET['curso'], $_GET['asignatura'])) {
   		header("location:../index.php");
   	}elseif (Usuario::usuarioAdmin($_SESSION['user'])) {
   	    header("location:../administration/index.php");
   	}elseif (Usuario::usuarioProfesor($_SESSION['user'])) {
   	    header("location:profesorado/index.php");
   	}elseif (!Temario::verificado($_GET['asignatura'], $_GET['temario'])) {
   		header("location:../index.php");
   	}
}else header("location: ../index.php");

$temario = new Temario();
$temario->obtenerTemarioPorId($_GET['temario']);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Temario | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		var sectionSize;
		window.onload = function (){
			document.getElementById('icono2').className = "activo";
			sectionSize = document.getElementsByTagName('section')[0].style.width;
		}
	</script>
</head>
<body>
	<section>
		<div class='tarjetaCampus'>
            <h3><i class='fas fa-bookmark'></i> <?php echo $temario->getNombre(); ?></h3>
            <h5><?php echo $temario->converirFecha($temario->getFecha()); ?></h5>
            <p><?php echo $temario->getContenido(); ?></p>
            <ul>
            	<?php
            		echo $temario->archivosCampus("SELECT archivo FROM Archivo_Temario WHERE id_temario = ".$temario->getId().";");
            	?>
            </ul>
            <button onclick="muestraComentario()">Comentar</button>
        </div>

        <div id="newComentarioCampus">
        	<form name="comentario_form" action="../src/controlador/temario.main.php?opcion=3" method="post">
        		<h2 id="tituloH2">Nuevo comentario</h2>
        		<ul>
        			<li><label id="comentarioLabel">Tu Comentario</label></li>
        			<li>
                        <input type="hidden" name="curso" value="<?php echo $_GET['curso']; ?>">
                        <input type="hidden" name="asignatura" value="<?php echo $_GET['asignatura']; ?>">
                        <input type="hidden" name="temario" value="<?php echo $_GET['temario']; ?>">
        				<textarea name="comentario" placeholder="Escribe aqui tu comentario" maxlength="140"></textarea>
        			</li>
        		</ul>
                <div id="fileForm">
                    <input id="numSelect" type="hidden">
                    <ul>
                        <li id="spanImage1" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName1" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected1" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected1').click()" value="Subir Archivo"></li>
                        <li><input id="archivo1" name="archivo1" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected1" type="file" style="display: none;" onchange="subirArchivoForm(1, 'cambiarArchivoTemario')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage2" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName2" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected2" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected2').click()" value="Subir Archivo"></li>
                        <li><input id="archivo2" name="archivo2" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected2" type="file" style="display: none;" onchange="subirArchivoForm(2, 'cambiarArchivoTemario')"></li>
                    </ul>
                    <ul>
                        <li id="spanImage3" style="text-align: center;">
                            <span style="background-image: url(../images/archivos/nofile.png);"></span>
                        </li>
                        <li id="fileName3" style="text-align: center; font-size: 0.8em;">Archivo no seleccionado</li>
                        <li><input id="botonSelected3" type="button" style="background-color: #C0392B;" onclick="document.getElementById('fileSelected3').click()" value="Subir Archivo"></li>
                        <li><input id="archivo3" name="archivo3" type="hidden" style="display: none;" value=""></li>
                        <li><input id="fileSelected3" type="file" style="display: none;" onchange="subirArchivoForm(3, 'cambiarArchivoTemario')"></li>
                    </ul>
                </div>
        		<input type="button" value="Enviar" onclick="nuevoComentario()">
        	</form>
        </div>

        <h2 id="tituloH2"><i class="fas fa-comments"></i> Comentarios</h2>

        <?php
            echo $temario->temarioComentariosListCampus($temario->getId(), $_SESSION['user']);
        ?>

	</section>
	<?php include 'includes/aside.inc.php';?>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
</body>
</html>