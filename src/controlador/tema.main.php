<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/tema.class.php';
require_once '../modelo/usuario.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		tema_form();
		break;
	case 2:
		eliminar_tema();
		break;
	case 3:
		cambiar_tema_usuario();
		break;
	case 4:
		comprar_tema();
		break;
	
	default:
		# code...
		break;
}

function tema_form(){
	$tema = new Tema();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
    		$tema->nuevoTema($_POST);
			header("location:../../administration/temasList.php");
		}else{
			$tema->editarTema($_POST);
			header("location:../../administration/temasList.php");
	    }
	}else{
		header("location:../../administration/temasForm.php");
	}
}

function eliminar_tema() {
	if ($_GET["id_tema"] != "") {
		$tema = new Tema();
		$tema->eliminarTema($_GET["id_tema"]);
   		header("location:../../administration/temasList.php");
	}else{
   		header("location:../../administration/temasList.php");
	}
}

function cambiar_tema_usuario(){
    session_start();
	$tema = new Tema();
	if (isset($_POST) && !empty($_POST)) {
		$tema->cambiarTemaUsuario($_POST['tema'], $_SESSION['user']);
	}
	header("location:../../".$_POST['url']);
}

function comprar_tema(){
    session_start();
	$tema = new Tema();

	if (isset($_POST) && !empty($_POST)) {
		$tema->comprarTema($_POST['tema'], $_SESSION['user']);
		$tema->cambiarTemaUsuario($_POST['tema'], $_SESSION['user']);
	}
	header("location:../../".$_POST['url']);
}

?>