<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/usuario.class.php';
require_once '../modelo/temario.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		temario_form();
		break;
	case 2:
		eliminar_temario();
		break;
	case 3:
		comentario_temario();
		break;
	
	default:
		# code...
		break;
}

function temario_form() {
	$temario = new Temario();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
    		$temario->nuevoTemario($_POST);
    		header("location:../../".$_POST['url']);
		}else{
			$temario->editarTemario($_POST);
			header("location:../../".$_POST['url']);
	    }
	}else{
		header("location:../../administration/temariosForm.php");
	}
}

function eliminar_temario() {
	if ($_GET["id_temario"] != "") {
		$temario = new Temario();
		$temario->eliminarTemario($_GET["id_temario"]);
		header("location:../../".$_GET['url']);
	}else{
		header("location:../../".$_GET['url']);
	}
}

function comentario_temario() {
	session_start();
	$temario = new Temario();
	if (isset($_POST) && !empty($_POST)) {
    	$temario->nuevoComentario($_POST, $_SESSION['user']);
		header("location:../../campus/temario.php?curso=".$_POST['curso']."&asignatura=".$_POST['asignatura']."&temario=".$_POST['temario']."");
	}else{
		header("location:../../index.php");
	}
}

?>