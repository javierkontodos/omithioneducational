<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/curso.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		curso_form();
		break;
	case 2:
		eliminar_curso();
		break;
	
	default:
		# code...
		break;
}

function curso_form(){
	$curso = new Curso();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
			if(!$curso->registrado($_POST["nombre"])){
    			$curso->nuevoCurso($_POST);
				header("location:../../administration/cursosList.php");
	    	}else{
				header("location:../../administration/cursosForm.php");
	    	}
		}else{
			$curso->editarCurso($_POST);
			header("location:../../administration/cursosList.php");
	    }
	}else{
		header("location:../../administration/cursosForm.php");
	}
}

function eliminar_curso() {
	if ($_GET["id_curso"] != "") {
		$curso = new Curso();
		$curso->eliminarCurso($_GET["id_curso"]);
   		header("location:../../administration/cursosList.php");
	}else{
   		header("location:../../administration/cursosList.php");
	}
}

?>