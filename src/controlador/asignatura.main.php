<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/asignatura.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		asignatura_form();
		break;
	case 2:
		eliminar_asignatura();
		break;
	
	default:
		# code...
		break;
}

function asignatura_form(){
	$asignatura = new Asignatura();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
    		$asignatura->nuevaAsignatura($_POST);
			header("location:../../administration/asignaturasList.php");
		}else{
			$asignatura->editarAsignatura($_POST);
			header("location:../../administration/asignaturasList.php");
	    }
	}else{
		header("location:../../administration/asignaturasForm.php");
	}
}

function eliminar_asignatura() {
	if ($_GET["id_asignatura"] != "") {
		$asignatura = new Asignatura();
		$asignatura->eliminarAsignatura($_GET["id_asignatura"]);
   		header("location:../../administration/asignaturasList.php");
	}else{
   		header("location:../../administration/asignaturasList.php");
	}
}


?>