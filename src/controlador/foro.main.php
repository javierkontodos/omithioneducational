<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/usuario.class.php';
require_once '../modelo/foro.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		foro_form();
		break;
	case 2:
		eliminar_foro();
		break;
	case 3:
		comentario_foro();
		break;
	
	default:
		# code...
		break;
}

function foro_form() {
	session_start();
	$foro = new Foro();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
    		$foro->nuevoForo($_POST, $_SESSION['user']);
    		header("location:../../".$_POST['url']);
		}else{
			$foro->editarForo($_POST);
			header("location:../../".$_POST['url']);
	    }
	}else{
		header("location:../../administration/forosForm.php");
	}
}

function eliminar_foro() {
	if ($_GET["id_foro"] != "") {
		$foro = new Foro();
		$foro->eliminarForo($_GET["id_foro"]);
		header("location:../../".$_GET['url']);
	}else{
		header("location:../../".$_GET['url']);
	}
}

function comentario_foro() {
	session_start();
	$foro = new Foro();
	if (isset($_POST) && !empty($_POST)) {
    	$foro->nuevoComentario($_POST, $_SESSION['user']);
		header("location:../../campus/foro.php?curso=".$_POST['curso']."&asignatura=".$_POST['asignatura']."&foro=".$_POST['foro']."");
	}else{
		header("location:../../index.php");
	}
}

?>