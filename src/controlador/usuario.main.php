<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/usuario.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		iniciar_sesion();
		break;
	case 2:
		cerrar_sesion();
		break;
	case 3:
		nuevo_usuario();
		break;
	case 4:
		cambiar_email();
		break;
	case 5:
		enviar_verificacion();
		header("location:../../index.php");
		break;
	case 6:
		comprobar_codigo();
		break;
	case 7:
		completa_perfil();
		break;
	case 8:
		eliminar_usuario();
		break;
	case 9:
		usuario_form();
		break;
	case 10:
		editar_datos_perfil();
		break;
	case 11:
		editar_redes_perfil();
		break;
	case 12:
		recuperar_password();
		break;
	case 13:
		nueva_password();
		break;
	case 14:
		cambiar_password();
		break;

	default:
		# code...
		break;
}

function iniciar_sesion(){
	if(Usuario::login($_POST)){
		session_start();
		$_SESSION["user"]=$_POST["email"];
   		echo "true";
	}else{
    	echo "false";
	}
}

function cerrar_sesion(){
    session_start();
	//destruimos la sesion abierta
    session_destroy();
    //mandamos al ususario al login
    header("location:../../login.php");
}

function nuevo_usuario(){
	$usuario = new Usuario();

	if (isset($_POST) && !empty($_POST)) {
		if($usuario->registrado($_POST["email"])){
    		echo "false";
	    }else{
			$usuario->crearUsuario($_POST);
			session_start();
			$_SESSION["user"]=$_POST["email"];
			enviar_verificacion();
			echo "true";
	    }
	}
}

function cambiar_email(){

	$usuario = new Usuario();

	if ($_GET["email"] != "") {
		if ($usuario->comprueba_email($_GET["email"])) {
			session_start();
			$usuario->cambiar_email($_SESSION["user"], $_GET["email"]);
			$_SESSION["user"] = $_GET["email"];
			$usuario->nuevaVerificacion($_SESSION["user"]);
			echo "true";
		}else{
	    	echo "erroneo";
	    }
	}else{
		echo "vacio";
	}
}

function enviar_verificacion(){
	session_start();

	$usuario = new Usuario();
	$usuario->nuevaVerificacion($_SESSION["user"]);
}

function comprobar_codigo(){
	session_start();

	$usuario = new Usuario();
	if($usuario->comprobarCodigo($_SESSION["user"], $_GET["codigo"])){
		//si es correcto llamo a una funcion para poner a true el campo de verificacion en el usuario
		$usuario->verificacionCorrecta($_SESSION["user"]);
		echo "true";
	}else{
		//si esta mal el codigo le mostrare al usuario por pantalla que lo tiene mal o que se ha caducado, y le dare la opcion de que se lo vuelvan a enviar o que cambie el email (esto por texto plano, que para algo ya tiene estas opciones en la pantalla de verificacion)
		echo "false";
	}
}

function completa_perfil(){
	session_start();
	$usuario = new Usuario();

	if (isset($_POST) && !empty($_POST)) {
		if(!Usuario::perfilCompletado($_SESSION["user"])){
			$usuario->completaPerfil($_POST, $_SESSION["user"]);
	    }
		header("location:../../index.php");
	}
}

function eliminar_usuario() {
	if ($_GET["id_usuario"] != "") {
		$usuario = new Usuario();
		$usuario->eliminarUsuario($_GET["id_usuario"]);
   		header("location:../../administration/usuariosList.php");
	}else{
   		header("location:../../administration/usuariosList.php");
	}
}

function usuario_form(){
	$usuario = new Usuario();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
			if(!$usuario->registrado($_POST["email"])){
    			$usuario->nuevoUsuario($_POST);
				header("location:../../administration/usuariosList.php");
	    	}else{
				header("location:../../administration/usuariosForm.php");
	    	}
		}else{
			$usuario->editarUsuario($_POST);
			header("location:../../administration/usuariosList.php");
	    }
	}else{
		header("location:../../administration/usuariosForm.php");
	}
}

function editar_datos_perfil(){
	$usuario = new Usuario();
	if (isset($_POST) && !empty($_POST)) {
		$usuario->editarDatosPerfil($_POST);
	}
	header("location:../../".$_POST['url']);
}

function editar_redes_perfil(){
	$usuario = new Usuario();
	if (isset($_POST) && !empty($_POST)) {
		$usuario->editarRedesPerfil($_POST);
	}
	header("location:../../".$_POST['url']);
}

function recuperar_password(){
	$usuario = new Usuario();
	if (isset($_POST) && !empty($_POST)) {
		$usuario->recuperarPassword($_POST);
	}
	header("location:../../login.php");
}

function nueva_password(){
	session_start();

	$usuario = new Usuario();
	if($usuario->comprobarRecuperacion($_POST["id"], $_POST["code"])){
		$usuario->recuperacionCorrecta($_POST["id"], $_POST["pass"]);
		echo "true";
	}else{
		echo "false";
	}
}

function cambiar_password(){
	session_start();
	$usuario = new Usuario();

	if (isset($_POST) && $_POST["currentPass"] != "" && $_POST["newPass"] != "" && $_POST["repeatPass"] != "") {
		if ($usuario->passCorrecta($_SESSION["user"], $_POST["currentPass"])) {
			if ($_POST["newPass"] == $_POST["repeatPass"]) {
				$usuario->cambiarPassword($_SESSION["user"], $_POST["newPass"]);
			}else{
				echo "coincidencia";
			}
		}else{
			echo "error";
		}
	}else{
		echo "false";
	}
}

?>