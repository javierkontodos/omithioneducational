<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/usuario.class.php';
require_once '../modelo/actividad.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		actividad_form();
		break;
	case 2:
		eliminar_actividad();
		break;
	case 3:
		comentario_actividad();
		break;
	
	default:
		# code...
		break;
}

function actividad_form(){
	$actividad = new Actividad();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
    		$actividad->nuevaActividad($_POST);
			header("location:../../".$_POST['url']);
		}else{
			$actividad->editarActividad($_POST);
			header("location:../../".$_POST['url']);
	    }
	}else{
		header("location:../../administration/actividadesForm.php");
	}
}

function eliminar_actividad() {
	if ($_GET["id_actividad"] != "") {
		$actividad = new Actividad();
		$actividad->eliminarActividad($_GET["id_actividad"]);
   		header("location:../../".$_GET['url']);
	}else{
   		header("location:../../".$_GET['url']);
	}
}

function comentario_actividad() {
	session_start();
	$actividad = new Actividad();
	if (isset($_POST) && !empty($_POST)) {
    	$actividad->nuevoComentario($_POST, $_SESSION['user']);
		header("location:../../campus/actividad.php?curso=".$_POST['curso']."&asignatura=".$_POST['asignatura']."&actividad=".$_POST['actividad']."");
	}else{
		header("location:../../index.php");
	}
}

?>