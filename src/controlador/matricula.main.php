<?php
require_once "../captcha/recaptchalib.php";
require_once '../modelo/encrypter.class.php';
require_once '../modelo/matricula.class.php';
require_once '../conector/bd.class.php';
require_once '../gestorMail/phpmailer.class.php';
require_once '../gestorMail/smtp.class.php';
require_once '../gestorMail/exception.class.php';
require_once '../gestorMail/gestorMail.class.php';

switch ($_GET["opcion"]) {
	case 1:
		nueva_matricula();
		break;
	case 2:
		eliminar_matricula();
		break;
	case 3:
		matricula_form();
		break;
	case 4:
		cambiar_notificaciones();
		break;

	default:
		# code...
		break;
}

function nueva_matricula(){
	$matricula = new Matricula();

	if (isset($_POST) && !empty($_POST)) {
		$matricula->nuevaMatricula($_POST);
	}
	header("location:../../index.php");
}

function eliminar_matricula() {
	if ($_GET["id_matricula"] != "") {
		$matricula = new Matricula();
		$matricula->eliminarMatricula($_GET["id_matricula"]);
   		header("location:../../administration/matriculasList.php");
	}else{
   		header("location:../../administration/matriculasList.php");
	}
}

function matricula_form(){
	$matricula = new Matricula();
	if (isset($_POST) && !empty($_POST)) {
		if ($_POST["id"] == "0") {
			if(!$matricula->registrada($_POST)){
    			$matricula->nuevaMatricula($_POST);
				header("location:../../administration/matriculasList.php");
	    	}else{
				header("location:../../administration/matriculasForm.php");
	    	}
		}else{
			$matricula->editarMatricula($_POST);
			header("location:../../administration/matriculasList.php");
	    }
	}else{
		header("location:../../administration/matriculasForm.php");
	}
}

function cambiar_notificaciones(){
	$matricula = new Matricula();

	if (isset($_GET) && !empty($_GET)) {
		$matricula->cambiarNotificaciones($_GET);
		echo "true";
	}else{
		echo "false";
	}
}

?>