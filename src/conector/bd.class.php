<?php

class Bd{

    private $server = "localhost:3306";
    private $usuario = "admin_educa";
    private $pass = "123456";
    private $basedatos = "admin_educational";
    private $conexion;
	  private $resultado;
	  private $resultado2;

  public function __construct(){ 
           
       $this->conexion = new mysqli($this->server, $this->usuario, $this->pass, $this->basedatos);
       $this->conexion->select_db($this->basedatos);
       $this->conexion->query("SET NAMES 'utf8'");
        
    
  }
  
  
  public function consultaSimple($consulta){ 
    //echo "el sql".$consulta ."fin";
    $this->resultado =   $this->conexion->query($consulta);
    $num = $this->numeroElementos();
    
    if($num>0){
       $res = mysqli_fetch_assoc($this->resultado);
    }else{
        $res = false;
    }
    return $res;
  }

    public function consulta($consulta){ 
    //echo $consulta;
    $this->resultado = $this->conexion->query($consulta);
     $res = $this->resultado ;
    return $res;
  }
  
  public function query($sql){
       $res = $this->conexion->query($sql);
       return $res;
  }




  public function numeroElementos(){
       
        $num = $this->resultado->num_rows;
        return $num;
        
   }
   
     public function numeroElementosConSql($sql){
       
    $this->resultado = $this->conexion->query($sql);
    $num = $this->numeroElementos();
    return $num;
        
   }

   public function nombreDB(){
       
       return $this->basedatos;
   }

   
    
  public function insertarElemento($tabla, $datos, $foto = 0, $directorio = ""){ 
   
        $claves  = array();
        $valores = array();
        
        foreach ($datos as $clave => $valor){
            $claves[] = $clave;
            $valores[] = "'".$valor."'";
        }
        
        if($foto != 0){
            $ruta = subirFoto($foto, $directorio);
            $claves[] = "ruta";
            $valores[] = "'".$ruta."'";
        }
        
        
        $sql = "INSERT ".$tabla." (".implode(',', $claves).") VALUES  (".implode(',', $valores).")";
     
        $this->resultado =   $this->conexion->query($sql);
        $res = $this->resultado; 
        return $res;
  }
}


?>