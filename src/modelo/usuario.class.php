<?php

/**
* Esta es la clase usuario
* @Author OmithionEducational
* @Version 0.1
*/
class Usuario {
	//variables de la clase
    private $id;
    private $admin;
    private $profesor;
    private $verificado;
    private $nombre;
    private $apellidos;
    private $fecha_nacimiento;
    private $email;
    private $dni;
    private $pass;//la contraseña
    private $direccion;
    private $cod_postal;
    private $poblacion;
    private $provincia;
    private $pais;
    private $telefono;
    private $movil;
    private $biografia;
    private $foto;
    private $twitter;
    private $linkedin;
    private $monedas;
    private $id_tema_actual;
    private $id_icono_actual;

    //Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
    public function getId(){ return $this->id; }
    public function setId($id){ $this->id = $id; }

    //Getter y Setter del Boolean Admin
    public function getAdmin(){ return $this->admin; }
    public function setAdmin($admin){ $this->admin = $admin; }

    //Getter y Setter del Boolean Profesor
    public function getProfesor(){ return $this->profesor; }
    public function setProfesor($profesor){ $this->profesor = $profesor; }

    //Getter y Setter del Boolean Verificado
    public function getVerificado(){ return $this->verificado; }
    public function setVerificado($verificado){ $this->verificado = $verificado; }

    //Getter y Setter del String Nombre
    public function getNombre(){ return $this->nombre; }
    public function setNombre($nombre){ $this->nombre = addslashes($nombre); }

    //Getter y Setter del String Apellidos
    public function getApellidos(){ return $this->apellidos; }
    public function setApellidos($apellidos){ $this->apellidos = addslashes($apellidos); }

    //Getter y Setter del Date Fecha_Nacimiento
    public function getFecha_nacimiento(){ return $this->fecha_nacimiento; }
    public function setFecha_nacimiento($fecha_nacimiento){ $this->fecha_nacimiento = $fecha_nacimiento; }

    //Getter y Setter del String Email
    public function getEmail(){ return $this->email; }
    public function setEmail($email){ $this->email = addslashes($email); }

    //Getter y Setter del String Dni
    public function getDni(){ return $this->dni; }
    public function setDni($dni){ $this->dni = addslashes($dni); }

    //Getter y Setter del String Pass
    public function getPass(){ return $this->pass; }
    public function setPass($pass){ $this->pass = password_hash($pass, PASSWORD_DEFAULT, array("cost"=>12)); }

    //Getter y Setter del String Direccion
    public function getDireccion(){ return $this->direccion; }
    public function setDireccion($direccion){ $this->direccion = addslashes($direccion); }

    //Getter y Setter del String Cod_Postal
    public function getCod_postal(){ return $this->cod_postal; }
    public function setCod_postal($cod_postal){ $this->cod_postal = addslashes($cod_postal); }

    //Getter y Setter del String Poblacion
    public function getPoblacion(){ return $this->poblacion; }
    public function setPoblacion($poblacion){ $this->poblacion = addslashes($poblacion); }

    //Getter y Setter del String Provincia
    public function getProvincia(){ return $this->provincia; }
    public function setProvincia($provincia){ $this->provincia = addslashes($provincia); }

    //Getter y Setter del String Pais
    public function getPais(){ return $this->pais; }
    public function setPais($pais){ $this->pais = addslashes($pais); }

    //Getter y Setter del String Telefono
    public function getTelefono(){ return $this->telefono; }
    public function setTelefono($telefono){ $this->telefono = addslashes($telefono); }

    //Getter y Setter del String Movil
    public function getMovil(){ return $this->movil; }
    public function setMovil($movil){ $this->movil = addslashes($movil); }

    //Getter y Setter del String Biografia
    public function getBiografia(){ return $this->biografia; }
    public function setBiografia($biografia){ $this->biografia = addslashes($biografia); }

    //Getter y Setter del String Foto
    public function getFoto(){ return $this->foto; }
    public function setFoto($foto){ $this->foto = $foto; }

    //Getter y Setter del String Twitter
    public function getTwitter(){ return $this->twitter; }
    public function setTwitter($twitter){ $this->twitter = $twitter; }

    //Getter y Setter del String Linkedin
    public function getLinkedin(){ return $this->linkedin; }
    public function setLinkedin($linkedin){ $this->linkedin = $linkedin; }

    //Getter y Setter del Integer Monedas
    public function getMonedas(){ return $this->monedas; }
    public function setMonedas($monedas){ $this->monedas = $monedas; }

    //Getter y Setter del Integer Id_Tema_Actual
    public function getId_tema_actual(){ return $this->id_tema_actual; }
    public function setId_tema_actual($id_tema_actual){ $this->id_tema_actual = $id_tema_actual; }

    //Getter y Setter del Integer Id_Icono_Actual
    public function getId_icono_actual(){ return $this->id_icono_actual; }
    public function setId_icono_actual($id_icono_actual){ $this->id_icono_actual = $id_icono_actual; }

    /**
    * Recoge los datos del formulario y crea un nuevo objeto usuario, despues llama a la funcion addUsuarioBD la cual lo añadira a la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function crearUsuario($datos){
        $this->setNombre($datos['nombre']);
        $this->setApellidos($datos['apellidos']);
        $this->setFecha_nacimiento($datos['fecha']);
        $this->setEmail($datos['email']);
        $this->setDni($datos['dni']);
        $this->setPass($datos['pass']);

        $this->addUsuarioBD();
    }

    /**
    * Basandose en el objeto actual, agrega una nueva entrada de un usuario en la base de datos.
    */
    private function addUsuarioBD(){
        $sql = "INSERT INTO Usuario(nombre, apellidos, email, dni, fecha_nacimiento, pass, foto, verificado) VALUES ('".$this->nombre."', '".$this->apellidos."', '".$this->email."', '".md5($this->dni)."', '".$this->fecha_nacimiento."', '".$this->pass."', 'images/usuarios/plantilla.png', 0);";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Devolvera true o false en base a si la contraseña que recibe la funcion mediante un formulario de login es la misma que en la base de datos.
    * @param Array $datos contenido del POST del formulario
    * @return Boolean $ok true o false
    */
    public static function login($datos){
        $ok = false;

        $sql = "SELECT pass FROM Usuario WHERE email='".$datos['email']."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if(password_verify($datos['pass'], $res['pass'])){
            $ok = true;
        }
        return $ok;
    }

    /**
    * Devolvera true o false en base a si el correo que recibe la funcion mediante un formulario de registro esta siendo o no usado ya en la base de datos.
    * @param String $email email de usuario
    * @return Boolean $ok true o false
    */
    public function registrado($email){
        $ok = false;
        $sql = "SELECT count(id) as correos FROM Usuario WHERE email='".$email."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if ($res['correos'] != 0) {
            $ok = true;
        }
        return $ok;
    }

    /**
    * Devolvera true o false en base a si el correo que recibe la funcion mediante la variable de email esta verificado o no en la base de datos.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function verificado($email){
        $ok = false;

        $sql = "SELECT verificado FROM Usuario WHERE email='".$email."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if($res['verificado'] != 0){
            $ok = true;
        }
        return $ok;
    }

    /**
    * Devolvera true o false en base a si el correo que recibe la funcion mediante la variable de email esta verificado o no en la base de datos.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function perfilCompletado($email){
        $ok = false;
        $conexion = new Bd();

        $sql = "SELECT id FROM Usuario WHERE email='".$email."';";
        $res = $conexion->consultaSimple($sql);
        $id = $res['id'];

        $sql = "SELECT direccion, movil FROM Usuario WHERE id='".$id."';";
        $res = $conexion->consultaSimple($sql);

        if($res['direccion'] != NULL && $res['movil'] != NULL){
            $ok = true;
        }

        return $ok;
    }

    /**
    * Devolvera true o false en base a si el usuario es administrador o no.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function usuarioAdmin($email){
        $ok = false;
        $conexion = new Bd();

        $sql = "SELECT id FROM Usuario WHERE email='".$email."';";
        $res = $conexion->consultaSimple($sql);
        $id = $res['id'];

        $sql = "SELECT admin FROM Usuario WHERE id='".$id."';";
        $res = $conexion->consultaSimple($sql);

        if($res['admin'] != 0){
            $ok = true;
        }
        return $ok;
    }

    /**
    * Devolvera true o false en base a si el usuario es profesor o no.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function usuarioProfesor($email){
        $ok = false;
        $conexion = new Bd();

        $sql = "SELECT id FROM Usuario WHERE email='".$email."';";
        $res = $conexion->consultaSimple($sql);
        $id = $res['id'];

        $sql = "SELECT profesor FROM Usuario WHERE id='".$id."';";
        $res = $conexion->consultaSimple($sql);

        if($res['profesor'] != 0){
            $ok = true;
        }
        return $ok;
    }

    /**
    * Comprobara que el correo introducido sea valido o que no exista en la base de datos.
    * @param String $email email nuevo para el usuario
    * @return Boolean true o false
    */
    public function comprueba_email($email){
        $ok = false;
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $ok = !$this->registrado($email);
        }
        return $ok;
    }

    /**
    * Cambiara el correo en la base de datos por el que tuviese anteriormente.
    * @param String $email email actual del usuario
    * @param String $nuevo email nuevo para el usuario
    */
    public function cambiar_email($email, $nuevo){
        $id = $this->obtenerIdPorEmail($email);

        $sql = "UPDATE Usuario SET email='".$nuevo."'WHERE id='".$id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Se generara un codigo y una fecha y hora limite de uso para que asi pueda el usuario verificar su correo
    * @param String $email sera el email del usuario conectado a la aplicacion
    */
    public function nuevaVerificacion($email){
        $fecha = strtotime ('+10 minute', strtotime(date('Y-m-j H:i:s')));
        $fecha = date ('Y-m-j H:i:s', $fecha);
        $codigo = strtoupper(substr(sha1(microtime()),0,5));
        $id = $this->obtenerIdPorEmail($email);

        $this->borrarVerificacion($id);

        $sql = "INSERT INTO Verificacion(id_usuario, codigo, fecha_hora, recuperar) VALUES ('".$id."', '".$codigo."', '".$fecha."', 0);";
        $conexion = new Bd();
        $conexion->consulta($sql);

        $this->enviarCodigo($email, $codigo);
    }

    /**
    * Mediante la id de un usuario, borra cualquier entrada que disponga este dentro de la tabla de verificacion
    * @param Integer $id numero de id del usuario
    */
    public function borrarVerificacion($id) {
        $conexion = new Bd();

        $sql = "DELETE FROM Verificacion WHERE id_usuario='".$id."' AND recuperar = 0;";
        $conexion->consulta($sql);
    }

    /**
    * Mediante la id de un usuario, borra cualquier entrada que disponga este dentro de la tabla de verificacion
    * @param Integer $id numero de id del usuario
    */
    public function borrarRecuperacion($id) {
        $conexion = new Bd();

        $sql = "DELETE FROM Verificacion WHERE id_usuario='".$id."' AND recuperar = 1;";
        $conexion->consulta($sql);
    }

    /**
    * Envia por correo mediante PHPMailer el codigo de verificacion al correo del usuario.
    * @param String $email variable email con el email del usuario
    * @param String $codigo variable codigo con el codigo de verificacion
    */
    public function enviarCodigo($email, $codigo){
        $correo = new GestorMail();

        $tumensaje = "<div style='width: 76%; margin: 2% 10%; text-align: center; font-family: arial; font-weight: bold; box-shadow: 0 0 1em #797D7F; padding: 2%; border-radius: 10px; color: black;'>
            <a href='http://omithioneducational.com/'><img style='width: 40%;' src='http://omithioneducational.com/images/logo.png'></a>
            <p style='font-size: 1.25em;'>Aquí tienes el código de verificacion de tu cuenta en Omithion Educational:</p>
            <h2 style='font-size: 3.5em; color: #152770;'>".$codigo."</h2>
            <p style='font-size: 1.20em;'>Como medida de seguridad, el código caducará en 10 minutos.</p>
            <hr style='height:1px;border:none;color:#333;background-color:#ddd;'>
            <a href='https://twitter.com/Omithion'><img style='width: 50px; margin: 5px;' src='http://omithiontech.com/images/twitter.png'></a>
            <p>© 2019 Omithion Educational. Todos los derechos reservados.</p>
            </div>";

        $correo->enviar($email," Código de verificación de tu cuenta en Omithion Educational", $tumensaje);
    }

    /**
    * Devolvera true o false en base a si la hora de la generacion del codigo de verificacion y si el codigo es correcto
    * @param String $email sera el email del usuario
    * @param String $codigo es el codigo que ha introducido el usuario
    * @return Boolean true o false
    */
    public function comprobarCodigo($email, $codigo) {
    	$id = $this->obtenerIdPorEmail($email);
		$sql = "SELECT codigo, fecha_hora FROM Verificacion WHERE id_usuario='".$id."' AND recuperar=0;";

		$conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        return ($res['codigo'] == $codigo && $this->compruebaFecha($res['fecha_hora']));
    }

    /**
    * Devolvera true o false en base a si la hora acutal del sistema a superado o no a la hora limite de uso del codigo de verificacion
    * @param datetime $fecha sera la fecha en la que se genero el codigo en la bbdd
    * @return Boolean true o false
    */
    public function compruebaFecha($fecha){
        return date('Y-m-d H:i:s') < $fecha;
    }

    /**
    * Una vez el codigo se ha verificado se cambiara en la base de datos el estado de la verificacion del usuario
    * @param String $email sera el correo del usuario
    */
    public function verificacionCorrecta($email) {
        $conexion = new Bd();

        $id = $this->obtenerIdPorEmail($email);
        
        $sql = "UPDATE Usuario SET verificado='1' WHERE id='".$id."';";
        $conexion->consulta($sql);
    }

    /**
    * Devolvera la id de usuario en base a si el correo que recibe la funcion mediante la variable de sesion.
    * @param String $email variable email con el email del usuario
    * @return Integer $res['id'] devolvera la id
    */
    public function obtenerIdPorEmail($email){
        $sql = "SELECT id FROM Usuario WHERE email='".$email."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        return $res['id'];
    }

    /**
    * Actualizara los datos restantes por completar del usuario para que este este completado en su totalidad.
    * @param Array $datos contenido del POST del formulario
    * @param String $email variable email con el email del usuario
    */
    public function completaPerfil($datos, $email) {
        $id = $this->obtenerIdPorEmail($email);

        if (isset($datos['telefono'])) {
            $telefono = $datos['telefono'];
        }else{
             $telefono = null;
        }

        $sql = "UPDATE Usuario SET direccion='".$datos['direccion']."',cod_postal='".$datos['cod_postal']."',poblacion='".$datos['poblacion']."',provincia='".$datos['provincia']."',pais='".$datos['pais']."',telefono='".$telefono."',movil='".$datos['movil']."',biografia='".$datos['biografia']."',foto='".$datos['foto']."',id_tema_actual='".$datos['tema']."' WHERE id='".$id."';";

        $conexion = new Bd();
        $conexion->consulta($sql);

        $sql = "INSERT INTO Tema_Usuario(id_usuario, id_tema) VALUES (".$id.", ".$datos['tema'].")";
        $conexion->consulta($sql);
    }

    /**
    * Devolvera la ruta del archivo css asigada al tema que tenga el usuario establecido.
    * @param String $email variable email con el email del usuario
    * @return String $res['ruta'] devolvera la ruta del archivo css
    */
    public static function temaUsuario($email) {
        $conexion = new Bd();

        $sql = "SELECT id_tema_actual FROM Usuario WHERE email='".$email."';";
        $res = $conexion->consultaSimple($sql);
        $id = $res['id_tema_actual'];

        $sql = "SELECT ruta FROM Tema WHERE id='".$id."';";
        $res = $conexion->consultaSimple($sql);

        return $res['ruta'];
    }

    /**
    * Listara los usuarios de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de los usuarios
    */
    public function listarUsuarios() {
        $sql = "SELECT id, foto, nombre, apellidos, biografia, admin, profesor, verificado, email FROM Usuario;";
       
        $txt = $this->seleccionarUsuarios($sql);

        return $txt;
    }

    /**
    * Listara los usuarios de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $usuario es el usuario a buscar en la base de datos
    * @return String $txt devolvera la vista de los usuarios
    */
    public function buscarUsuarios($usuario) {
       $sql = "SELECT id, foto, nombre, apellidos, biografia, admin, profesor, verificado, email FROM Usuario WHERE nombre LIKE '%".$usuario."%' OR apellidos LIKE '%".$usuario."%';";

       $txt = $this->seleccionarUsuarios($sql);

       return $txt;
    }

    /**
    * Listara los usuarios de la base de datos con el sql que recibe.
    * @param String $sql es el sql de la peticion
    * @return Array $listaUsuario devolvera un array con los usuarios
    */
    public function consultarUsuarios($sql) {
        $listaUsuario = array();

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id, $foto, $nombre, $apellidos, $biografia, $admin, $profesor, $verificado, $email) = mysqli_fetch_array($res)) {
            $usuario = new Usuario();
            $usuario->llenarUsuario($id, $foto, $nombre, $apellidos, $biografia, $admin, $profesor, $verificado, $email);
            array_push($listaUsuario, $usuario);
        }

        return $listaUsuario;
    }

    /**
    * Listara los usuarios de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de los usuarios
    */
    public function seleccionarUsuarios($sql) {
        $listaUsuario = array();
        $listaUsuario = $this->consultarUsuarios($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaUsuario);$i++){
            $txt .= $this->tarjetaUsuario($listaUsuario[$i]);
        }

        return $txt;
    }

    /**
    * Listara los options con los profesores de la base de datos.
    * @param Integer $profesor es la id del profesor
    * @return String $txt devolvera la vista de los profesores en options
    */
    public function selectorProfesores($profesor) {
        $sql = "SELECT id, foto, nombre, apellidos, biografia, admin, profesor, verificado FROM Usuario WHERE admin <> 1 AND profesor <> 0 AND verificado <> 0;";

        $listaProfesores = array();
        $listaProfesores = $this->consultarUsuarios($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaProfesores);$i++){
            $txt .= "<option value='".$listaProfesores[$i]->getId()."' ".strval(($listaProfesores[$i]->getId() == $profesor)?'selected':'').">".$listaProfesores[$i]->getNombre()." ".$listaProfesores[$i]->getApellidos()."</option>";
        }

        return $txt;
    }

    /**
    * Listara los options con los alumnos de la base de datos.
    * @param Integer $alumno es la id del alumno
    * @return String $txt devolvera la vista de los alumnos en options
    */
    public function selectorAlumnos($alumno) {
        $sql = "SELECT id, foto, nombre, apellidos, biografia, admin, profesor, verificado FROM Usuario WHERE admin <> 1 AND profesor <> 1 AND verificado <> 0;";

        $listaAlumnos = array();
        $listaAlumnos = $this->consultarUsuarios($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaAlumnos);$i++){
            $txt .= "<option value='".$listaAlumnos[$i]->getId()."' ".strval(($listaAlumnos[$i]->getId() == $alumno)?'selected':'').">".$listaAlumnos[$i]->getNombre()." ".$listaAlumnos[$i]->getApellidos()."</option>";
        }

        return $txt;
    }

    /**
    * Rellenara los campos de un usuario con los datos que recibe del mismo.
    * @param Integer $id
    * @param String $foto 
    * @param String $nombre
    * @param String $apellidos
    * @param String $biografia
    * @param Boolean $admin
    * @param Boleean $profesor
    * @param Boolean $verificado
    */
    public function llenarUsuario($id, $foto, $nombre, $apellidos, $biografia, $admin, $profesor, $verificado, $email){
        $this->id = $id;
        $this->foto = $foto;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->biografia = $biografia;
        $this->admin = $admin;
        $this->profesor = $profesor;
        $this->verificado = $verificado;
        $this->email = $email;
    }

    /**
    * Devolvera la vista de todos los usuarios de la base de datos.
    * @param String $usuario recibe un usuario
    * @return String $txt devolvera la vista de los usuarios
    */
    public function tarjetaUsuario($usuario) {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='imagenContainer'><span style='background-image: url(../".$usuario->getFoto().");''></span></div>";

        $txt .= "<div class='datosContainer'><p><b>".$usuario->getNombre()." ".$usuario->getApellidos()."</b></p><p>".$usuario->getBiografia()."</p>";

        if ($usuario->getAdmin() == 0) {
            $txt .= "<p style='margin-bottom: 0; text-transform: uppercase;'><span style='color: #C0392B;''><i class='fas fa-times-circle'></i></span> - Administrador</p>";
        }else{
            $txt .= "<p style='margin-bottom: 0; text-transform: uppercase;'><span style='color: #9FD120;''><i class='fas fa-check-circle'></i></span> - Administrador</p>";
        }

        if ($usuario->getProfesor() == 0) {
            $txt .= "<p style='margin: 0; text-transform: uppercase;'><span style='color: #C0392B;'><i class='fas fa-times-circle'></i></span> - Profesor</p>";
        }else{
            $txt .= "<p style='margin: 0; text-transform: uppercase;'><span style='color: #9FD120;'><i class='fas fa-check-circle'></i></span> - Profesor</p>";
        }

        if ($usuario->getVerificado() == 0) {
            $txt .= "<p style='margin-top: 0; text-transform: uppercase;'><span style='color: #C0392B;'><i class='fas fa-times-circle'></i></span> - Verificado</p>";
        }else{
            $txt .= "<p style='margin-top: 0; text-transform: uppercase;'><span style='color: #9FD120;'><i class='fas fa-check-circle'></i></span> - Verificado</p>";
        }

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button onclick=window.location='usuariosForm.php?id=".$usuario->getId()."';>Editar</button>
                        <button style='background-color: #9FD120;' onclick=\"recuperacionAdmin('".$usuario->getEmail()."')\";>Recuperar Contraseña</button>
                        <button style='background-color: #C0392B;' onclick='borraUsuario(".$usuario->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara el usuario que reciba por parametros
    * @param Integer $id_usuario la id del usuario a borrar
    */
    public function eliminarUsuario($id_usuario) {
        $sql = "DELETE FROM Usuario WHERE id='".$id_usuario."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de un usuario.
    * @param $id
    */
    public function obtenerUsuarioPorId($id){
        $sql = "SELECT id, admin, profesor, verificado, nombre, apellidos, fecha_nacimiento, direccion, cod_postal, poblacion, provincia, pais, telefono, movil, biografia, foto, twitter, linkedin, monedas FROM Usuario WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->admin = $res['admin'];
        $this->profesor = $res['profesor'];
        $this->verificado = $res['verificado'];
        $this->nombre = $res['nombre'];
        $this->apellidos = $res['apellidos'];
        $this->fecha_nacimiento = $res['fecha_nacimiento'];
        $this->direccion = $res['direccion'];
        $this->cod_postal = $res['cod_postal'];
        $this->poblacion = $res['poblacion'];
        $this->provincia = $res['provincia'];
        $this->pais = $res['pais'];
        $this->telefono = $res['telefono'];
        $this->movil = $res['movil'];
        $this->biografia = $res['biografia'];
        $this->foto = $res['foto'];
        $this->twitter = $res['twitter'];
        $this->linkedin = $res['linkedin'];
        $this->monedas = $res['monedas'];
    }

    /**
    * Recoge los datos del formulario y crea un nuevo usuario en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoUsuario($datos) {
        $this->setBiografia($datos['biografia']);
        $this->setFoto($datos['foto']);
        $this->setNombre($datos['nombre']);
        $this->setApellidos($datos['apellidos']);
        $this->setFecha_nacimiento($datos['fecha']);
        $this->setEmail($datos['email']);
        $this->setPass($datos['pass']);
        $this->setDni($datos['dni']);
        $this->setDireccion($datos['direccion']);
        $this->setCod_postal($datos['cod_postal']);
        $this->setPais($datos['pais']);
        $this->setPoblacion($datos['poblacion']);
        $this->setProvincia($datos['provincia']);
        $this->setMovil($datos['movil']);
        if (isset($datos['telefono'])) {
            $this->setTelefono($datos['telefono']);
        }else{
            $this->setTelefono(NULL);
        }
        $this->setId_tema_actual($datos['tema']);

        $sql = "INSERT INTO Usuario(biografia, foto, nombre, apellidos, fecha_nacimiento, email, pass, dni, direccion, cod_postal, pais, poblacion, provincia, movil, telefono, id_tema_actual, verificado) VALUES ('".$this->biografia."', '".$this->foto."', '".$this->nombre."', '".$this->apellidos."', '".$this->fecha_nacimiento."', '".$this->email."', '".$this->pass."', '".md5($this->dni)."', '".$this->direccion."', '".$this->cod_postal."', '".$this->pais."', '".$this->poblacion."', '".$this->provincia."', '".$this->movil."', '".$this->telefono."', '".$this->id_tema_actual."', 0);";
        
        $conexion = new Bd();
        $conexion->consulta($sql);


        $id = $this->obtenerIdPorEmail($this->email);

        $sql = "INSERT INTO Tema_Usuario(id_usuario, id_tema) VALUES (".$id.", ".$datos['tema'].")";
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita un usuario existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarUsuario($datos) {
        $this->setId($datos['id']);
        $this->setBiografia($datos['biografia']);
        $this->setFoto($datos['foto']);
        $this->setNombre($datos['nombre']);
        $this->setApellidos($datos['apellidos']);
        $this->setFecha_nacimiento($datos['fecha']);
        $this->setEmail($datos['email']);
        $this->setDni($datos['dni']);
        $this->setDireccion($datos['direccion']);
        $this->setCod_postal($datos['cod_postal']);
        $this->setPais($datos['pais']);
        $this->setPoblacion($datos['poblacion']);
        $this->setProvincia($datos['provincia']);
        $this->setMonedas($datos['monedas']);
        if (isset($datos['twitter'])) {
            $this->setTwitter($datos['twitter']);
        }else{
            $this->setTwitter(NULL);
        }
        if (isset($datos['linkedin'])) {
            $this->setLinkedin($datos['linkedin']);
        }else{
            $this->setLinkedin(NULL);
        }
        $this->setMovil($datos['movil']);
        if (isset($datos['telefono'])) {
            $this->setTelefono($datos['telefono']);
        }else{
            $this->setTelefono(NULL);
        }

        if (isset($datos['admin'])) {
            $this->setAdmin(1);
        }else{
            $this->setAdmin(0);
        }
        if (isset($datos['profesor'])) {
            $this->setProfesor(1);
        }else{
            $this->setProfesor(0);
        }
        if (isset($datos['verificado'])) {
            $this->setVerificado(1);
        }else{
            $this->setVerificado(0);
        }

        $sql = "UPDATE Usuario SET biografia='".$this->biografia."', foto='".$this->foto."', nombre='".$this->nombre."', apellidos='".$this->apellidos."', fecha_nacimiento='".$this->fecha_nacimiento."', direccion='".$this->direccion."', cod_postal='".$this->cod_postal."', pais='".$this->pais."', poblacion='".$this->poblacion."', provincia='".$this->provincia."', monedas='".$this->monedas."', twitter='".$this->twitter."', linkedin='".$this->linkedin."', movil='".$this->movil."', telefono='".$this->telefono."', admin='".$this->admin."', profesor='".$this->profesor."', verificado='".$this->verificado."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita un usuario existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarDatosPerfil($datos) {
        $this->setId($datos['id']);
        $this->setBiografia($datos['biografia']);
        $this->setFoto($datos['foto']);
        $this->setDireccion($datos['direccion']);
        $this->setPoblacion($datos['poblacion']);
        $this->setMovil($datos['movil']);
        if (isset($datos['telefono'])) {
            $this->setTelefono($datos['telefono']);
        }else{
            $this->setTelefono(NULL);
        }
        
        $sql = "UPDATE Usuario SET biografia='".$this->biografia."', foto='".$this->foto."', direccion='".$this->direccion."', poblacion='".$this->poblacion."', movil='".$this->movil."', telefono='".$this->telefono."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita un usuario existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarRedesPerfil($datos) {
        $this->setId($datos['id']);
        if (isset($datos['twitter'])) {
            $this->setTwitter($datos['twitter']);
        }else{
            $this->setTwitter(NULL);
        }
        if (isset($datos['linkedin'])) {
            $this->setLinkedin($datos['linkedin']);
        }else{
            $this->setLinkedin(NULL);
        }

        $sql = "UPDATE Usuario SET twitter='".$this->twitter."', linkedin='".$this->linkedin."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Enviara un correo al usuario con un enlace de recuperacion de password.
    * @param Array $datos contenido del POST del formulario
    */
    public function recuperarPassword($datos) {
        $codigo = substr(sha1(microtime()), 0, 20);
        $fecha = strtotime ('+10 minute', strtotime(date('Y-m-j H:i:s')));
        $fecha = date ('Y-m-j H:i:s', $fecha);
        $id = $this->obtenerIdPorEmail($datos['email']);

        $this->borrarRecuperacion($id);

        $sql = "INSERT INTO Verificacion(id_usuario, codigo, fecha_hora, recuperar) VALUES ('".$id."', '".$codigo."', '".$fecha."', 1);";
        $conexion = new Bd();
        $conexion->consulta($sql);

        $enlace = "http://omithioneducational.com/recuperar.php?code=".$codigo."&stamp=".substr(sha1(microtime()), 0, 10)."&id=".$id."&super=".time();

        $correo = new GestorMail();
        $tumensaje = "<div style='width: 76%; margin: 2% 10%; text-align: center; font-family: arial; font-weight: bold; box-shadow: 0 0 1em #797D7F; padding: 2%; border-radius: 10px; color: black;'>
            <a href='http://omithioneducational.com/'><img style='width: 40%;' src='http://omithioneducational.com/images/logo.png'></a>
            <p style='font-size: 1.25em;'>Aquí tienes el enlace para recupera la contraseña de tu cuenta en Omithion Educational:</p>
            <h2><a style='text-decoration: none; font-size: 1.5em; color: #152770;' href='".$enlace."'>".$enlace."</a></h2>
            <a href='".$enlace."'><button style='margin: 20px 0; padding: 20px; border: none; border-radius: 25px; font-size: 2em; font-weight: bold; text-transform: uppercase; background-color: #FFBA00; color: #FFF; text-shadow: 1.2px 1.2px 1.2px #7A7A7A; box-shadow: 0 0 0.5em #797D7F;'>Recuperar Contraseña</button></a>
            <p style='font-size: 1.20em;'>Como medida de seguridad, el enlace caducará en 10 minutos.</p>
            <hr style='height:1px;border:none;color:#333;background-color:#ddd;'>
            <a href='https://twitter.com/Omithion'><img style='width: 50px; margin: 5px;' src='http://omithiontech.com/images/twitter.png'></a>
            <p>© 2019 Omithion Educational. Todos los derechos reservados.</p>
            </div>";
        $correo->enviar($datos['email']," Recuperación de tu contraseña en Omithion Educational", $tumensaje);
    }

    /**
    * Devolvera true o false en base a si la hora de la generacion del codigo de recuperacion y si el codigo es correcto
    * @param String $email sera el email del usuario
    * @param String $codigo es el codigo que se ha enviado al usuario
    * @return Boolean true o false
    */
    public function comprobarRecuperacion($id, $codigo) {
        $sql = "SELECT codigo, fecha_hora FROM Verificacion WHERE id_usuario='".$id."' AND recuperar=1;";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        return ($res['codigo'] == $codigo && $this->compruebaFecha($res['fecha_hora']));
    }

    /**
    * Una vez el codigo se ha verificado se cambiara en la base de datos el estado de la verificacion del usuario
    * @param String $email sera el correo del usuario
    */
    public function recuperacionCorrecta($id, $pass) {
        $this->setPass($pass);

        $this->borrarRecuperacion($id);

        $sql = "UPDATE Usuario SET pass='".$this->pass."' WHERE id='".$id."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    public function obtenerMonedas($email) {
        $id = $this->obtenerIdPorEmail($email);

        $sql = "SELECT monedas FROM Usuario WHERE id = ".$id.";";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        return $res['monedas'];
    }

    public function passCorrecta($email, $pass) {
        $ok = false;

        $sql = "SELECT pass FROM Usuario WHERE email='".$email."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if(password_verify($pass, $res['pass'])){
            $ok = true;
        }
        return $ok;
    }

    public function cambiarPassword($email, $pass) {
        $id = $this->obtenerIdPorEmail($email);

        $this->setPass($pass);

        $sql = "UPDATE Usuario SET pass='".$this->pass."' WHERE id='".$id."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }
}