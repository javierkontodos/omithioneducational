<?php

/**
 * Esta es la clase foro
 */
class Foro {
	//variables de la clase
	private $id;
	private $id_usuario;
	private $id_asignatura;
	private $titulo;
	private $descripcion;
	private $fecha;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

    //Getter y Setter del Integer Id_usuario
	public function getId_usuario(){	return $this->id_usuario; }
	public function setId_usuario($id_usuario){ $this->id_usuario = $id_usuario; }

    //Getter y Setter del Integer Id_asignatura
	public function getId_asignatura(){	return $this->id_asignatura; }
	public function setId_asignatura($id_asignatura){ $this->id_asignatura = $id_asignatura; }

    //Getter y Setter del String Titulo
	public function getTitulo(){ return $this->titulo; }
	public function setTitulo($titulo){ $this->titulo = $titulo; }

    //Getter y Setter del String Descripcion
	public function getDescripcion(){ return $this->descripcion; }
	public function setDescripcion($descripcion){ $this->descripcion = $descripcion; }

    //Getter y Setter del Date Fecha
	public function getFecha(){ return $this->fecha; }
	public function setFecha($fecha){ $this->fecha = $fecha; }

	/**
    * Listara los foros de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de los foros
    */
    public function listarForos() {
        $sql = "SELECT Foro.id, Asignatura.nombre as asignatura, Usuario.nombre, Usuario.apellidos, Foro.titulo, Foro.descripcion, Foro.fecha FROM Foro, Asignatura, Usuario WHERE Foro.id_usuario = Usuario.id AND Foro.id_asignatura = Asignatura.id AND Asignatura.id = ";
        
        $txt = $this->seleccionarForos($sql);

        return $txt;
    }

    /**
    * Listara los foros de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $foro es el foro a buscar en la base de datos
    * @return String $txt devolvera la vista de los foro
    */
    public function buscarForos($foro) {
        $sql = "SELECT Foro.id, Asignatura.nombre as asignatura, Usuario.nombre, Usuario.apellidos, Foro.titulo, Foro.descripcion, Foro.fecha FROM Foro, Asignatura, Usuario WHERE Foro.titulo LIKE '%".$foro."%' AND Foro.id_usuario = Usuario.id AND Foro.id_asignatura = Asignatura.id AND Asignatura.id = ";
        
        $txt = $this->seleccionarForos($sql);

        return $txt;
    }

    /**
    * Listara todos las asignaturas de la base de datos.
    * @return Array $listaAsignatura devolvera una lista con las id de las asignaturas
    */
    public function seleccionaAsignaturas() {
        $listaAsignatura = array();
    	$sql = "SELECT id FROM Asignatura;";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id) = mysqli_fetch_array($res)) {
            array_push($listaAsignatura, $id);
        }

        return $listaAsignatura;
    }

    /**
    * Listara los foros de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de los foros
    */
    public function seleccionarForos($sql) {
        $listaAsignatura = array();
        $txt = "";

        $listaAsignatura = $this->seleccionaAsignaturas();

    	for($i=0;$i<sizeof($listaAsignatura);$i++){
            $listaForo = array();
	
        	$conexion = new Bd();
        	$res = $conexion->consulta($sql.$listaAsignatura[$i].";");
	
        	while (list($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha) = mysqli_fetch_array($res)) {
        	    $foro = new Foro();
        	    $foro->llenarForo($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha);
        	    array_push($listaForo, $foro);
        	}

            if (sizeof($listaForo) > 0) {
                $txt .= "<p class='tituloDesplegable'><a href='javascript:tituloPlegable(".$listaAsignatura[$i].")'><span id='iconoPlegable".$listaAsignatura[$i]."'><i class='far fa-caret-square-down'></i></span> ".$listaForo[0]->id_asignatura."</a></p>";
                $txt .= "<span id='cursoPlegable".$listaAsignatura[$i]."'>";

                for($z=0;$z<sizeof($listaForo);$z++){
                    $txt .= $this->tarjetaForo($listaForo[$z]);
                }
    
                $txt .= "</span>";
            }
		}

        return $txt;
    }

    /**
    * Rellenara los campos de un foro con los datos que recibe de la misma.
    * @param Integer $id
    * @param String $asignatura 
    * @param String $titulo
    * @param String $descripcion
    * @param Date $fecha
    */
    public function llenarForo($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha){
        $this->id = $id;
        $this->id_usuario = $nombre." ".$apellidos;
        $this->id_asignatura = $asignatura;
        $this->titulo = $titulo;
        $this->descripcion = $descripcion;
        $this->fecha = $fecha;
    }

    /**
    * Devolvera la vista del foro de la base de datos.
    * @param String $foro recibe un foro
    * @return String $txt devolvera la vista del foro
    */
    public function tarjetaForo($foro, $url="") {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='datosContainer'><h2>".$foro->getTitulo()."</h2>";

        $txt .= "<p>".$foro->getDescripcion()."</p>";

        $txt .= "<h4><i class='fas fa-user-circle'></i> ".$foro->getId_usuario()."</h4>";
        
        $txt .= "<p><i class='far fa-calendar-alt'></i> Creado el <b>".$this->converirFecha($foro->getFecha())."</b></p>";

        $txt .= "<ul class='archivosContainer'>";
        $txt .= $this->listarArchivos($foro->getId());
        $txt .= "</ul>";

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button style='background-color: #9FD120;' onclick=window.location='forosMore.php?".$url."id=".$foro->getId()."';>Ver Más</button>
                        <button onclick=window.location='forosForm.php?".$url."id=".$foro->getId()."';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraForo(".$foro->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara el foro que reciba por parametros
    * @param Integer $id_foro la id del foro a borrar
    */
    public function eliminarForo($id_foro) {
        $sql = "DELETE FROM Foro WHERE id='".$id_foro."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de un foro.
    * @param $id
    */
    public function obtenerForoPorId($id){
        $sql = "SELECT id, id_asignatura, titulo, descripcion, fecha FROM Foro WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->id_asignatura = $res['id_asignatura'];
        $this->titulo = $res['titulo'];
        $this->descripcion = $res['descripcion'];
        $this->fecha = $res['fecha'];
    }

    /**
    * Recoge los datos del formulario y crea un nuevo foro en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoForo($datos, $email) {
        $usuario = new Usuario();
        $this->setId_usuario($usuario->obtenerIdPorEmail($email));
        $this->setId_asignatura($datos['asignatura']);
        $this->setTitulo($datos['titulo']);
        $this->setDescripcion($datos['descripcion']);
        $this->setFecha(date('Y-m-d', time()));


        $sql = "INSERT INTO Foro(id_usuario, id_asignatura, titulo, descripcion, fecha) VALUES ('".$this->id_usuario."', '".$this->id_asignatura."', '".$this->titulo."', '".$this->descripcion."', '".$this->fecha."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        if (!empty($datos['archivo1'])) {
            $this->subirArchivos($datos['archivo1']);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivos($datos['archivo2']);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivos($datos['archivo3']);
        }
    }

    /**
    * Recoge los datos del formulario y edita un foro existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarForo($datos) {
        $this->setId($datos['id']);
        $this->setId_asignatura($datos['asignatura']);
        $this->setTitulo($datos['titulo']);
        $this->setDescripcion($datos['descripcion']);

        $sql = "UPDATE Foro SET id_asignatura='".$this->id_asignatura."', titulo='".$this->titulo."', descripcion='".$this->descripcion."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        $sql = "DELETE FROM Archivo_Foro WHERE id_foro='".$this->id."';";

        if (!empty($datos['archivo1'])) {
            $this->subirArchivos($datos['archivo1'], $this->id);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivos($datos['archivo2'], $this->id);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivos($datos['archivo3'], $this->id);
        }
    }

    /**
    * Inserta en la base de datos un nuevo archivo con la id que recibe o la ultima de la base de datos si se trata de un nuevo foro
    * @param String $archivo ruta del archivo
    * @param Integer $id la id del foro
    */
    public function subirArchivos($archivo, $id=0) {
        $conexion = new Bd();

        if ($id == 0) {
            $sql = "SELECT MAX(id) as id FROM Foro;";
            $res = $conexion->consultaSimple($sql);
            $id = $res["id"];
        }

        $sql = "INSERT INTO Archivo_Foro(id_foro, archivo) VALUES ('".$id."','".$archivo."');";

        $conexion->consulta($sql);
    }

    /**
    * Pintara los archivos que tenga un foro con su archivo de extension propio
    * @param Integer $id la id del foro
    * @return String $id la vista de los archivos
    */
    public function listarArchivos($id) {
        $listaArchivo = array();
        $conexion = new Bd();

        $sql = "SELECT archivo FROM Archivo_Foro WHERE id_foro = ".$id.";";

        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) == 1) {
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
        }
        if (sizeof($listaArchivo) == 2) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
        }
        if (sizeof($listaArchivo) == 3) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[2]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[2], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[2], 25)."</p></li>";
        }

        return $txt;
    }

    /**
    * Pintara todos los archivos que tenga un foro con su archivo de extension propio
    * @param Integer $id la id del foro
    * @return String $id la vista de los archivos
    */
    public function obtenerArchivos($id) {
        $listaArchivo = array();
        $conexion = new Bd();

        $sql = "SELECT archivo FROM Archivo_Foro WHERE id_foro = ".$id.";";

        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) > 0) {
            if (sizeof($listaArchivo) == 1) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2);
                $txt .= $this->pintarArchivo(3);
            }
            if (sizeof($listaArchivo) == 2) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2, $listaArchivo[1], pathinfo($listaArchivo[1], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(3);
            }
            if (sizeof($listaArchivo) == 3) {
                $txt .= $this->pintarArchivo(1, $listaArchivo[0], pathinfo($listaArchivo[0], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(2, $listaArchivo[1], pathinfo($listaArchivo[1], PATHINFO_EXTENSION), "1E8449");
                $txt .= $this->pintarArchivo(3, $listaArchivo[2], pathinfo($listaArchivo[2], PATHINFO_EXTENSION), "1E8449");
            }
        }else{
            $txt .= $this->pintarArchivo(1);
            $txt .= $this->pintarArchivo(2);
            $txt .= $this->pintarArchivo(3);
        }

        return $txt;
    }

    /**
    * Pintara los archivos que tenga un foro con su archivo de extension propio
    * @param Integer $pos la posicion del archivo en el formulario
    * @param String $archivo la ruta del archivo
    * @param String $icon la extension del tipo de archivo
    * @param String $color el color que recibe sera el de cuando esta marcado el archivo
    * @return String $id la vista de los archivos
    */
    public function pintarArchivo($pos, $archivo='', $icon= "nofile", $color="C0392B") {
        $txt = "<ul>";

        if ($archivo != "") {
            $txt .= "<li id='spanImage".$pos."' style='text-align: center;'><a href='../".$archivo."' download><span style='background-image: url(../images/archivos/".$icon.".png);''></span></a></li>";
            $txt .= "<li id='fileName".$pos."' style='text-align: center; font-size: 0.8em; word-break: break-all;'>".substr($archivo, 25)."</li>";
        }else{
            $txt .= "<li id='spanImage".$pos."' style='text-align: center;'><span style='background-image: url(../images/archivos/".$icon.".png);'></span></li>";
            $txt .= "<li id='fileName".$pos."' style='text-align: center; font-size: 0.8em;'>Archivo no seleccionado</li>";
        }

        $txt .= "<li><input id='botonSelected".$pos."' type='button' style='background-color: #".$color.";' onclick=\"document.getElementById('fileSelected".$pos."').click()\" value='Subir Archivo'></li>
                 <li><input id='archivo".$pos."' name='archivo".$pos."' type='hidden' style='display: none;' value=''></li>
                 <li><input id='fileSelected".$pos."' type='file' style='display: none;' onchange=\"subirArchivoForm(".$pos.", 'cambiarArchivoForo')\"></li>";
                
        $txt .= "</ul>";

        return $txt;
    }

    /**
    * Devolvera una lista de foros que tenga una asignatura
    * @param Integer $asignatura la id de la asignatura
    * @param Integer $curso la id del curso
    * @return String $txt la lista de foros
    */
    public function foroListCampus($asignatura, $curso) {
        $sql = "SELECT id, titulo, descripcion, fecha FROM Foro WHERE id_asignatura = ".$asignatura.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $titulo, $descripcion, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                        <div class='imagenTarjetaCampus'>
                            <span style='background-image: url(images/foro.png)'></span>
                        </div>
                        <div class='datosTarjetaCampus'>
                            <h3><i class='fas fa-bookmark'></i> ".$titulo."</h3>
                            <h5>".$this->converirFecha($fecha)."</h5>
                            <p>".substr($descripcion, 0, 100)."...</p>
                        </div>
                        <div class='botonTarjetaCampus'>
                            <button onclick=\"window.location='foro.php?curso=".$curso."&asignatura=".$asignatura."&foro=".$id."';\">Ver Más</button>
                        </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun foro disponible en esta asignatura</h3>";

        return $txt;
    }

    /**
    * Devolvera un string con la fecha bien formateada
    * @param Date $fecha una fecha
    * @return String $txt la fecha formateada
    */
    public function converirFecha($fecha) {
        $fechaComoEntero = strtotime($fecha);
        $meses = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

        $txt = date("d", $fechaComoEntero)." de ";
        $txt .= $meses[date("m", $fechaComoEntero)-1];
        $txt .= " de ".date("Y", $fechaComoEntero);

        return $txt;
    }

    /**
    * Devolvera true o false en base a si el foro esta en esa asignatura.
    * @param String $email variable SESSION de php
    * @return Boolean $ok true o false
    */
    public static function verificado($id_asignatura, $id_foro){
        $ok = false;

        $sql = "SELECT count(id) as verificado FROM Foro WHERE id_asignatura='".$id_asignatura."' AND id='".$id_foro."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        if($res['verificado'] != 0){
            $ok = true;
        }

        return $ok;
    }

    /**
    * Pintara todos los archivos que tenga un foro con su archivo de extension propio
    * @param String $sql el sql de los archivos
    * @return String $id la vista de los archivos
    */
    public function archivosCampus($sql) {
        $listaArchivo = array();
        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        while (list($archivo) = mysqli_fetch_array($res)) {
            array_push($listaArchivo, $archivo);
        }

        $txt = "";

        if (sizeof($listaArchivo) == 1) {
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
        }
        if (sizeof($listaArchivo) == 2) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><span></span></a></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
        }
        if (sizeof($listaArchivo) == 3) {
            $txt .= "<li><a href='../".$listaArchivo[0]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[0], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[0], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[1]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[1], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[1], 25)."</p></li>";
            $txt .= "<li><a href='../".$listaArchivo[2]."' download><span style='background-image: url(../images/archivos/".pathinfo($listaArchivo[2], PATHINFO_EXTENSION).".png);'></span></a><p>".substr($listaArchivo[2], 25)."</p></li>";
        }

        return $txt;
    }

    /**
    * Recoge los datos del formulario y crea un nuevo comentario en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevoComentario($datos, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);
        $sql = "INSERT INTO Comentario_Foro(id_foro, id_usuario, respuesta, fecha) VALUES ('".$datos['foro']."', '".$id_usuario."', '".$datos['comentario']."', '".date('Y-m-d', time())."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);

        if (!empty($datos['archivo1'])) {
            $this->subirArchivosComentario($datos['archivo1']);
        }
        if (!empty($datos['archivo2'])) {
            $this->subirArchivosComentario($datos['archivo2']);
        }
        if (!empty($datos['archivo3'])) {
            $this->subirArchivosComentario($datos['archivo3']);
        }
    }

    /**
    * Inserta en la base de datos un nuevo archivo con la id que recibe o la ultima de la base de datos si se trata de un nuevo comentario
    * @param String $archivo ruta del archivo
    * @param Integer $id la id del comentario
    */
    public function subirArchivosComentario($archivo, $id=0) {
        $conexion = new Bd();

        if ($id == 0) {
            $sql = "SELECT MAX(id) as id FROM Comentario_Foro;";
            $res = $conexion->consultaSimple($sql);
            $id = $res["id"];
        }

        $sql = "INSERT INTO Archivo_Comentario_Foro(id_comentario_foro, archivo) VALUES ('".$id."','".$archivo."');";

        $conexion->consulta($sql);
    }

    /**
    * Devolvera una lista de foros que tenga una asignatura
    * @param Integer $foro la id del foro
    * @param Integer $email el email del usuario
    * @return String $txt la lista de foros
    */
    public function foroComentariosListCampus($foro, $email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Comentario_Foro.id, Usuario.nombre, Usuario.apellidos, Usuario.foto, Usuario.profesor, Comentario_Foro.respuesta, Comentario_Foro.fecha FROM Comentario_Foro, Usuario WHERE Comentario_Foro.id_usuario = Usuario.id AND Comentario_Foro.id_foro = ".$foro.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $nombre, $apellidos, $foto, $profesor, $respuesta, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                         <div class='imagenTarjetaCampus'>
                             <img src='../".$foto."'>
                         </div>
                         <div class='datosTarjetaCampus'>";
            if ($profesor == 0) {
                $txt .= "<h4><i class='fas fa-user-astronaut'></i> ".$nombre." ".$apellidos."</h4>";
            }else{
                $txt .= "<h4><i class='fas fa-user-secret'></i> ".$nombre." ".$apellidos."</h4>";
            }
            $txt .= "<h5>".$this->converirFecha($fecha)."</h5>
                             <p>".substr($respuesta, 0, 140)."</p>
                             <div class='archivosTarjetaCampus'>
                                <ul>".$this->archivosCampus("SELECT archivo FROM Archivo_Comentario_Foro WHERE id_comentario_foro = ".$id.";")."</ul>
                             </div>
                         </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun comentario en este foro</h3>";

        return $txt;
    }

    /**
    * Devolvera una lista de foros que tenga una asignatura
    * @param Integer $foro la id del foro
    * @return String $txt la lista de foros
    */
    public function foroComentariosListProfesor($foro) {
        $sql = "SELECT Comentario_Foro.id, Usuario.nombre, Usuario.apellidos, Usuario.foto, Usuario.profesor, Comentario_Foro.respuesta, Comentario_Foro.fecha FROM Comentario_Foro, Usuario WHERE Comentario_Foro.id_usuario = Usuario.id AND Comentario_Foro.id_foro = ".$foro.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);
        
        $txt = "";
        $i = 0;
        while (list($id, $nombre, $apellidos, $foto, $profesor, $respuesta, $fecha) = mysqli_fetch_array($res)) {
            $txt .= "<div class='tarjetaCampus'>
                         <div class='imagenTarjetaCampus'>
                             <img src='../".$foto."'>
                         </div>
                         <div class='datosTarjetaCampus'>";
            if ($profesor == 0) {
                $txt .= "<h4><i class='fas fa-user-astronaut'></i> ".$nombre." ".$apellidos."</h4>";
            }else{
                $txt .= "<h4><i class='fas fa-user-secret'></i> ".$nombre." ".$apellidos."</h4>";
            }
            $txt .= "<h5>".$this->converirFecha($fecha)."</h5>
                             <p>".substr($respuesta, 0, 140)."</p>
                             <div class='archivosTarjetaCampus'>
                                <ul>".$this->archivosCampus("SELECT archivo FROM Archivo_Comentario_Foro WHERE id_comentario_foro = ".$id.";")."</ul>
                             </div>
                         </div>
                         <div class='botonTarjetaCampus'>
                            <button style='background-color: #C0392B; margin: 10px;' onclick=''>Borrar</button>
                        </div>
                     </div>";
            $i++;
        }
        if ($i == 0) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun comentario en este foro</h3>";

        return $txt;
    }

    /**
    * Listara los foros de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @param String $asignatura es la asignatura
    * @return String $txt devolvera la vista de los foros
    */
    public function listarForosProfesor($asignatura) {
        $url = "asignatura=".$asignatura."&";
        $sql = "SELECT Foro.id, Asignatura.nombre as asignatura, Usuario.nombre, Usuario.apellidos, Foro.titulo, Foro.descripcion, Foro.fecha FROM Foro, Asignatura, Usuario WHERE Foro.id_usuario = Usuario.id AND Foro.id_asignatura = Asignatura.id AND Asignatura.id = ".$asignatura.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $listaForo = array();
        while (list($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha) = mysqli_fetch_array($res)) {
            $foro = new Foro();
            $foro->llenarForo($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha);
            array_push($listaForo, $foro);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaForo);$i++){
            $txt .= $this->tarjetaForo($listaForo[$i], $url);
        }

        if (sizeof($listaForo) < 1) $txt = "<h3 style='text-align: center; margin-top: 50px;'>Actualmente no hay ningun foro en esta asignatura</h3>";

        return $txt;
    }

    /**
    * Listara los foros de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $foro es el foro a buscar en la base de datos
    * @param String $asignatura es la asignatura
    * @return String $txt devolvera la vista de los foro
    */
    public function buscarForosProfesor($foro, $asignatura) {
        $url = "asignatura=".$asignatura."&";
        $sql = "SELECT Foro.id, Asignatura.nombre as asignatura, Usuario.nombre, Usuario.apellidos, Foro.titulo, Foro.descripcion, Foro.fecha FROM Foro, Asignatura, Usuario WHERE Foro.titulo LIKE '%".$foro."%' AND Foro.id_usuario = Usuario.id AND Foro.id_asignatura = Asignatura.id AND Asignatura.id = ".$asignatura.";";
        
        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $listaForo = array();
        while (list($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha) = mysqli_fetch_array($res)) {
            $foro = new Foro();
            $foro->llenarForo($id, $asignatura, $nombre, $apellidos, $titulo, $descripcion, $fecha);
            array_push($listaForo, $foro);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaForo);$i++){
            $txt .= $this->tarjetaForo($listaForo[$i], $url);
        }

        return $txt;
    }
}