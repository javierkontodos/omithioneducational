<?php

/**
 * Esta es la clase asignatura
 */
class Asignatura {
	//variables de la clase
	private $id;
	private $id_usuario_profesor;
	private $id_curso;
	private $nombre;

	//Constructor vacio
    public function __construct(){ }

    //Getter y Setter del Integer Id
	public function getId(){ return $this->id; }
	public function setId($id){	$this->id = $id; }

    //Getter y Setter del Integer Id_usuario_profesor
	public function getId_usuario_profesor(){ return $this->id_usuario_profesor; }
	public function setId_usuario_profesor($id_usuario_profesor){ $this->id_usuario_profesor = $id_usuario_profesor; }

    //Getter y Setter del Integer Id_curso
	public function getId_curso(){	return $this->id_curso; }
	public function setId_curso($id_curso){ $this->id_curso = $id_curso; }

    //Getter y Setter del String Nombre
	public function getNombre(){ return $this->nombre; }
	public function setNombre($nombre){ $this->nombre = $nombre; }

	/**
    * Listara las asignaturas de la base de datos y llamara a una funcion para pintarlos en la vista.
    * @return String $txt devolvera la vista de las asignaturas
    */
    public function listarAsignaturas() {
        $sql = "SELECT Asignatura.id as id, Usuario.nombre as nombre_profe, Usuario.apellidos as apellidos_profe, Curso.nombre as curso, Asignatura.nombre as nombre FROM Asignatura, Curso, Usuario WHERE Asignatura.id_usuario_profesor = Usuario.id AND Asignatura.id_curso = Curso.id AND Curso.id = ";
       
        $txt = $this->seleccionarAsignaturas($sql);

        return $txt;
    }

     /**
    * Listara las asignaturas de la base de datos en base a la busqueda y llamara a una funcion para pintarlos en la vista.
    * @param String $asignatura es el asignatura a buscar en la base de datos
    * @return String $txt devolvera la vista de las asignatura
    */
    public function buscarAsignaturas($asignatura) {
        $sql = "SELECT Asignatura.id as id, Usuario.nombre as nombre_profe, Usuario.apellidos as apellidos_profe, Curso.nombre as curso, Asignatura.nombre as nombre FROM Asignatura, Curso, Usuario WHERE Asignatura.nombre LIKE '%".$asignatura."%' AND Asignatura.id_usuario_profesor = Usuario.id AND Asignatura.id_curso = Curso.id AND Curso.id = ";
        
        $txt = $this->seleccionarAsignaturas($sql);

        return $txt;
    }

    /**
    * Listara todos los cursos de la base de datos.
    * @return Array $listaCurso devolvera una lista con las id de los cursos
    */
    public function seleccionaCursos() {
        $listaCurso = array();
    	$sql = "SELECT id FROM Curso;";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id) = mysqli_fetch_array($res)) {
            array_push($listaCurso, $id);
        }

        return $listaCurso;
    }

    /**
    * Listara las asignaturas de la base de datos con el sql que recibe y llamara a una funcion para pintarlos uno a uno en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de las asignaturas
    */
    public function seleccionarAsignaturas($sql) {
        $listaCurso = array();
        $txt = "";

        $listaCurso = $this->seleccionaCursos();

    	for($i=0;$i<sizeof($listaCurso);$i++){
            $listaAsignatura = array();
	
        	$conexion = new Bd();
        	$res = $conexion->consulta($sql.$listaCurso[$i].";");
	
        	while (list($id, $nombre_profe, $apellidos_profe, $curso, $nombre) = mysqli_fetch_array($res)) {
        	    $asignatura = new Asignatura();
        	    $asignatura->llenarAsignatura($id, $nombre_profe, $apellidos_profe, $curso, $nombre);
        	    array_push($listaAsignatura, $asignatura);
        	}

            if (sizeof($listaAsignatura) > 0) {
                $txt .= "<p class='tituloDesplegable'><a href='javascript:tituloPlegable(".$listaCurso[$i].")'><span id='iconoPlegable".$listaCurso[$i]."'><i class='far fa-caret-square-down'></i></span> ".$listaAsignatura[0]->id_curso."</a></p>";
                $txt .= "<span id='cursoPlegable".$listaCurso[$i]."'>";

                for($z=0;$z<sizeof($listaAsignatura);$z++){
                    $txt .= $this->tarjetaAsignatura($listaAsignatura[$z]);
                }
    
                $txt .= "</span>";
            }
		}

        return $txt;
    }

    /**
    * Rellenara los campos de una asignatura con los datos que recibe de la misma.
    * @param Integer $id
    * @param String $apellidos_profe 
    * @param String $nombre_profe
    * @param String $curso
    * @param String $nombre
    */
    public function llenarAsignatura($id, $nombre_profe, $apellidos_profe, $curso, $nombre){
        $this->id = $id;
        $this->id_usuario_profesor = $nombre_profe." ".$apellidos_profe;
        $this->id_curso = $curso;
        $this->nombre = $nombre;
    }

    /**
    * Devolvera la vista del asignatura de la base de datos.
    * @param String $asignatura recibe un asignatura
    * @return String $txt devolvera la vista del asignatura
    */
    public function tarjetaAsignatura($asignatura) {
        $txt = "<div class='tarjetaContainer'>";

        $txt .= "<div class='datosContainer' style='margin-left: 25px;'><h2>".$asignatura->getNombre()."</h2>";

        $txt .= "<p><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i></p><p><b>PROFESOR</b></p>";

        $txt .= "<p>".$asignatura->getId_usuario_profesor()."</p>";

        $txt .= "</div>
                    <div class='botonesContainer'>
                        <button onclick=window.location='asignaturasForm.php?id=".$asignatura->getId()."';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraAsignatura(".$asignatura->getId().")'>Borrar</button>
                    </div>
                </div>";

        return $txt;
    }

    /**
    * Borrara la asignatura que reciba por parametros
    * @param Integer $id_asignatura la id de la asignatura a borrar
    */
    public function eliminarAsignatura($id_asignatura) {
        $sql = "DELETE FROM Asignatura WHERE id='".$id_asignatura."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Seleciona todos los datos de una asignatura.
    * @param $id
    */
    public function obtenerAsignaturaPorId($id){
        $sql = "SELECT id, id_usuario_profesor, id_curso, nombre FROM Asignatura WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->id_usuario_profesor = $res['id_usuario_profesor'];
        $this->id_curso = $res['id_curso'];
        $this->nombre = $res['nombre'];
    }

    /**
    * Recoge los datos del formulario y crea una nueva asignatura en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function nuevaAsignatura($datos) {
        $this->setId($datos['id']);
        $this->setId_usuario_profesor($datos['profesor']);
        $this->setId_curso($datos['curso']);
        $this->setNombre($datos['nombre']);


        $sql = "INSERT INTO Asignatura(id_usuario_profesor, id_curso, nombre) VALUES ('".$this->id_usuario_profesor."', '".$this->id_curso."', '".$this->nombre."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita una asignatura existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarAsignatura($datos) {
        $this->setId($datos['id']);
        $this->setId_usuario_profesor($datos['profesor']);
        $this->setId_curso($datos['curso']);
        $this->setNombre($datos['nombre']);

        $sql = "UPDATE Asignatura SET id_usuario_profesor='".$this->id_usuario_profesor."', id_curso='".$this->id_curso."', nombre='".$this->nombre."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Listara los options con las asignaturas de la base de datos.
    * @param Integer $asignatura es la id de la asignatura que tiene el temario
    * @return String $txt devolvera la vista de las asignaturas en options
    */
    public function selectorAsignaturas($id_asignatura) {
        $sql = "SELECT id, id_usuario_profesor, id_usuario_profesor, id_curso, nombre FROM Asignatura;";

        $listaAsignatura = array();
        $conexion = new Bd();

        $res = $conexion->consulta($sql);
        while (list($id, $nombre_profe, $apellidos_profe, $curso, $nombre) = mysqli_fetch_array($res)) {
            $asignatura = new Asignatura();
            $asignatura->llenarAsignatura($id, $nombre_profe, $apellidos_profe, $curso, $nombre);
            array_push($listaAsignatura, $asignatura);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaAsignatura);$i++){
            $txt .= "<option value='".$listaAsignatura[$i]->getId()."' ".strval(($listaAsignatura[$i]->getId() == $id_asignatura)?'selected':'').">".$listaAsignatura[$i]->getNombre()."</option>";
        }

        return $txt;
    }

    public function asignaturasAside($id_curso, $id_asignatura) {
        $sql = "SELECT id, id_usuario_profesor, id_usuario_profesor, id_curso, nombre FROM Asignatura WHERE id_curso=".$id_curso.";";

        $listaAsignatura = array();
        $conexion = new Bd();

        $res = $conexion->consulta($sql);
        while (list($id, $nombre_profe, $apellidos_profe, $curso, $nombre) = mysqli_fetch_array($res)) {
            $asignatura = new Asignatura();
            $asignatura->llenarAsignatura($id, $nombre_profe, $apellidos_profe, $curso, $nombre);
            array_push($listaAsignatura, $asignatura);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaAsignatura);$i++){
            $txt .= "<option value='".$listaAsignatura[$i]->getId()."' ".strval(($listaAsignatura[$i]->getId() == $id_asignatura)?'selected':'').">".$listaAsignatura[$i]->getNombre()."</option>";
        }

        return $txt;
    }

    public function asignaturasIndex($id_curso) {
        $sql = "SELECT id, id_usuario_profesor, id_usuario_profesor, id_curso, nombre FROM Asignatura WHERE id_curso=".$id_curso.";";

        $listaAsignatura = array();
        $conexion = new Bd();

        $res = $conexion->consulta($sql);
        while (list($id, $nombre_profe, $apellidos_profe, $curso, $nombre) = mysqli_fetch_array($res)) {
            $asignatura = new Asignatura();
            $asignatura->llenarAsignatura($id, $nombre_profe, $apellidos_profe, $curso, $nombre);
            array_push($listaAsignatura, $asignatura);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaAsignatura);$i++){
            $txt .= "<div class='asignaturasIndex' onclick=window.location='temarios.php?curso=".$id_curso."&asignatura=".$listaAsignatura[$i]->getId()."';>".$listaAsignatura[$i]->getNombre()."</div>";
        }

        return $txt;
    }

    public function asignaturasProfesorado($email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);
        $sql = "SELECT id, id_usuario_profesor, id_usuario_profesor, id_curso, nombre FROM Asignatura WHERE id_usuario_profesor=".$id_usuario.";";

        $listaAsignatura = array();
        $conexion = new Bd();

        $res = $conexion->consulta($sql);
        while (list($id, $nombre_profe, $apellidos_profe, $curso, $nombre) = mysqli_fetch_array($res)) {
            $asignatura = new Asignatura();
            $asignatura->llenarAsignatura($id, $nombre_profe, $apellidos_profe, $curso, $nombre);
            array_push($listaAsignatura, $asignatura);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaAsignatura);$i++){
            $txt .= "<button class='asignaturasIndex' onclick=\"window.location='panel.php?asignatura=".$listaAsignatura[$i]->getId()."'\">".$listaAsignatura[$i]->getNombre()."</button>";
        }

        return $txt;
    }

    public static function verificaProfesor($email, $asignatura) {
        $ok = false;
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT count(id) as verfificada FROM Asignatura WHERE id_usuario_profesor='".$id_usuario."' AND id=".$asignatura.";";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if ($res['verfificada'] != 0) {
            $ok = true;
        }
        return $ok;
    }
}