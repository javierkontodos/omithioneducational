<?php  

/**
* Esta es la clase matricula
* @Author OmithionEducational
* @Version 0.1
*/
class Matricula {
	//variables de la clase
    private $id;
    private $id_usuario;
    private $id_curso;
    private $tipo_pago;
    private $fecha_compra;
    private $notificaciones;
    
    
    //Constructor vacio
    public function __construct(){}
    
    //Getter y Setter del Integer Id
    public function getId() {return $this->id;}
    public function setId($id) {$id = intval($id);$this->id = $id;}    

    //Getter y Setter del Integer Id_usuario
    public function getId_usuario() {return $this->id_usuario;}
    public function setId_usuario($id_usuario) {$this->id_usuario = $id_usuario;}

    //Getter y Setter del Integer Id_curso
    public function getId_curso() {return $this->id_curso;}
    public function setId_curso($id_curso) {$this->id_curso = $id_curso;}

    //Getter y Setter del String Tipo_pago
    public function getTipo_pago() {return $this->tipo_pago;}
    public function setTipo_pago($tipo_pago) {$this->tipo_pago = addslashes($tipo_pago);}

    //Getter y Setter del Date Fecha_compra
    public function getFecha_compra() {return $this->fecha_compra;}
    public function setFecha_compra($fecha_compra) {$this->fecha_compra = addslashes($fecha_compra);}

    //Getter y Setter del Boolean Notificaciones
    public function getNotificaciones() {return $this->notificaciones;}
    public function setNotificaciones($notificaciones) {$this->notificaciones = addslashes($notificaciones);}

    
    /**
    * Esta funcion crea una nueva matricula
    * @param Array $datos son los datos del formulario
    */
    public function nuevaMatricula($datos){
        $this->setId_usuario($datos['id_usuario']);
        $this->setId_curso($datos['id_curso']);
        $this->setFecha_compra(date('Y-m-d', time()));

        if (isset($datos['notificaciones'])) {
            $this->setNotificaciones(1);
        }else{
            $this->setNotificaciones(0);
        }

        switch ($datos['tipo_pago']) {
            case 1:
                $this->setTipo_pago('tarjeta');
                $this->nuevoPagoTarjeta($datos);
                break;
            case 2:
                $this->setTipo_pago('transferencia');
                $this->nuevoPagoTransferencia($datos);
                break;
            case 3:
                $this->setTipo_pago('administracion');
                break;
            default:
                $this->setTipo_pago('errores');
                break;
        }

        $sql = "INSERT INTO Matricula(id_usuario, id_curso, tipo_pago, fecha_compra,  notificaciones)
                VALUES ('".$this->id_usuario."', '".$this->id_curso."', '".$this->tipo_pago."', '".$this->fecha_compra."', '".$this->notificaciones."');";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Guarda la informacion de la tarjeta en la base de datos.
    * @param Array $datos son los datos del formulario
    */
    public function nuevoPagoTarjeta($datos){
        $id = $this->obtenerIdMatricula();

        if (!empty($datos['idTarjetaBancaria'])) {
            $tarjetaBancaria = new TarjetaBancaria();
            $tarjetaBancaria->obtenerTarjetaPorId($datos['idTarjetaBancaria']);

            $sql = "INSERT INTO Pago_Tarjeta(id_matricula, titular, numero) VALUES ('".$id."','".$tarjetaBancaria->getTitular()."','".$tarjetaBancaria->getNumero()."')";
        }else{
            if(isset($datos['recordarTarjeta'])){
                $tarjetaBancaria = new TarjetaBancaria();
                $tarjetaBancaria->nuevaTarjeta($datos);
            }
            $sql = "INSERT INTO Pago_Tarjeta(id_matricula, titular, numero) VALUES ('".$id."','".$datos['nombreTarjeta']."','".$datos['numeroTarjeta']."')";
        }
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Guarda la informacion de la transferencia en la base de datos.
    * @param Array $datos son los datos del formulario
    */
    public function nuevoPagoTransferencia($datos){
        $id = $this->obtenerIdMatricula();

        $sql = "INSERT INTO Pago_Transferencia(id_matricula, recibo) VALUES ('".$id."','".$datos['recibo']."')";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Devolvera la ultima id de una matricula.
    * @return Integer $id la id de la ultima matricula + 1
    */
    public function obtenerIdMatricula(){
        $sql = "SELECT MAX(id) as id FROM Matricula;"; 

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $id = intval($res['id'])+1;

        return $id;
    }

    /**
    * Seleciona todos los datos de una matricula.
    * @param $id
    */
    public function obtenerMatriculaPorId($id){
        $sql = "SELECT id, id_usuario, id_curso, tipo_pago, fecha_compra, notificaciones FROM Matricula WHERE id=".$id;

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $this->id = $res['id'];
        $this->id_usuario = $res['id_usuario'];
        $this->id_curso = $res['id_curso'];
        $this->tipo_pago = $res['tipo_pago'];
        $this->fecha_compra = $res['fecha_compra'];
        $this->notificaciones = $res['notificaciones'];
    }

    /**
    * Listara las matriculas de la base de datos y llamara a una funcion para pintarlas en la vista.
    * @return String $txt devolvera la vista de las matriculas
    */
    public function listarMatriculas() {
        $sql = "SELECT Matricula.id, Usuario.nombre as nombre_usuario, Usuario.apellidos as apellidos_usuario, Curso.nombre as nombre_curso, Matricula.tipo_pago, Matricula.fecha_compra, Matricula.notificaciones FROM Matricula, Usuario, Curso WHERE Matricula.id_usuario = Usuario.id AND Matricula.id_curso = Curso.id;";
       
        $txt = $this->seleccionarMatriculas($sql);

        return $txt;
    }

     /**
    * Listara las matriculas de la base de datos en base a la busqueda y llamara a una funcion para pintarlas en la vista.
    * @param String $nombre es el nombre de usuario o curso a buscar en la base de datos
    * @return String $txt devolvera la vista de las matriculas
    */
    public function buscarMatriculas($nombre) {
       $sql = "SELECT Matricula.id, Usuario.nombre as nombre_usuario, Usuario.apellidos as apellidos_usuario, Curso.nombre as nombre_curso, Matricula.tipo_pago, Matricula.fecha_compra, Matricula.notificaciones FROM Matricula, Usuario, Curso WHERE Matricula.id_usuario = Usuario.id AND Matricula.id_curso = Curso.id AND Curso.nombre LIKE '%".$nombre."%' OR Matricula.id_usuario = Usuario.id AND Matricula.id_curso = Curso.id AND Usuario.nombre LIKE '%".$nombre."%' OR Matricula.id_usuario = Usuario.id AND Matricula.id_curso = Curso.id AND Usuario.apellidos LIKE '%".$nombre."%';";

       $txt = $this->seleccionarMatriculas($sql);

       return $txt;
    }

    /**
    * Listara las matriculas de la base de datos con el sql que recibe.
    * @param String $sql es el sql de la peticion
    * @return Array $listaMatricula devolvera un array con las matriculas
    */
    public function consultarMatriculas($sql) {
        $listaMatricula = array();

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id, $nombre_usuario, $apellidos_usuario, $nombre_curso, $tipo_pago, $fecha_compra, $notificaciones) = mysqli_fetch_array($res)) {
            $matricula = new Matricula();
            $matricula->llenarMatricula($id, $nombre_usuario, $apellidos_usuario, $nombre_curso, $tipo_pago, $fecha_compra, $notificaciones);
            array_push($listaMatricula, $matricula);
        }

        return $listaMatricula;
    }

    /**
    * Listara las matriculas de la base de datos con el sql que recibe y llamara a una funcion para pintarlas una a una en la vista.
    * @param String $sql es el sql de la peticion
    * @return String $txt devolvera la vista de las matriculas
    */
    public function seleccionarMatriculas($sql) {
        $listaMatricula = array();
        $listaMatricula = $this->consultarMatriculas($sql);

        $txt = "";
        for($i=0;$i<sizeof($listaMatricula);$i++){
            $txt .= $this->tarjetaMatricula($listaMatricula[$i]);
        }

        return $txt;
    }

    /**
    * Rellenara los campos de una matricula con los datos que recibe del mismo.
    * @param Integer $id
    * @param String $nombre_usuario 
    * @param String $apellidos_usuario
    * @param String $nombre_curso
    * @param Date $fecha_compra
    * @param Boolean $notificaciones
    */
    public function llenarMatricula($id, $nombre_usuario, $apellidos_usuario, $nombre_curso, $tipo_pago, $fecha_compra, $notificaciones){
        $this->id = $id;
        $this->id_usuario = $nombre_usuario." ".$apellidos_usuario;
        $this->id_curso = $nombre_curso;
        $this->tipo_pago = $tipo_pago;
        $this->fecha_compra = $fecha_compra;
        $this->notificaciones = $notificaciones;
    }

    /**
    * Devolvera la vista de la matricula de la base de datos.
    * @param String $curso recibe un curso
    * @return String $txt devolvera la vista del curso
    */
    public function tarjetaMatricula($matricula) {
        $txt = "<div class='tarjetaContainer'>
                <div class='datosContainer' style='width: auto; margin-left: 25px;'>
                    <h3 style='margin-top: 25px;'>".$matricula->getId_usuario()."</h3>
                    <p style='color: #BA4A00;'><b>".$matricula->getId_curso()."</b></p>
                    <p style='margin-bottom: 2.5px;'><i class='fas fa-cash-register'></i> Pago con <b>".$matricula->getTipo_pago()."</b></p>
                    <p style='margin-top: 2.5px;'><i class='fas fa-calendar-alt'></i> Creada/Modificada el <b>".$matricula->getFecha_compra()."</b></p>";

        if ($matricula->getNotificaciones() == 0) {
            $txt .= "<p><b>Notificaciones activas</b> - <span style='color: #C0392B;''><i class='fas fa-times-circle'></i></span></p>";
        }else{
            $txt .= "<p><b>Notificaciones activas</b> - <span style='color: #9FD120;''><i class='fas fa-check-circle'></i></span></p>";
        }

        $txt .= "</div>
                 <div class='botonesContainer'>
                     <button onclick=window.location='matriculasForm.php?id=".$matricula->getId()."';>Editar</button>
                     <button style='background-color: #C0392B;' onclick='borraMatricula(".$matricula->getId().")'>Borrar</button>
                 </div>
             </div>";

        return $txt;
    }

    /**
    * Borrara la matricula que reciba por parametros
    * @param Integer $id_matricula la id de la matricula a borrar
    */
    public function eliminarMatricula($id_matricula) {
        $sql = "DELETE FROM Matricula WHERE id='".$id_matricula."';";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Recoge los datos del formulario y edita una matricula existente en la base de datos.
    * @param Array $datos contenido del POST del formulario
    */
    public function editarMatricula($datos) {
        $this->setId($datos['id']);

        if (isset($datos['notificaciones'])) {
            $this->setNotificaciones(1);
        }else{
            $this->setNotificaciones(0);
        }

        $sql = "UPDATE Matricula SET notificaciones='".$this->notificaciones."' WHERE id='".$this->id."';";
        
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Devolvera true o false en base a si el contenido que recibe la funcion mediante un formulario de matricula esta siendo o no usado ya en la base de datos.
    * @param Array $datos contenido del POST del formulario
    * @return Boolean $ok true o false
    */
    public function registrada($datos){
        $ok = false;
        $sql = "SELECT count(id) as nombre FROM Matricula WHERE id_usuario='".$datos['id_usuario']."' AND id_curso='".$datos['id_curso']."';";

        $conexion = new Bd();

        $res = $conexion->consultaSimple($sql);
        if ($res['nombre'] != 0) {
            $ok = true;
        }
        return $ok;
    }

    /**
    * Devolvera true o false en base a si el usuario esta matriculado en el curso y si la asignatura pertenece al curso.
    * @param String $email variable SESSION de php
    * @param Integer $id_curso la id del curso
    * @param Integer $id_asignatura la id de la asignatura
    * @return Boolean $ok true o false
    */
    public static function verificada($email, $id_curso, $id_asignatura=0){
        $ok = false;
        $conexion = new Bd();
        $usuario = new Usuario();
        $id = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT count(id) as verificada FROM Matricula WHERE id_usuario='".$id."' AND id_curso='".$id_curso."';";
        $sql2 = "SELECT count(id) as verificada FROM Asignatura WHERE id='".$id_asignatura."' AND id_curso='".$id_curso."';";

        
        $res = $conexion->consultaSimple($sql);
        $res2 = $conexion->consultaSimple($sql2);

        if($id_asignatura != 0 && $res['verificada'] != 0 && $res2['verificada'] != 0 || $id_asignatura == 0 && $res['verificada'] != 0){
            $ok = true;
        }
        return $ok;
    }

    /**
    * Funcion que pintara una lista de cursos y si recibes notificaciones de ellos
    * @param String $email variable SESSION de php
    * @return String $txt devolvera la vista de las notificaciones de las matriculas
    */
    public function notificacionesMatriculas($email) {
        $usuario = new Usuario();
        $id_usuario = $usuario->obtenerIdPorEmail($email);

        $sql = "SELECT Matricula.id, Curso.nombre, Matricula.notificaciones FROM Matricula, Curso WHERE Matricula.id_curso = Curso.id AND Matricula.id_usuario = ".$id_usuario.";";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        $txt = "";
        $i = 0;
        while (list($id, $nombre, $notificaciones) = mysqli_fetch_array($res)) {
            $txt .= "<ul><li><label><i class='fas fa-hand-point-right'></i><b> ".$nombre."</b></label></li><li>";
            if ($notificaciones == 1) {
                $txt .= "<input type='button' style='background-color: #149F1C;' onclick='ajustarNotificaciones(".$id.", 0)' value='Recibir Notificaciones'>";
            }else{
                $txt .= "<input type='button' style='background-color: #A80707;' onclick='ajustarNotificaciones(".$id.", 1)' value='NO Recibir Notificaciones'>";
            }
            $txt .= "</li></ul>";
            $i++;
        }

        return $txt;
    }

    /**
    * Cambiara en al base de datos si recibes notificaciones de un curso
    * @param Array $datos contenido del POST del formulario
    */
    public function cambiarNotificaciones($datos) {
        $sql = "UPDATE Matricula SET notificaciones = ".$datos['notificaciones']." WHERE id = ".$datos['matricula'].";";

        $conexion = new Bd();
        $conexion->consulta($sql);
    }
}

/**
* Esta es la clase tarjeta bancaria
* @Author OmithionEducational
* @Version 0.1
*/
class TarjetaBancaria {
    //variables de la clase
    private $id;
    private $id_usuario;
    private $titular;
    private $numero;
    private $mes;
    private $ano;
    private $codigo;

    /**
    * Constructor vacio
    */
    public function __construct(){
        
    }

    //Getter y Setter del Integer Id
    public function getId() {return $this->id;}
    public function setId($id) {$id = intval($id);$this->id = $id;}    

    //Getter y Setter del Integer Id_usuario
    public function getId_usuario() {return $this->id_usuario;}
    public function setId_usuario($id_usuario) {$this->id_usuario = addslashes($id_usuario);}

    //Getter y Setter del String
    public function getTitular() {return $this->titular;}
    public function setTitular($titular) {$this->titular = addslashes($titular);}

    //Getter y Setter del String Numero
    public function getNumero() {return $this->numero;}
    public function setNumero($numero) {$this->numero = addslashes($numero);}

    //Getter y Setter del String Mes
    public function getMes() {return $this->mes;}
    public function setMes($mes) {$this->mes = addslashes($mes);}

    //Getter y Setter del String Ano
    public function getAno() {return $this->ano;}
    public function setAno($ano) {$this->ano = addslashes($ano);}

    //Getter y Setter del String Codigo
    public function getCodigo() {return $this->codigo;}
    public function setCodigo($codigo) {$this->codigo = addslashes($codigo);}

    /**
    * Esta funcion crea una nueva tarjeta para el usuario en cuestion
    * @param Array $datos son los datos del formulario
    */
    public function nuevaTarjeta($datos){
        $this->setId_usuario($datos['id_usuario']);
        $this->setTitular($datos['nombreTarjeta']);
        $this->setNumero($datos['numeroTarjeta']);
        $this->setMes($datos['mesTarjeta']);
        $this->setAno($datos['anoTarjeta']);
        $this->setCodigo($datos['cvvTarjeta']);

        $sql = "INSERT INTO Tarjeta_Bancaria(id_usuario, titular, numero, mes, ano, codigo) VALUES ('".$this->id_usuario."', '".$this->titular."', '".$this->numero."', '".$this->mes."', '".$this->ano."', '".$this->codigo."');";
        $conexion = new Bd();
        $conexion->consulta($sql);
    }

    /**
    * Listara las tarjetas de un usuario y llamara a una funcion para pintarlas en la vista.
    * @param Integer $id es la id del usuario
    * @return String $txt devolvera la vista de las tarjetas
    */
    public function listarTarjetas($id) {
        $listaTarjeta = array();
        $sql = "SELECT id, titular, numero, mes, ano FROM Tarjeta_Bancaria WHERE id_usuario = '".$id."';";

        $conexion = new Bd();
        $res = $conexion->consulta($sql);

        while (list($id, $titular, $numero, $mes, $ano) = mysqli_fetch_array($res)) {
            $tarjetaBancaria = new TarjetaBancaria();
            $tarjetaBancaria->llenarTarjeta($id, $titular, $numero, $mes, $ano);
            array_push($listaTarjeta, $tarjetaBancaria);
        }

        $txt = "";
        for($i=0;$i<sizeof($listaTarjeta);$i++){
            $txt .= $this->impresionTarjeta($listaTarjeta[$i]);
        }

        return $txt;
    }

    /**
    * Rellenara los campos de una tarjeta con los datos que recibe de la misma.
    * @param Integer $id
    * @param String $titular
    * @param String $numero
    * @param Integer $mes
    * @param Integer $ano
    */
    public function llenarTarjeta($id, $titular, $numero, $mes, $ano){
        $this->id = $id;
        $this->titular = $titular;
        $this->numero = $numero;
        $this->mes = $mes;
        $this->ano = $ano;
    }

    /**
    * Devolvera la vista de la tarjeta de la base de datos.
    * @param String $tarjeta recibe un tarjeta
    * @return String $txt devolvera la vista del tarjeta
    */
    public function impresionTarjeta($tarjeta) {
        $txt = "<ul id='miTarjeta".$tarjeta->getId()."' class='misTarjetas' onclick='seleccionarTarjeta(".$tarjeta->getId().")'>
                        <li><i class='far fa-credit-card'></i></li>
                        <li>Termina en ".substr($tarjeta->getNumero(),-4)."</li>
                        <li>".strtoupper($tarjeta->getTitular())."</li>
                        <li>".str_pad($tarjeta->getMes(), 2, "0", STR_PAD_LEFT)." / 20".$tarjeta->getAno()."</li>
                    </ul>";

        return $txt;
    }

    /**
    * Devolvera una tarjeta de la base de datos.
    * @param Integer $id es la id de la tarjeta
    * @return TarjetaBancaria $tarjetaBancaria la tarjeta que devuelve
    */
    public function obtenerTarjetaPorId($id) {
        $sql = "SELECT id, titular, numero, mes, ano FROM Tarjeta_Bancaria WHERE id = '".$id."';";

        $conexion = new Bd();
        $res = $conexion->consultaSimple($sql);

        $tarjetaBancaria = new TarjetaBancaria();
        $tarjetaBancaria->llenarTarjeta($res['id'], $res['titular'], $res['numero'], $res['mes'], $res['ano']);

        return $tarjetaBancaria;
    }
}