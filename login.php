<!DOCTYPE html>
<html>
<head>
	<title>Login | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
</head>
<body>
	<header id="headerLogin">
		<div id="atrasLogin"><a href="login.php"><i class="fas fa-arrow-left"></i></a></div>
		<img id="imagenLogin" class="imagenNormalLogin" src="images/logo_blanco.png">
	</header>
	<section id="sectionLogin">
		<div id="botoneraLogin">
			<p>Bienvenido a Omithion Educational, inicia sesión o registrate para acceder al campus.</p>
			<button style="background-color: #9FD120;" onclick="ver_login()">Acceder</button>
			<button style="background-color: #FFBA00;" onclick="ver_registro()">Registrarse</button>
		</div>
		<div id="loginLogin">
			<h1 style="color: #FFBA00;">Iniciar sesión</h1>
			<form name="login" action="" method="post">
				<ul>
					<li><label class="labelLogin"><span class="alertaLogin"><i class="fas fa-exclamation-circle"></i></span> Email</label></li>
					<li><input type="email" name="email" onblur="login_vacio(this, 0)" placeholder="Escribe tu email"></li>
				</ul>
				<ul>
					<li><label class="labelLogin"><span class="alertaLogin"><i class="fas fa-exclamation-circle"></i></span> Contraseña</label></li>
					<li><input type="password" name="pass" onblur="login_vacio(this, 1)" placeholder="Escribe tu contraseña"></li>
				</ul>
				<p><a href="javascript:ver_recordar()">¿Has olvidado la contraseña?</a></p>
				<input type="button" name="enviar" onclick="validar_login()" value="Acceder">
			</form>
		</div>
		<div id="recuperarLogin">
			<h1 style="color: #FFBA00;">Recuperar contraseña</h1>
			<form name="recordar" action="src/controlador/usuario.main.php?opcion=12" method="post">
				<ul>
					<li><label class="labelLogin"><span class="alertaLogin"><i class="fas fa-exclamation-circle"></i></span> Email</label></li>
					<li><input type="email" name="email" onblur="login_vacio(this, 2)" placeholder="Escribe tu email"></li>
				</ul>
				<input type="button" name="enviar" onclick="validar_recordar()" value="Enviar correo">
			</form>
		</div>
		<div id="registroLogin">
			<h1 style="color: #9FD120;">Crea tu cuenta</h1>
			<form name="registro" action="" method="post">
				<div id="registroNombre">
					<ul>
						<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Nombre</label></li>
						<li><input type="text" name="nombre" onblur="registro_vacio(this, 0)" placeholder="Escribe tu nombre"></li>
					</ul>
					<ul>
						<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Apellidos</label></li>
						<li><input type="text" name="apellidos" onblur="registro_vacio(this, 1)" placeholder="Escribe tus apellidos"></li>
					</ul>
				</div>
				<ul>
					<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Correo electrónico</label></li>
					<li><input type="email" name="email" onblur="registro_vacio(this, 2)" placeholder="Escribe tu correo electronico"></li>
				</ul>
				<ul>
					<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> DNI</label></li>
					<li><input type="text" name="dni" onblur="registro_vacio(this, 3)" placeholder="Escribe tu DNI completo"></li>
				</ul>
				<ul>
					<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Fecha de nacimiento</label></li>
					<li><input type="date" name="fecha" onblur="registro_vacio(this, 4)" placeholder="Introduce tu fecha de nacimiento"></li>
				</ul>
				<div id="registroPass">
					<ul>
						<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Contraseña</label></li>
						<li><input type="password" name="pass" onblur="registro_vacio(this, 5)" placeholder="Escribe tu contraseña"></li>
					</ul>
					<ul>
						<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Repetir contraseña</label></li>
						<li><input type="password" name="pass2" onblur="registro_vacio(this, 6)" placeholder="Repite tu contraseña"></li>
					</ul>
				</div>
				<div id="registroCaptcha">
					<ul>
						<li><img id="imgCaptcha" src="./src/captcha/captcha.php"></li>
					</ul>
					<ul>
						<li><label class="labelRegistro"><span class="alertaRegistro"><i class="fas fa-exclamation-circle"></i></span> Captcha</label></li>
						<li><input id="captcha" type="text" name="captcha" onblur="registro_vacio(this, 7)" placeholder="Introduce el captcha" maxlength="6"></li>
					</ul>
				</div>
				<input type="button" name="enviar" onclick="validar_registro()" value="Registrarse">
			</form>
		</div>
	</section>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<footer id="footerLogin">
		<p>© 2019 Omithion Educational. Todos los derechos reservados.</p>
		<p><a href="https://twitter.com/omithion"><i class="fab fa-twitter fa-3x"></i></a><a id="androidLogin" href="files/OmithionEducational.apk" download><i class="fab fa-android fa-3x"></i></a></p>
	</footer>
</body>
</html>