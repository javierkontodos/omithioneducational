function ver_login() {
	document.getElementById('loginLogin').style.display = "block";
	document.getElementById('atrasLogin').style.display = "block";
	document.getElementById('botoneraLogin').style.display = "none";
	document.getElementById('imagenLogin').className = "imagenReducidaLogin";
}
function ver_registro() {
	document.getElementById('registroLogin').style.display = "block";
	document.getElementById('atrasLogin').style.display = "block";
	document.getElementById('botoneraLogin').style.display = "none";
    document.getElementById('footerLogin').style.display = "none";
	document.getElementById('imagenLogin').className = "imagenReducidaLogin";
}
function ver_recordar() {
    document.getElementById('loginLogin').style.display = "none";
    document.getElementById('recuperarLogin').style.display = "inline-block";
}

function cerrarAlerta() {
    document.getElementById('alertaPantalla').style.display = "none";
    document.getElementById('alertaMensaje').style.display = "none";
}

function menuMovil(posicion) {
	if (posicion == 1) {
		document.getElementById('menuMovil').style.display = "none";
		document.getElementById('menuMovil2').style.display = "inline-block";
		document.getElementsByTagName('nav')[0].style.display = "inline-block";
	}else if (posicion == 2){
		document.getElementById('menuMovil').style.display = "inline-block";
		document.getElementById('menuMovil2').style.display = "none";
		document.getElementsByTagName('nav')[0].style.display = "none";
	}
}


function registro_vacio(elemento, label){
	if (elemento.value == "") {
		elemento.style.borderColor = "red";
		document.getElementsByClassName('labelRegistro')[label].style.color = "red";
		document.getElementsByClassName('alertaRegistro')[label].style.display = "inline-block";
	}else{
		elemento.style.borderColor = "black";
		document.getElementsByClassName('labelRegistro')[label].style.color = "black";
		document.getElementsByClassName('alertaRegistro')[label].style.display = "none";
	}
}
function login_vacio(elemento, label){
	if (elemento.value == "") {
		elemento.style.borderColor = "red";
		document.getElementsByClassName('labelLogin')[label].style.color = "red";
		document.getElementsByClassName('alertaLogin')[label].style.display = "inline-block";
	}else{
		elemento.style.borderColor = "black";
		document.getElementsByClassName('labelLogin')[label].style.color = "black";
		document.getElementsByClassName('alertaLogin')[label].style.display = "none";
	}
}
function completa_perfil_vacio(elemento, label){
	if (elemento.value == "") {
		elemento.style.borderColor = "red";
		document.getElementsByTagName('label')[label].style.color = "red";
		document.getElementsByTagName('span')[label-1].style.display = "inline-block";
	}else{
		elemento.style.borderColor = "black";
		document.getElementsByTagName('label')[label].style.color = "black";
		document.getElementsByTagName('span')[label-1].style.display = "none";
	}
}

function validar_login() {
    var ok = true;
    var formulario = document.login;
    for (var i=0;i<formulario.elements.length;i++){
        if (formulario.elements[i]!=formulario.enviar) {
            if(!formulario.elements[i].value){
        		login_vacio(formulario.elements[i],i);
           	 ok = false;
        	}else{
        		login_vacio(formulario.elements[i],i);
        	} 
        }
    }
    if (ok) {
        realizarLogin();
    }
}
function validar_registro() {
    var ok = true;
    var formulario = document.registro;
    for (var i=0;i<8;i++){
        if (formulario.elements[i]!=formulario.enviar) {
            if(!formulario.elements[i].value){
        		registro_vacio(formulario.elements[i],i);
           	 ok = false;
        	}else{
        		registro_vacio(formulario.elements[i],i);
        	} 
        }
        if (formulario.elements[i]==formulario.pass2 && formulario.elements[i].value!=formulario.pass.value){
            formulario.elements[i].style.borderColor = "red";
			document.getElementsByClassName('labelRegistro')[i].style.color = "red";
			document.getElementsByClassName('alertaRegistro')[i].style.display = "inline-block";
            ok = false;
        }
    }
    if (ok) {
    	compruebaCaptcha();
    }
}
function validar_recordar() {
    var ok = true;
    var formulario = document.recordar;

    if(!formulario.email.value){
        login_vacio(formulario.email,2);
        ok = false;
    }

    if (ok) {
        formulario.submit();
    }
}
function validar_recuperar() {
    var ok = true;
    var formulario = document.recuperar;

    if(!formulario.pass.value){
        login_vacio(formulario.pass, 0);
        ok = false;
    }
    if(!formulario.pass2.value){
        login_vacio(formulario.pass2, 1);
        ok = false;
    }
    if (formulario.pass.value != formulario.pass2.value) {
        login_vacio(formulario.pass2, 1);
        ok = false;
    }

    if (ok) {
        realizarRecuperacion();
    }
}

function validar_completa_perfil() {
    var ok = true;
    var formulario = document.completa_perfil;

    for (var i=0;i<10;i++){
        switch (formulario.elements[i]){
            case formulario.biografia:
                if (!formulario.biografia.value) {
                    completa_perfil_vacio(formulario.elements[i],1);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],1);
                }
                break;
            case formulario.direccion:
                if (!formulario.direccion.value) {
                    completa_perfil_vacio(formulario.elements[i],2);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],2);
                }
                break;
            case formulario.cod_postal:
                if (!formulario.cod_postal.value) {
                    completa_perfil_vacio(formulario.elements[i],3);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],3);
                }
                break;
            case formulario.pais:
                if (!formulario.pais.value) {
                    completa_perfil_vacio(formulario.elements[i],4);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],4);
                }
                break;
            case formulario.poblacion:
                if (!formulario.poblacion.value) {
                    completa_perfil_vacio(formulario.elements[i],5);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],5);
                }
                break;
            case formulario.provincia:
                if (!formulario.provincia.value) {
                    completa_perfil_vacio(formulario.elements[i],6);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],6);
                }
                break;
            case formulario.movil:
                if (!formulario.movil.value) {
                    completa_perfil_vacio(formulario.elements[i],7);
                    ok = false;
                }else {
                    completa_perfil_vacio(formulario.elements[i],7);
                }
                break;
        }
    }

    if (ok) {
        formulario.submit();
    }
}

function seleccionarTema(num) {
    document.completa_perfil.tema[num].checked = true;
    document.getElementsByClassName('check')[0].style.borderColor = "#909497";
    document.getElementsByClassName('check')[1].style.borderColor = "#909497";
    document.getElementsByClassName('check')[2].style.borderColor = "#909497";
    switch (num) {
        case 0:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#152770";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#152770";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#152770B3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
        case 1:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#C0392B";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#C0392B";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#C0392BB3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
        case 2:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#58B616";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#58B616";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#58B616B3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
        case 3:
            document.getElementsByTagName('header')[0].style.backgroundColor = "#8E44AD";
            document.getElementsByTagName('nav')[0].style.backgroundColor = "#8E44AD";
            document.getElementsByTagName('footer')[0].style.backgroundColor = "#8E44ADB3";
            document.getElementsByClassName('check')[num].style.borderColor = "#F39C12";
            break;
    }
}

/** AJAX **/
function getXMLHTTPRequest() {
    try {
        req = new XMLHttpRequest();
    } catch(err1) {
        try {
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (err2) {
            try {
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (err3) {
                req = false;
            }
        }
    }
    return req;
}

var bCaptcha = getXMLHTTPRequest();
function compruebaCaptcha() {
    var captcha = document.getElementById('captcha').value;
    var myurl = 'src/llamadas/compruebaCaptcha.php';
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&codigo='+captcha;
    bCaptcha.open("GET", modurl, true); //true es que la conexion es asincrona
    bCaptcha.onreadystatechange = compruebaCaptchaResponse;
    bCaptcha.send(null);
}
function compruebaCaptchaResponse() {
    if (bCaptcha.readyState == 4) {
        if (bCaptcha.status == 200) {
            var miTexto = bCaptcha.responseText;

            if (miTexto == "true") {
            	realizarRegistro();
            }else{
    			document.registro.elements[7].style.borderColor = "red";
    			document.getElementsByClassName('labelRegistro')[7].style.color = "red";
				document.getElementsByClassName('alertaRegistro')[7].style.display = "inline-block";
    		}
        }
    }
}

var bSubeFoto = getXMLHTTPRequest();
function subeFotoUsuario() {
    var fotoAntigua = document.getElementById('selectedFoto').value;
    var myurl = 'src/llamadas/cambiarFotoUser.php';
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&fotoAntigua='+fotoAntigua;
    bSubeFoto.open("POST", modurl, true); //true es que la conexion es asincrona
    bSubeFoto.onreadystatechange = subeFotoUsuarioResponse;

    var fileInput = document.getElementById('selectedFile');
	var file = fileInput.files[0];
	var formData = new FormData();
	formData.append('foto', file);

	bSubeFoto.send(formData);
}
function subeFotoUsuarioResponse() {
    if (bSubeFoto.readyState == 4) {
        if (bSubeFoto.status == 200) {
            var miTexto = bSubeFoto.responseText;

            if (miTexto != "grande") {
                if (miTexto == "tipo") {
                    alertaPantalla("Archivo no reconocido.", "El archivo que intentas subir no se corresponde al de una imagen.", "Intentalo de nuevo con un archivo de tipo .jpg, .jpeg o .png.");
                }else{
                    document.getElementById('fotoUser').style.backgroundImage = "url('"+miTexto+"')";
                    document.getElementById('selectedFoto').value = miTexto;
                }
            }else {
                alertaPantalla("Archivo demasiado grande", "El archivo no puede superar los 2 Mb de tamaño.", "Intentalo de nuevo con un archivo mas pequeño.");
            }
        }
    }
}

var bCambioEmail = getXMLHTTPRequest();
function cambiarEmail() {
    var email = document.getElementById('email').value;
    var myurl = 'src/controlador/usuario.main.php?opcion=4&email='+email;
    bCambioEmail.open("GET", myurl, true); //true es que la conexion es asincrona
    bCambioEmail.onreadystatechange = cambiarEmailResponse;
    bCambioEmail.send(null);
}
function cambiarEmailResponse() {
    if (bCambioEmail.readyState == 4) {
        if (bCambioEmail.status == 200) {
            var miTexto = bCambioEmail.responseText;
            
            if (miTexto == "true") {
                location.href ="index.php";
            }else{
                if (miTexto == "erroneo") {
                    alertaPantalla("Correo no valido", "El correo electronico introducido es erroneo o ya esta siendo utilizado.", "Intentalo de nuevo o prueba a revisar la carpeta de spam de tu servicio de correo.");
                }

                if (miTexto == "vacio") {
                    alertaPantalla("Algo ha salido mal...", "Necesitas introducir un correo para poder continuar.", "Intentalo de nuevo escribiendo un correo electronico valido.");
                }
            }
        }
    }
}

var bCodigo = getXMLHTTPRequest();
function compruebaCodigo() {
    var codigo = document.getElementById('codigo').value;
    var myurl = 'src/controlador/usuario.main.php?opcion=6&codigo='+codigo;
    bCodigo.open("GET", myurl, true); //true es que la conexion es asincrona
    bCodigo.onreadystatechange = compruebaCodigoResponse;
    bCodigo.send(null);
}
function compruebaCodigoResponse() {
    if (bCodigo.readyState == 4) {
        if (bCodigo.status == 200) {
            var miTexto = bCodigo.responseText;
            if (miTexto == "true") {
                location.href ="index.php";
            }else if (miTexto == "false") {
                alertaPantalla("Algo ha salido mal...", "El codigo introducido es erroneo o éste ha caducado.", "Intentalo de nuevo o prueba a reenviar el codigo o cambiar el correo electronico.");
            }
        }
    }
}

var bLogin = getXMLHTTPRequest();
function realizarLogin() {
    reemail = document.login.email.value;
    repass = document.login.pass.value;
    misdatos = "email="+reemail+"&pass="+repass;

    var myurl = 'src/controlador/usuario.main.php?opcion=1';
    bLogin.open("POST", myurl, true); //true es que la conexion es asincrona
    bLogin.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bLogin.onreadystatechange = realizarLoginResponse;
    bLogin.send(misdatos);
}
function realizarLoginResponse() {
    if (bLogin.readyState == 4) {
        if (bLogin.status == 200) {
            var miTexto = bLogin.responseText;
            if (miTexto == "true") {
                location.href ="index.php";
            }else if (miTexto == "false") {
                alertaPantalla("Algo ha salido mal...", "La contraseña o email introducidos son erroneos", "Intentalo de nuevo o haz click en el boton de 'he olvidado mi contraseña'.");
            }
        }
    }
}

var bRegistro = getXMLHTTPRequest();
function realizarRegistro() {
    renombre = document.registro.nombre.value;
    reapellidos = document.registro.apellidos.value;
    reemail = document.registro.email.value;
    redni = document.registro.dni.value;
    refecha = document.registro.fecha.value;
    repass = document.registro.pass.value;
    misdatos = "nombre="+renombre+"&apellidos="+reapellidos+"&email="+reemail+"&dni="+redni+"&fecha="+refecha+"&pass="+repass;

    var myurl = 'src/controlador/usuario.main.php?opcion=3';
    bRegistro.open("POST", myurl, true); //true es que la conexion es asincrona
    bRegistro.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bRegistro.onreadystatechange = realizarRegistroResponse;
    bRegistro.send(misdatos);
}
function realizarRegistroResponse() {
    if (bRegistro.readyState == 4) {
        if (bRegistro.status == 200) {
            var miTexto = bRegistro.responseText;
            if (miTexto == "true") {
                location.href ="index.php";
            }else if (miTexto == "false") {
                alertaPantalla("Algo ha salido mal...", "El email introducido ya esta siendo usado", "Intentalo de nuevo con otro email o haz click en el boton de 'he olvidado mi contraseña' del login.");
            }
        }
    }
}

var bRecuperacion = getXMLHTTPRequest();
function realizarRecuperacion() {
    reid = document.recuperar.id.value;
    recode = document.recuperar.code.value;
    repass = document.recuperar.pass.value;
    misdatos = "id="+reid+"&code="+recode+"&pass="+repass;

    var myurl = 'src/controlador/usuario.main.php?opcion=13';
    bRecuperacion.open("POST", myurl, true); //true es que la conexion es asincrona
    bRecuperacion.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bRecuperacion.onreadystatechange = realizarRecuperacionResponse;
    bRecuperacion.send(misdatos);
}
function realizarRecuperacionResponse() {
    if (bRecuperacion.readyState == 4) {
        if (bRecuperacion.status == 200) {
            var miTexto = bRecuperacion.responseText;
            if (miTexto == "true") {
                location.href ="login.php";
            }else if (miTexto == "false") {
                alertaPantalla("Algo ha salido mal...", "El codigo asociado al enlace ha caducado", "Intentalo de nuevo haciendo click en el boton de 'he olvidado mi contraseña' del login.");
            }
        }
    }
}

var bSubeRecibo = getXMLHTTPRequest();
function reciboMatricula() {
    var reciboAntiguo = document.getElementById('selectedRecibo').value;
    var myurl = 'src/llamadas/subirReciboMatricula.php';
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?rand='+myRand+'&reciboAntiguo='+reciboAntiguo;
    bSubeRecibo.open("POST", modurl, true); //true es que la conexion es asincrona
    bSubeRecibo.onreadystatechange = reciboMatriculaResponse;

    var fileInput = document.getElementById('selectedFile');
    var file = fileInput.files[0];
    var formData = new FormData();
    formData.append('recibo', file);

    bSubeRecibo.send(formData);
}
function reciboMatriculaResponse() {
    if (bSubeRecibo.readyState == 4) {
        if (bSubeRecibo.status == 200) {
            var miTexto = bSubeRecibo.responseText;

            if (miTexto != "grande") {
                document.getElementById('selectedRecibo').value = miTexto;
                document.getElementById('nombreFile').innerHTML = miTexto.substr(24);
            }else {
                alertaPantalla("Archivo demasiado grande", "El archivo no puede superar los 2 Mb de tamaño.", "Intentalo de nuevo con un archivo mas pequeño o comprime el actual.");
            }
        }
    }
}

function alertaPantalla(titulo, subtitulo, mensaje) {
    document.getElementById('alertaPantalla').style.display = "inline-block";
    document.getElementById('alertaMensaje').style.display = "inline-block";
    document.getElementById('alertaH2').innerHTML = titulo;
    document.getElementById('alertaH3').innerHTML = subtitulo;
    document.getElementById('alertaP').innerHTML = mensaje;
}

function anadirTarjeta(){
    for (var i = 0; i < document.getElementsByClassName('misTarjetas').length; i++) {
        document.getElementsByClassName('misTarjetas')[i].style.border = "none";
        document.getElementsByClassName('misTarjetas')[i].style.color = "#000";
        document.getElementsByClassName('misTarjetas')[i].style.backgroundColor = "#00000000";
    }
    document.getElementById('idTarjetaBancaria').value = "";

    if (document.getElementById('nuevaTarjeta').style.display == "none") {
        document.getElementById('nuevaTarjeta').style.display = "block";
        document.getElementById('opcionesTarjeta').style.display = "none";
    }else{
        document.getElementById('nuevaTarjeta').style.display = "none";
        document.getElementById('opcionesTarjeta').style.display = "block";
    }
    
}

function seleccionarTarjeta(num) {
    for (var i = 0; i < document.getElementsByClassName('misTarjetas').length; i++) {
        document.getElementsByClassName('misTarjetas')[i].style.border = "none";
        document.getElementsByClassName('misTarjetas')[i].style.color = "#000";
        document.getElementsByClassName('misTarjetas')[i].style.backgroundColor = "#00000000";
    }
    document.getElementById('miTarjeta'+num).style.border = "0.5px solid #FFBA00";
    document.getElementById('miTarjeta'+num).style.color = "#9FD120";
    document.getElementById('miTarjeta'+num).style.backgroundColor = "#FEF7E4";
    document.getElementById('idTarjetaBancaria').value = num;
}

function seleccionarPago(elemeto){
    document.getElementById('opcionesTarjeta').style.display = "none";
    document.getElementById('nuevaTarjeta').style.display = "none";
    document.getElementById('nuevaTransferencia').style.display = "none";
    document.getElementById('botonesPago').style.display = "none";
    if (elemeto.value == 1) {
        document.getElementById('opcionesTarjeta').style.display = "inline-block";
        document.getElementById('botonesPago').style.display = "inline-block";
    }else if(elemeto.value == 2){
        document.getElementById('nuevaTransferencia').style.display = "inline-block";
        document.getElementById('botonesPago').style.display = "inline-block";
    }
}

function cursoPlegable(num) {
    if (document.getElementById('cursosPlegables'+num+'').style.display == "none") {
        document.getElementById('cursosPlegables'+num+'').style.display="block"
        document.getElementById('iconosPlegables'+num+'').innerHTML = "<i class='fas fa-chevron-circle-down'></i>";
    }else{
        document.getElementById('cursosPlegables'+num+'').style.display="none";
        document.getElementById('iconosPlegables'+num+'').innerHTML = "<i class='fas fa-chevron-circle-right'></i>";
    }
}

function redesPlegable() {
    if (document.getElementById('redesPerfil').style.display == "none") {
        document.getElementById('redesPerfil').style.display="block"
        document.getElementById('redesPlegablePerfil').innerHTML = "<a href='javascript:redesPlegable()'><span><i class='fas fa-chevron-circle-down'></i></span> Redes sociales</a>";
    }else{
        document.getElementById('redesPerfil').style.display="none";
        document.getElementById('redesPlegablePerfil').innerHTML = "<a href='javascript:redesPlegable()'><span><i class='fas fa-chevron-circle-right'></i></span> Redes sociales</a>";
    }
}

function ayudaPlegable(num) {
    if (document.getElementById('ayudaPlegable'+num+'').style.display == "none") {
        document.getElementById('ayudaPlegable'+num+'').style.display="block"
        document.getElementById('iconoPlegable'+num+'').innerHTML = "<i class='fas fa-chevron-circle-down'></i>";
    }else{
        document.getElementById('ayudaPlegable'+num+'').style.display="none";
        document.getElementById('iconoPlegable'+num+'').innerHTML = "<i class='fas fa-chevron-circle-right'></i>";
    }
}

function imagenHoverPerfil(borde, num){
    if (num == 1) {
        document.getElementById('cambiaFotoPerfil').style.color = "#000";
        borde.style.boxShadow = "0 0 5em #424949";
    }else{
        document.getElementById('cambiaFotoPerfil').style.color = "#f2f3f4";
        borde.style.boxShadow = "0 0 1em #979A9A";
    }
}

function recibirNotificaciones(boton) {
    if (document.getElementById('notificacionesCheck').checked) {
        document.getElementById('notificacionesCheck').checked = false;
        boton.style.backgroundColor = "#A80707";
        boton.value = "NO Recibir Notificaciones";
    }else{
        document.getElementById('notificacionesCheck').checked = true;
        boton.style.backgroundColor = "#149F1C";
        boton.value = "Recibir Notificaciones";
    }
}

function recordarTarjetaCheck(boton) {
    if (document.getElementById('recordarCheck').checked) {
        document.getElementById('recordarCheck').checked = false;
        boton.style.backgroundColor = "#A80707";
        boton.value = "NO Recordar";
    }else{
        document.getElementById('recordarCheck').checked = true;
        boton.style.backgroundColor = "#149F1C";
        boton.value = "Recordar";
    }
}

function verifica_matriculacion() {
    var ok = true;
    var formulario = document.nuevaMatricula;

    if (formulario.tipo_pago.value == 1 || formulario.tipo_pago.value == 2) {
        if (formulario.tipo_pago.value == 1 && !formulario.idTarjetaBancaria.value){
            if(formulario.numeroTarjeta.value && formulario.cvvTarjeta.value && formulario.nombreTarjeta.value){

            }else{
                ok = false
                alertaPantalla("Tarjeta Bancaria", "Para completar la matricula debes selecciona un tarjeta valida o crear una nueva", "Intentalo de nuevo seleccionando una tarjeta valida o creando una nueva.");
            }
        }
        if (formulario.tipo_pago.value == 2 && !formulario.recibo.value) {
            ok = false
            alertaPantalla("Transferencia Bancaria", "Para completar la matricula debes subir un recibo valido", "Intentalo de nuevo subiendo un recibo valido.");
        }
    }else{
        alertaPantalla("Tipo de pago", "Para completar la matricula debes selecciona un tipo de pago valido", "Intentalo de nuevo seleccionando un tipo de pago valido.");
    }

    if (ok) {
        formulario.submit();
    }
}

function ajustarTema(pos, color) {
    for (var i = 0; i < document.getElementsByClassName('check').length; i++) {
        document.getElementsByClassName('check')[i].style.borderColor = "#909497";
    }

    document.getElementsByName('tema')[pos].checked = true;
    if (document.getElementsByTagName('header')[0]) {
        document.getElementsByTagName('header')[0].style.backgroundColor = color;
        document.getElementsByTagName('nav')[0].style.backgroundColor = color;
        document.getElementsByTagName('footer')[0].style.backgroundColor = color+"B3";
    }else{
        document.getElementsByTagName('aside')[0].style.backgroundColor = color;
        document.getElementById('asidePlegado').style.backgroundColor = color;
        document.getElementById('asideMovilPlegado').style.backgroundColor = color;
    }
    
    document.getElementsByClassName('check')[pos].style.borderColor = "#F39C12";
}

var bAjustaNotificaciones = getXMLHTTPRequest();
function ajustarNotificaciones(matricula, notificaciones) {
    var myurl = 'src/controlador/matricula.main.php';
    myRand = parseInt(Math.random()*999999999999999);
    modurl = myurl+'?opcion=4&matricula='+matricula+'&notificaciones='+notificaciones;
    bAjustaNotificaciones.open("GET", modurl, true); //true es que la conexion es asincrona
    bAjustaNotificaciones.onreadystatechange = ajustarNotificacionesResponse;
    bAjustaNotificaciones.send(null);
}
function ajustarNotificacionesResponse() {
    if (bAjustaNotificaciones.readyState == 4) {
        if (bAjustaNotificaciones.status == 200) {
            var miTexto = bAjustaNotificaciones.responseText;
            if (miTexto == "false") {
                alertaPantalla("Error inesperado", "Algo ha sudecido mal con tu petición.", "Intentalo de nuevo más tarde.");
            }else{
                location.href = "ajustes.php";
            }
        }
    }
}

var bAjustaPassword = getXMLHTTPRequest();
function cambiarPassword(ruta) {
    currentPass = document.pass_ajustes.currentPass.value;
    newPass = document.pass_ajustes.newPass.value;
    repeatPass = document.pass_ajustes.repeatPass.value;
    misdatos = "currentPass="+currentPass+"&newPass="+newPass+"&repeatPass="+repeatPass;

    var myurl = ruta+'/controlador/usuario.main.php?opcion=14';
    bAjustaPassword.open("POST", myurl, true); //true es que la conexion es asincrona
    bAjustaPassword.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bAjustaPassword.onreadystatechange = cambiarPasswordResponse;
    bAjustaPassword.send(misdatos);
}
function cambiarPasswordResponse() {
    if (bAjustaPassword.readyState == 4) {
        if (bAjustaPassword.status == 200) {
            var miTexto = bAjustaPassword.responseText;

            if (miTexto != "false") {
                if (miTexto == "error") {
                    alertaPantalla("Contraseña erronea", "La contraseña actual que has introducido no es correcta.", "Intentalo de nuevo con la contraseña actual correcta.");
                }else if(miTexto == "coincidencia"){
                    alertaPantalla("Contraseñas erroneas", "Las contraseñas que has elegido no coinciden.", "Intentalo de nuevo con unas contraseñas que coincidan correctamente.");
                }else {
                    location.href = location.href;
                }
            }else{
                alertaPantalla("Datos no validos", "Los datos que has introducido no son validos o estan vacios.", "Intentalo de nuevo rellenando correctamente el formulario.");
            }
            
        }
    }
}
