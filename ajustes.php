<?php 
	require 'includes/verificacion.inc.php';
	require_once 'src/modelo/tema.class.php';
	require_once 'src/modelo/matricula.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ayuda | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion3').className = "activo";
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section id="ajustesView">
		<h1>Ajustes personales</h1>
		<p style="text-align: center;">Si quieres cambiar la contraseña, cambiar tu tema principal o modificar las notificaciónes de tus matriculas aqui podras gestionarlo.</p>

		<h3><a><i class='fas fa-chevron-circle-down'></i> Modificar el tema</a></h3>
		<div id="temaAjustes">
			<p>Selecciona uno de los temas que tengas para ver como quedaria en la aplicación, si quieres adquirir más temas pasate por la tienda del campus.</p>
			<form name="tema_ajustes" action="src/controlador/tema.main.php?opcion=3" method="post">
				<input type="hidden" name="url" value="ajustes.php">
				<div id="completaTema">
					<?php
						$temas = new Tema();
						echo $temas->temasDelUsuario($_SESSION['user']);
					?>
				</div>
				<input type="submit" name="enviar" value="Guardar Tema">
			</form>
		</div>

		<h3><a><i class='fas fa-chevron-circle-down'></i> Cambiar contraseña</a></h3>
		<div id="passAjustes">
			<p>Si deseas modificar tu contraseña, rellena este formulario. Tan solo tienes que introducir tu actual contraseña y la nueva que quieras.</p>
			<form name="pass_ajustes" method="post">
				<ul>
					<li><label>Contraseña actual</label></li>
					<li><input type="password" name="currentPass" placeholder="Tu contraseña actual"></li>
				</ul>
				<ul>
					<li><label>Nueva contraseña</label></li>
					<li><input type="password" name="newPass" placeholder="Elige una nueva contraseña"></li>
				</ul>
				<ul>
					<li><label>Repite contraseña</label></li>
					<li><input type="password" name="repeatPass" placeholder="Repite la contraseña"></li>
				</ul>
				<input type="button" name="enviar" onclick="cambiarPassword('src')" value="Cambiar">
			</form>
		</div>
		
		<h3><a><i class='fas fa-chevron-circle-down'></i> Preferencias de notificaciones</a></h3>
		<div id="notificacionesAjustes" name="notificacionesPag">
			<p>Selecciona entre recibir o no recibir notificaciones de correo para tus cursos comprados.</p>
			<?php
				$matriculas = new Matricula();
				echo $matriculas->notificacionesMatriculas($_SESSION['user']);
			?>
		</div>
	</section>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>