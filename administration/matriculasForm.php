<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/curso.class.php';
require '../src/modelo/matricula.class.php';

$matricula = new Matricula();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $matricula->obtenerMatriculaPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita una Matricula | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<style type="text/css">
		#notificacionesBtn{
			width: 25% !important;
			padding: 1% 0!important;
			margin: 2% 37.5% !important;
			color: #FFF !important;
			text-transform: none !important;
			font-size: 1em !important;
		}
		#notificacionesBtn:hover{
		    -webkit-transform:scale(1.05);
		    transform:scale(1.05);
		    box-shadow: 0 0 1em #797D7F;
		}
		@media screen and (max-width:1024px){
			#notificacionesBtn{
				width: 60% !important;
				margin: 5% 20% !important;
				font-size: 0.9em !important;
			}
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="matricula_form" action="../src/controlador/matricula.main.php?opcion=3" method="post">
				<input type="hidden" name="id" value="<?php echo ($matricula->getId()!= NULL)?$matricula->getId():"0" ?>">
				<input type="hidden" name="tipo_pago" value="3">
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Seleccionar Curso</label></li>
					<li>
						<?php
							echo "<select name='id_curso' ".strval(($matricula->getId() != NULL)?'disabled':'').">";
							$curso = new Curso();
							echo $curso->selectorCursos(($matricula->getId_curso()!= NULL)?$matricula->getId_curso():0);
							echo "</select>";
						?>
					</li>
				</ul>
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Seleccionar un Alumno</label></li>
					<li>
						<?php
							echo "<select name='id_usuario' ".strval(($matricula->getId() != NULL)?'disabled':'').">";
							$usuario = new Usuario();
							echo $usuario->selectorAlumnos(($matricula->getId_usuario()!= NULL)?$matricula->getId_usuario():0);
							echo "</select>";
						?>
					</li>
				</ul>
				<ul>
					<li>
						<?php
							if ($matricula->getId_curso()!= NULL && $matricula->getNotificaciones() == 1) {
								echo "<input type='button' id='notificacionesBtn' style='background-color: #149F1C;' onclick='recibirNotificaciones(this)' value='Recibir Notificaciones'>";
								echo "<input type='checkbox' id='notificacionesCheck' name='notificaciones' style='display: none;' checked>";
							}else{
								echo "<input type='button' id='notificacionesBtn' style='background-color: #A80707;' onclick='recibirNotificaciones(this)' value='NO Recibir Notificaciones'>";
								echo "<input type='checkbox' id='notificacionesCheck' name='notificaciones' style='display: none;'>";
							}

						?>
					</li>
				</ul>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.matricula_form.submit()" value="<?php echo ($matricula->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="matriculasList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>