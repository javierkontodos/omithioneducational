<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/consulta.class.php';

$consulta = new Consulta();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $consulta->obtenerConsultaPorId($id);
}else header("location: consultasList.php");

?>
<!DOCTYPE html>
<html>
<head>
	<title>Ver Consulta | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="consultaTarjeta">
			<ul>
				<li><h4>Titulo de la consulta</h4></li>
				<li><h3><?php echo $consulta->getTitulo(); ?></h3></li>
			</ul>
			<ul>
				<li><h4>Descripción de la consulta</h4></li>
				<li><p><?php echo $consulta->getDescripcion(); ?></p></li>
			</ul>
			<ul>
				<li><h4>Usuario y fecha de creación</h4></li>
				<li><p>Creado por <b><?php echo $consulta->obtenerNombreApellidos($consulta->getId_usuario()); ?></b> el día <b><?php echo $consulta->getFecha_creacion(); ?></b></p></li>
			</ul>
		</div>
		<div id="adminForm">
			<form name="consulta_form" action="../src/controlador/consulta.main.php?opcion=5" method="post">
				<input type="hidden" name="id" value="<?php echo $consulta->getId(); ?>">
				<input type="hidden" name="email" value="<?php echo $consulta->obtenerEmail($consulta->getId_usuario()); ?>">
				<ul id="formDatosTxtArea">
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Respuesta</label></li>
					<li><textarea name="respuesta" onblur="check_form_vacio(this, 0)" placeholder="Escribe aqui la respuesta a la consulta"></textarea></li>
				</ul>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.consulta_form.submit();" value="Responder">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="consultasList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>