<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/curso.class.php';

$curso = new Curso();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $curso->obtenerCursoPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Curso | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="curso_form" action="../src/controlador/curso.main.php?opcion=1" method="post">
				<input type="hidden" name="id" value="<?php echo ($curso->getId()!= NULL)?$curso->getId():"0" ?>">
				<div id="formDatos">
					<ul id="formDatosFoto">
						<li><label>Fotografia</label></li>
						<?php
							if ($curso->getId()==NULL) {
								echo "<li><div id='formFoto' style='background-image: url(../images/cursos/plantilla.png)'></div></li>";
							}else{
								echo "<li><div id='formFoto' style='background-image: url(../".$curso->getFoto().")'></div></li>";
							}
						?>
						<li>
							<input type="hidden" name="foto" id="selectedFoto" value="<?php echo ($curso->getFoto()!= NULL)?$curso->getFoto():"images/cursos/plantilla.png" ?>">
							<input type="file" name="selectedFile" id="selectedFile" style="display: none;" onchange="subirFotoForm('cambiarFotoCurso.php')" accept="image/jpeg">
							<input type="button" name="fotoBoton" value="Seleccionar Foto" onclick="document.getElementById('selectedFile').click();" style="border-radius: 5px;">
						</li>
					</ul>
					<ul id="formDatosTxtArea">
						<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Descripción</label></li>
						<li><textarea name="descripcion" onblur="check_form_vacio(this, 0)" placeholder="Escribe una breve descripción" maxlength="160"><?php echo ($curso->getDescripcion()!= NULL)?$curso->getDescripcion():"" ?></textarea></li>
					</ul>
				</div>
				<div id="dobleForm">
					<ul>
						<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Nombre</label></li>
						<li><input type="text" name="nombre" onblur="check_form_vacio(this, 1)" placeholder="Introduce el nombre" value="<?php echo ($curso->getNombre()!= NULL)?$curso->getNombre():"" ?>"></li>
					</ul>
					<ul>
						<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Precio</label></li>
						<li><input type="text" name="precio" onblur="check_form_vacio(this, 2)" placeholder="Introduce el precio" value="<?php echo ($curso->getPrecio()!= NULL)?$curso->getPrecio():"" ?>"></li>
					</ul>
				</div>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Duracion (en horas)</label></li>
					<li><input type="text" name="duracion" onblur="check_form_vacio(this, 3)" placeholder="Introduce la duracion" value="<?php echo ($curso->getDuracion()!= NULL)?$curso->getDuracion():"" ?>"></li>
				</ul>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.curso_form.submit()" value="<?php echo ($curso->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="cursosList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>