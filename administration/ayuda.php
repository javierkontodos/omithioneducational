<?php require 'includes/verificacion.inc.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion3').className = "activo";
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section id="ayudaView">
		<h1>Bienvenido a la pagina de ayuda</h1>
		<p style="text-align: center;">Aqui encontrarás una pequeña guia de uso de la administración de la aplicación.</p>

		<h3><a href='javascript:ayudaPlegable(1)'><span id='iconoPlegable1'><i class='fas fa-chevron-circle-right'></i></span> Crear usuario</a></h3>
        <span id='ayudaPlegable1' style="display: none;">
        	<p>Para crear un nuevo usuario deberemos pulsar el botón entrar, de la opción usuarios. Una vez estemos dentro deberemos pulsar al botón azul con el botón "+".</p>
        	<img src="../images/ayuda/ejemplo.jpg">
        	<p>Cuando tengamos el paso anterior compeltado deberemos cumplimentar el formulario con los datos requeridos y darle a crear.</p>
        </span>
        <h3><a href='javascript:ayudaPlegable(2)'><span id='iconoPlegable2'><i class='fas fa-chevron-circle-right'></i></span> Gestionar consultas</a></h3>
        <span id='ayudaPlegable2' style="display: none;">
        	<p>Para gestionar una consulta deberemos ir a la parte de inicio, después pu6lsar sobre el botón ubicado en consultas y finalmente entraremos en la zona de consultas y podremos gestionarlas.</p>
        	<img src="../images/ayuda/ejemplo.jpg">
        	<p>Podemos asignar o eliminar consultas. Una vez estén asignadas podremos ver las asignaturas o liberarlas. Si las liberamos para poder gestionarlas deberemos asignarnoslas otra vez. Para resolverlas le daremos al botónde "ver" y daremos respuesta al usuario automáticamente. La respuesta llegará por correo electrónico.</p>
        </span>
        <h3><a href='javascript:ayudaPlegable(3)'><span id='iconoPlegable3'><i class='fas fa-chevron-circle-right'></i></span> Crear matrículas</a></h3>
        <span id='ayudaPlegable3' style="display: none;">
        	<p>Para crear una matrícula deberemos pulsar en el botón "entrar" de la parte de matrículas. Una vez hayamos el botón accederemos a la zona de matriculaciones y podremos realizar diferentes gestiones.</p>
        	<img src="../images/ayuda/ejemplo.jpg">
        	<p>Para crear un nuevos usuario lo que debemos hacer será pulsar el botón "+" y completar los datos requeridos en el formulario. Al finalizar pulsaremos el botón de guardar.</p>
        </span>
        <h3><a href='javascript:ayudaPlegable(4)'><span id='iconoPlegable4'><i class='fas fa-chevron-circle-right'></i></span> Crear cursos</a></h3>
        <span id='ayudaPlegable4' style="display: none;">
        	<p>Para crear un curso nuevo deberemos ir a la zona de cursos. Una vez allí podremos realizar diferentes gestiones.</p>
        	<img src="../images/ayuda/ejemplo.jpg">
        	<p>Para crear un curso nuevo deberemos pulsar sobre el botón "+" y completar el formulario. Una vez hayamos completado esto pulsaremos el botón de guardar para mantener los datos.</p>
        </span>
        <h3><a href='javascript:ayudaPlegable(5)'><span id='iconoPlegable5'><i class='fas fa-chevron-circle-right'></i></span> Crear temas de colores</a></h3>
        <span id='ayudaPlegable5' style="display: none;">
        	<p>Para crear los temas de clores deberemos pulsar sobre el botón "entrar" de la opción "temas de colores".</p>
        	<img src="../images/ayuda/ejemplo.jpg">
        	<p>Cuando nos encontremos en la zona de lso temas deberemos pulsar sobre el botón "+" y completar el formulario. Una vez hayamos completado esto pulsaremos el botón de guardar para mantener los datos y guardar así el nuevo tema.</p>
        </span>

	</section>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>