<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/asignatura.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Asignaturas | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<style type="text/css">
		.datosContainer {
    			width: 80%;
			}
		@media screen and (max-width: 1920px){
			.datosContainer {
    			width: 80%;
			}
		}
		@media screen and (max-width:1024px){
			.datosContainer{
				width: auto;
			}
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarAsignatura')">
			</form>
		</div>
		<div id="listadoContainer" style="padding-top: 50px;">
			<?php
				$asignatura = new Asignatura();
				echo ($asignatura->listarAsignaturas());
			?>
		<!--
			<p class='tituloDesplegable'><a href='javascript:tituloPlegable(1)'>Curso Prueba <span id='iconoPlegable1'><i class='far fa-caret-square-down'></i></span></a></p>
			<span id='cursoPlegable1'>
				<div class='tarjetaContainer'>
					<div class='datosContainer'>
						<h2>Nombre</h2>
						<p><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i><i class='fas 	fa-grip-lines'></i></p>
						<p><b>PROFESOR</b></p>
						<p>Francisco Muñoz</p>
					</div>
					<div class='botonesContainer'>
						<button style='color: #000;'>Editar</button>
						<button style='background-color: #C0392B;'>Borrar</button>
					</div>
				</div>
			</span>
		-->
		
		</div>
		<form name="eliminaAsignatura" action="../src/controlador/asignatura.main.php">
			<input type="hidden" name="opcion" value="2">
			<input type="hidden" name="id_asignatura">
		</form>
	</section>
	<div id="addForm">
		<a href="asignaturasForm.php"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaAsignatura.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>