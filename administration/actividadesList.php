<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/actividad.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Actividades | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<style type="text/css">
		#moreBtn {
			display: none;
		}
		.datosContainer {
    			width: 80%;
    			margin-left: 25px;
			}
		@media screen and (max-width: 1920px){
			.datosContainer {
    			width: 80%;
			}
		}
		@media screen and (max-width:1024px){
			.datosContainer{
				width: 100%;
				margin: 0;
			}
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarActividad')">
			</form>
		</div>
		<div id="listadoContainer" style="padding-top: 50px;">
			<?php
				$actividad = new Actividad();
				echo ($actividad->listarActividades());
			?>
		<!--
			<p class='tituloDesplegable'><a href='javascript:tituloPlegable(1)'>Curso Prueba <span id='iconoPlegable1'><i class='far fa-caret-square-down'></i></span></a></p>
			<span id='cursoPlegable1'>
				<div class='tarjetaContainer'>
					
					<div class='datosContainer'>
						<h2>kkkkkkkkkkkkkkkkkk</h2>
						<p>hhhhhhhhhhhhhhh</p>
						<p>De dia <b>jjjjjj</b> hasta el <b>kkkkkkkkkkk</b></p>
						<ul class='archivosContainer'>
							<li><span style='background-image: url(../images/archivos/nofile.png);'></span></li>
							<li><span style='background-image: url(../images/archivos/nofile.png);'></span></li>
							<li><span style='background-image: url(../images/archivos/nofile.png);'></span></li>
						</ul>
					</div>
                    <div class='botonesContainer'>
                        <button style='color: #000;' onclick=window.location='';>Editar</button>
                        <button style='background-color: #C0392B;' onclick='borraActividad("")'>Borrar</button>
                    </div>
                    
                </div>
			</span>
		-->
		
		</div>
		<form name="eliminaActividad" action="../src/controlador/actividad.main.php">
			<input type="hidden" name="opcion" value="2">
			<input type="hidden" name="url" value="administration/actividadesList.php">
			<input type="hidden" name="id_actividad">
		</form>
	</section>
	<div id="addForm">
		<a href="actividadesForm.php"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaActividad.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>