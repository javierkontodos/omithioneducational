<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/tema.class.php';

$tema = new Tema();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $tema->obtenerTemaPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Tema | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		function cambiarColor(elemento) {
			elemento.style.backgroundColor = elemento.value;
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="tema_form" action="../src/controlador/tema.main.php?opcion=1" method="post">
				<input type="hidden" name="id" value="<?php echo ($tema->getId()!= NULL)?$tema->getId():"0" ?>">
				<input type="hidden" name="ruta" value="<?php echo ($tema->getRuta()!= NULL)?$tema->getRuta():"" ?>">
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Nombre del Tema</label></li>
					<li><input type="text" name="nombre" onblur="check_form_vacio(this, 0)" placeholder="Introduce el nombre" value="<?php echo ($tema->getNombre()!= NULL)?$tema->getNombre():"" ?>"></li>
				</ul>
				<div id="dobleForm">
					<ul>
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Color Primario</label></li>
						<?php 
							if ($tema->getColor_principal()!= NULL) {
								echo "<li><input type='color' name='color_principal' style='background-color: ".$tema->getColor_principal().";' value='".$tema->getColor_principal()."' onchange='cambiarColor(this)'></li>";
							}else {
								echo "<li><input type='color' name='color_principal' style='background-color: #DDDDDD;' value='#DDDDDD' onchange='cambiarColor(this)'></li>";
							}
						?>
					</ul>
					<ul>
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Color Secundario</label></li>
						<?php 
							if ($tema->getColor_secundario()!= NULL) {
								echo "<li><input type='color' name='color_secundario' style='background-color: ".$tema->getColor_secundario().";' value='".$tema->getColor_secundario()."' onchange='cambiarColor(this)'></li>";
							}else {
								echo "<li><input type='color' name='color_secundario' style='background-color: #DDDDDD;' value='#DDDDDD' onchange='cambiarColor(this)'></li>";
							}
						?>
					</ul>
				</div>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Precio del Tema</label></li>
					<li><input type="text" name="precio" onblur="check_form_vacio(this, 1)" placeholder="Introduce el precio" value="<?php echo ($tema->getPrecio()!= NULL)?$tema->getPrecio():"" ?>"></li>
				</ul>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.tema_form.submit()" value="<?php echo ($tema->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="temasList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>