<?php require 'includes/verificacion.inc.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<script type="text/javascript">
		window.onload = function (){
			document.getElementById('opcion1').className = "activo";
		}
	</script>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div class="adminCards">
			<div onclick="window.location='usuariosList.php';">
				<p class="iconoCard"><i class="fas fa-users"></i></p>
				<p>Usuarios</p>
				<input type="button" onclick="window.location='usuariosList.php';" value="Entrar">
			</div>
			<div onclick="window.location='consultasList.php';">
				<p class="iconoCard"><i class="fas fa-atom"></i></p>
				<p>Consultas</p>
				<input type="button" onclick="window.location='consultasList.php';" value="Entrar">
			</div>
			<div onclick="window.location='matriculasList.php';">
				<p class="iconoCard"><i class="fas fa-money-check"></i></p>
				<p>Matrículas</p>
				<input type="button" onclick="window.location='matriculasList.php';" value="Entrar">
			</div>
		</div>
		<div class="adminCards">
			<div onclick="window.location='cursosList.php';">
				<p class="iconoCard"><i class="fas fa-graduation-cap"></i></p>
				<p>Cursos</p>
				<input type="button" onclick="window.location='cursosList.php';" value="Entrar">
			</div>
			<div onclick="window.location='temasList.php';">
				<p class="iconoCard"><i class="fas fa-palette"></i></p>
				<p>Temas de Colores</p>
				<input type="button" onclick="window.location='temasList.php';" value="Entrar">
			</div>
			<div>
				<p class="iconoCard"><i class="fas fa-swatchbook"></i></p>
				<p>Pack de Iconos</p>
				<input type="button" value="Entrar">
			</div>
		</div>
		<div class="adminCards">
			<div onclick="window.location='asignaturasList.php';">
				<p class="iconoCard"><i class="fas fa-book"></i></p>
				<p>Asignaturas</p>
				<input type="button" onclick="window.location='asignaturasList.php';" value="Entrar">
			</div>
			<div onclick="window.location='temariosList.php';">
				<p class="iconoCard"><i class="fas fa-atlas"></i></p>
				<p>Temarios</p>
				<input type="button" onclick="window.location='temariosList.php';" value="Entrar">
			</div>
			<div onclick="window.location='actividadesList.php';">
				<p class="iconoCard"><i class="fas fa-journal-whills"></i></p>
				<p>Actividades</p>
				<input type="button" onclick="window.location='actividadesList.php';" value="Entrar">
			</div>
		</div>
	</section>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>