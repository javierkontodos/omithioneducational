<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/temario.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Temarios | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<style type="text/css">
		#moreBtn {
			display: none;
		}
		.datosContainer {
    			width: 80%;
    			margin-left: 25px;
			}
		@media screen and (max-width: 1920px){
			.datosContainer {
    			width: 80%;
			}
		}
		@media screen and (max-width:1024px){
			.datosContainer{
				width: 100%;
				margin: 0;
			}
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarTemario')">
			</form>
		</div>
		<div id="listadoContainer" style="padding-top: 50px;">
			<?php
				$temario = new Temario();
				echo ($temario->listarTemarios());
			?>
		<!--
			<p class='tituloDesplegable'><a href='javascript:tituloPlegable(1)'>Curso Prueba <span id='iconoPlegable1'><i class='far fa-caret-square-down'></i></span></a></p>
			<span id='cursoPlegable1'>
				<div class='tarjetaContainer'>
					<div class='datosContainer'>
						<h2>Nombre</h2>
						<p>Contenido</p>
						<p style='margin: 0;'><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i><i class='fas fa-grip-lines'></i><i class='fas 	fa-grip-lines'></i></p>
						<p style='margin-top: 0;'>Creado el </p>
					</div>
					<div class='botonesContainer'>
						<button style='color: #000;'>Editar</button>
						<button style='background-color: #C0392B;'>Borrar</button>
					</div>
				</div>
			</span>
		-->
		
		</div>
		<form name="eliminaTemario" action="../src/controlador/temario.main.php">
			<input type="hidden" name="opcion" value="2">
			<input type="hidden" name="url" value="administration/temariosList.php">
			<input type="hidden" name="id_temario">
		</form>
	</section>
	<div id="addForm">
		<a href="temariosForm.php"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaTemario.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>