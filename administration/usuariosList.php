<?php require 'includes/verificacion.inc.php';?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Usuarios | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
	<style type="text/css">
		.botonesContainer button{
			width: 100%;
			margin: 10px 0;
		}
	</style>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarUsuario')">
			</form>
		</div>
		<div id="listadoContainer">
			<?php
				$usuario = new Usuario();
				echo ($usuario->listarUsuarios());
			?>
		<!--
			<div class="tarjetaContainer">
				<div class="imagenContainer">
					<span style="background-image: url('../images/usuarios/15576779094aa217b2048f25ba77dd964d2a91bb90.jpg');"></span>
				</div>
				<div class="datosContainer">
					<p>Nombre y Apellidos</p>
					<p>Biografia</p>
					<p style="margin-bottom: 0;">Administrador: <span style="color: #C0392B;"><i class="fas fa-times-circle"></i></span></p>
					<p style="margin-top: 0;">Profesor: <span style="color: #9FD120;"><i class="fas fa-check-circle"></i></span></p>
				</div>
				<div class="botonesContainer">
					<button style="color: #000;">Editar</button>
					<button style="background-color: #C0392B;">Borrar</button>
				</div>
			</div>
		-->
		</div>
		<form name="eliminaUsuario" action="../src/controlador/usuario.main.php">
			<input type="hidden" name="opcion" value="8">
			<input type="hidden" name="id_usuario">
		</form>
	</section>
	<div id="addForm">
		<a href="usuariosForm.php"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaUsuario.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>