<?php 
require 'includes/verificacion.inc.php';
require '../src/modelo/asignatura.class.php';
require '../src/modelo/actividad.class.php';

$actividad = new Actividad();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = intval($_GET['id']);
    $actividad->obtenerActividadPorId($id);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Crea/Edita un Actividad | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="adminForm">
			<form name="actividad_form" action="../src/controlador/actividad.main.php?opcion=1" method="post">
				<input type="hidden" name="url" value="administration/actividadesList.php">
				<input type="hidden" name="id" value="<?php echo ($actividad->getId()!= NULL)?$actividad->getId():"0" ?>">
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Asignatura al que pretenece</label></li>
					<li>
						<select name="asignatura">
							<?php
								$asignatura = new Asignatura();
								echo $asignatura->selectorAsignaturas(($actividad->getId_asignatura()!= NULL)?$actividad->getId_asignatura():0);
							?>
						</select>
					</li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Nombre de la Actividades</label></li>
					<li><input type="text" name="nombre" onblur="check_form_vacio(this, 0)" placeholder="Introduce un nombre" value="<?php echo ($actividad->getNombre()!= NULL)?$actividad->getNombre():"" ?>"></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Contenido de la Actividad</label></li>
					<li><textarea name="contenido" onblur="check_form_vacio(this, 1)" placeholder="Introduce el contenido que tendrá" maxlength="160"><?php echo ($actividad->getContenido()!= NULL)?$actividad->getContenido():"" ?></textarea></li>
				</ul>
				<ul>
					<li><label class="labelForm"><span class="spanForm"><i class="fas fa-exclamation-circle"></i></span> Fecha de finalización</label></li>
					<li><input type="date" name="fecha" onblur="check_form_vacio(this, 2)" value="<?php echo ($actividad->getFecha_fin()!= NULL)?$actividad->getFecha_fin():"" ?>"></li>
				</ul>
				<div id="fileForm">
					<input id='numSelect' type='hidden'>
					<?php
						if ($actividad->getId()!= NULL) {
							echo $actividad->obtenerArchivos($actividad->getId());
						}else{
							echo $actividad->pintarArchivo(1);
							echo $actividad->pintarArchivo(2);
							echo $actividad->pintarArchivo(3);
						}
					?>
				</div>
				<input id="adminGuardar" type="button" name="enviar" onclick="document.actividad_form.submit()" value="<?php echo ($actividad->getId()!= NULL)?"Actualizar":"Crear" ?>">
			</form>
		</div>
	</section>
	<div id="backList">
		<a href="actividadesList.php"><i class="fas fa-arrow-alt-circle-left"></i></a>
	</div>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>