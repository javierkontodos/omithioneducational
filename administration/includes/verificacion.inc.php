<?php
	require_once '../src/modelo/usuario.class.php';
	require_once '../src/conector/bd.class.php';

	session_start();
    if (isset($_SESSION["user"])) {
        if (!Usuario::verificado($_SESSION["user"]) || !Usuario::perfilCompletado($_SESSION["user"]) || !Usuario::usuarioAdmin($_SESSION["user"])) {
            header("location:../index.php");
        }
    }else{
        header("location:../login.php");
    }
?>