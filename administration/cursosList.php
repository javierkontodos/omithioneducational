<?php
require 'includes/verificacion.inc.php';
require '../src/modelo/curso.class.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Cursos | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<?php include 'includes/header.inc.php';?>
	<section>
		<div id="buscadorList">
			<form id="formBusqueda">
				<input type="text" name="busqueda" id="nombreBuscado">
				<input type="button" name="Buscar" value="Buscar" onclick="barraBusqueda('buscarCurso')">
			</form>
		</div>
		<div id="listadoContainer">
			<?php
				$curso = new Curso();
				echo ($curso->listarCursos());
			?>
		<!--
			<div class='tarjetaContainer'>
				<div class='imagenContainer'>
					<span style='background-image: url(../images/usuarios/15576779094aa217b2048f25ba77dd964d2a91bb90.jpg);'></span>
				</div>
				<div class='datosContainer'>
					<p>Nombre</p>
					<p>Descripcion</p>
					<p><b>Precio</b> - 200.00 €</p>
					<p><b>Duración</b> - 1800 Horas</p>
				</div>
				<div class='botonesContainer'>
					<button style='color: #000;'>Editar</button>
					<button style='background-color: #C0392B;'>Borrar</button>
				</div>
			</div>
		-->
		</div>
		<form name="eliminaCurso" action="../src/controlador/curso.main.php">
			<input type="hidden" name="opcion" value="2">
			<input type="hidden" name="id_curso">
		</form>
	</section>
	<div id="addForm">
		<a href="cursosForm.php"><i class="fas fa-plus-circle"></i></a>
	</div>
	<div id="confirmacionPantalla" onclick="cerrarAlerta()"></div>
	<div id="confirmacionMensaje">
		<h2 id="confirmacionH2"></h2>
		<h3 id="confirmacionH3"></h3>
		<p id="confirmacionP"></p>
		<button style="background-color: #C0392B;" onclick="cerrarConfirmacion()">Cancelar</button>
		<button id="confirmacionBoton" onclick="document.eliminaCurso.submit()">Confirmar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>