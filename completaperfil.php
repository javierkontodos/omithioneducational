<?php
	require_once 'src/modelo/usuario.class.php';
	require_once 'src/conector/bd.class.php';

	session_start();
    if (!isset($_SESSION["user"])) {
        header("location:login.php");
    }else if (Usuario::perfilCompletado($_SESSION["user"])) {
    	header("location:index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Inicio | Omithion Educational</title>
	<?php include 'includes/head.inc.php';?>
	<?php include 'includes/tema.head.inc.php';?>
</head>
<body>
	<header>
		<img src="images/logo_blanco.png">
		<div id="menuMovil">
			<a href="javascript:menuMovil(1)"><i class="fas fa-bars fa-lg"></i></a>
		</div>
		<div id="menuMovil2">
			<a href="javascript:menuMovil(2)"><i class="fas fa-times fa-lg"></i></a>
		</div>
		<nav>
			<a href="src/controlador/usuario.main.php?opcion=2">
				<ul>
					<li><i class="fas fa-power-off fa-lg"></i></li>
					<li>Salir</li>
				</ul>
			</a>
		</nav>
	</header>
	<section>
		<h1 style="color: #FFBA00;">Completa tu perfil</h1>
		<p style="text-align: center;">Antes de continuar, completa los siguientes campos para poder finalizar el registro de tu usuario.</p>
		<div id="completaPerfil">
			<form name="completa_perfil" action="src/controlador/usuario.main.php?opcion=7" method="post">
				<div id="completaDatos">
					<ul id="completaDatosFoto">
						<li><label>Fotografia</label></li>
						<li><div id="fotoUser"></div></li>
						<li>
							<input type="hidden" name="foto" id="selectedFoto" value="images/usuarios/plantilla.png">
							<input type="file" name="selectedFile" id="selectedFile" style="display: none;" onchange="subeFotoUsuario()" accept="image/jpeg">
							<input type="button" name="fotoBoton" value="Subir Foto" onclick="document.getElementById('selectedFile').click();" style="border-radius: 5px;">
						</li>
					</ul>
					<ul id="completaDatosBio">
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Descripción</label></li>
						<li><textarea name="biografia" onblur="completa_perfil_vacio(this, 1)" placeholder="Introduce una breve descripción tuya" maxlength="160"></textarea></li>
					</ul>
				</div>
				<div id="completaDireccion">
					<ul>
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Dirección</label></li>
						<li><input type="text" name="direccion" onblur="completa_perfil_vacio(this, 2)" placeholder="Introduce tu dirección"></li>
					</ul>
					<ul>
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Codigo Postal</label></li>
						<li><input type="text" name="cod_postal" onblur="completa_perfil_vacio(this, 3)" placeholder="Introduce tu código postal"></li>
					</ul>
				</div>
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Pais</label></li>
					<li><input type="text" name="pais" onblur="completa_perfil_vacio(this, 4)" placeholder="Introduce tu pais"></li>
				</ul>
				<div id="completaDireccion">
					<ul>
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Poblacion</label></li>
						<li><input type="text" name="poblacion" onblur="completa_perfil_vacio(this, 5)" placeholder="Introduce tu población"></li>
					</ul>
					<ul>
						<li><label><span><i class="fas fa-exclamation-circle"></i></span> Provincia</label></li>
						<li><input type="text" name="provincia" onblur="completa_perfil_vacio(this, 6)" placeholder="Introduce tu provincia"></li>
					</ul>
				</div>
				<ul>
					<li><label><span><i class="fas fa-exclamation-circle"></i></span> Telefono movil</label></li>
					<li><input type="text" name="movil" onblur="completa_perfil_vacio(this, 7)" placeholder="Introduce tu movil"></li>
				</ul>
				<ul>
					<li><label>Otro telefono</label></li>
					<li><input type="text" name="telefono" placeholder="Introduce otro telefono (opcional)"></li>
				</ul>
				<ul>
					<li>Selecciona un Tema</li>
					<li>
						<div id="completaTema">
							<input type="button" class="check" style="background-color: #152770; border-color: #F39C12;" onclick="seleccionarTema(0)">
							<input type="button" class="check" style="background-color: #C0392B;" onclick="seleccionarTema(1)">
							<input type="button" class="check" style="background-color: #58B616;" onclick="seleccionarTema(2)">
							<input type="button" class="check" style="background-color: #8E44AD;" onclick="seleccionarTema(3)">
						</div>
					</li>
					<input type="radio" name="tema" value="1" style="display: none;" checked>
					<input type="radio" name="tema" value="2" style="display: none;">
					<input type="radio" name="tema" value="3" style="display: none;">
					<input type="radio" name="tema" value="4" style="display: none;">
				</ul>
				<input id="completaGuardar" type="button" name="enviar" onclick="validar_completa_perfil()" value="Guardar">
			</form>
		</div>
	</section>
	<div id="alertaPantalla" onclick="cerrarAlerta()"></div>
	<div id="alertaMensaje">
		<h2 id="alertaH2"></h2>
		<h3 id="alertaH3"></h3>
		<p id="alertaP"></p>
		<button onclick="cerrarAlerta()">Cerrar</button>
	</div>
	<?php include 'includes/footer.inc.php';?>
</body>
</html>